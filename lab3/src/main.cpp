#include <iostream>
#include "Polinomio.h"
#include "Fraccion.h"

using namespace std;

int main()
{
     int select = 0;
     cout << endl
          << " ¡Bienvenid@s!  :)" << endl;
     bool continuar = true;
     int c;

     while (continuar == true)
     {

          cout << endl
               << " ¿Con cuál clase desea realizar pruebas?" << endl
               << "\t\t1. Polinomios" << endl
               << "\t\t2. Fracciones" << endl
               << "\t\t3. Salir del programa" << endl
               << "\tIngrese una opción:" << endl
               << "\t\t-> ";

          //Seleccion de clase para trabajar
          while (1)
          {
               try
               {

                    cin >> select;
                    if (select == 1) // implementacion de polinomio
                    {
                         cout << endl
                              << "===========================================================" << endl
                              << "---> Pruebas para implementación de la clase Polinomio <---" << endl
                              << "===========================================================" << endl;

                         int c1;
                         int c2;
                         int d1;
                         int d2;
                         int e1;
                         int e2;

                         cout << endl
                              << endl
                              << " Ingresará un primer polinomio de la forma p(x) = cx² + dx¹ + e"
                              << endl
                              << "\t\t-> c = ";
                         cin >> c1;
                         cout << "\t\t-> d = ";
                         cin >> d1;
                         cout << "\t\t-> e = ";
                         cin >> e1;

                         cout << endl
                              << " Ingresará un segundo polinomio de la forma p(x) = cx² + dx¹ + e"
                              << endl
                              << "\t\t-> c = ";
                         cin >> c2;
                         cout << "\t\t-> d = ";
                         cin >> d2;
                         cout << "\t\t-> e = ";
                         cin >> e2;

                         Polinomio p1(c1, d1, e1);
                         Polinomio p2(c2, d2, e2);
                         cout << endl
                              << " Ha ingresado los polinomios: " << endl
                              << "\tp1(x) = " << p1 << endl
                              << "\tp2(x) = " << p2 << endl;

                         int operac;
                         cout << endl
                              << " ¿Que operación desea probar?" << endl
                              << "\t\t1. Suma            p1 + p2" << endl
                              << "\t\t2. Resta           p1 - p2" << endl
                              << "\t\t3. Multiplicación  p1 * p2" << endl
                              << "\t\t4. División        p1 / p2" << endl
                              << "\tIngrese una opción:" << endl
                              << "\t\t-> ";
                         cin >> operac;
                         cout << endl
                              << "\t\tEl resultado es:" << endl
                              << "\t\t";
                         switch (operac)
                         {
                         case 1:
                         {
                              Polinomio suma = p1 + p2;
                              cout << "\tp1 + p2 = " << suma << endl;
                         }
                         break;
                         case 2:
                         {
                              Polinomio resta = p1 - p2;
                              cout << "\tp1 - p2 = " << resta << endl;
                              break;
                         }
                         case 3:
                         {
                              Polinomio mult = p1 * p2;
                              cout << "\tp1 * p2 = " << mult << endl;
                              break;
                         }
                         case 4:
                         {
                              Polinomio div = p1 / p2;
                              cout << "\t" << div << endl;
                              break;
                         }
                         default:
                              break;
                         }
                         cout << endl
                              << "==========================================================" << endl
                              << " ¿Desea continuar probando el programa?" << endl
                              << "\t\tPresione 1 para continuar en el programa " << endl
                              << "\t\to cualquier otro número distinto de 1 para salir." << endl
                              << "\t\t->";

                         cin >> c;
                         if (c == 1)

                         {
                              cout << "==========================================================" << endl;
                              continuar = true;
                         }
                         else
                         {
                              continuar = false;
                              cout << " Saliendo del programa, gracias." << endl
                                   << "==========================================================" << endl;
                         }

                         break;
                    }
                    else if (select == 2) // implementacion de fraccion
                    {
                         cout << endl
                              << "==========================================================" << endl
                              << "---> Pruebas para implementación de la clase Fracción <---" << endl
                              << "===========================================================" << endl;

                         Fraccion f1;
                         Fraccion f2;

                         cout << endl
                              << " Ingresará una primera fracción de forma f = a/b" << endl
                              << "\t\t->f1 = ";
                         cin >> f1;

                         cout << endl
                              << " Ingresará una segunda primer fracción de forma f = a/b" << endl
                              << "\t\t->f2 = ";
                         cin >> f2;

                         int operac;
                         cout << endl
                              << " ¿Que operación desea realizar?" << endl
                              << "\t\t1. Suma:             p1 + p2" << endl
                              << "\t\t2. Resta:            p1 - p2" << endl
                              << "\t\t3. Multiplicación:   p1 * p2" << endl
                              << "\t\t4. División:         p1 / p2" << endl
                              << "\tIngrese una opción:" << endl
                              << "\t\t-> ";
                         cin >> operac;
                         cout << endl
                              << "\tEl resultado es:" << endl
                              << "\t\t";

                         switch (operac)
                         {
                         case 1:
                         {
                              Fraccion suma = f1 + f2;
                              cout << "\tf1 + f2 = " << suma << endl;
                         }
                         break;
                         case 2:
                         {
                              Fraccion resta = f1 - f2;
                              cout << "\tf1 - f2 = " << resta << endl;
                              break;
                         }
                         case 3:
                         {
                              Fraccion mult = f1 * f2;
                              cout << "\tf1 * f2 = " << mult << endl;
                              break;
                         }
                         case 4:
                         {
                              Fraccion div = f1 / f2;
                              cout << "\t" << div << endl;
                              break;
                         }
                         default:
                              break;
                         }

                         cout << endl
                              << "==========================================================" << endl
                              << " ¿Desea continuar probando el programa?" << endl
                              << "\t\tPresione 1 para continuar en el programa " << endl
                              << "\t\to cualquier otro número distinto de 1 para salir." << endl
                              << "\t\t-> ";
                         cin >> c;
                         if (c == 1)
                         {
                              cout << "==========================================================" << endl;
                              continuar = true;
                         }
                         else
                         {
                              cout << " Saliendo del programa, gracias." << endl
                                   << "==========================================================" << endl;
                              continuar = false;
                         }

                         break;
                    }
                    else if (select == 3)
                    {
                         cout << " Saliendo del programa, gracias." << endl
                              << "==========================================================" << endl;
                         return 0;
                    }
                    else
                    {
                         bool errFlag = true;
                         throw(errFlag);
                    }
               }

               catch (bool myErr)
               {
                    cout << "\tHa ingresado una opción inválida, por favor intente de nuevo" << endl
                         << "\tIngrese una opción válida:" << endl
                         << "\t\t-> ";
               }
          }
     }

     return 0;
}