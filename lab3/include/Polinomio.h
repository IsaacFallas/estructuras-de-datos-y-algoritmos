#pragma once
#include <iostream>
#include <string>
#include <cstring>
#include <vector>

using namespace std;

class Polinomio
{
private:
    float a; /**<Término de grado cuatro del polinomio*/
    float b; /**<Término de grado tres del polinomio*/
    float c; /**<Término de grado dos del polinomio*/
    float d; /**<Término de grado uno del polinomio*/
    float e; /**<Término de grado cero del polinomio*/

public:
    /**
    * @brief Constructor por defecto de la clase.
    */
    Polinomio();
    /**
    * @brief Constructor de la clase.
    * @param c - Término de grado dos del polinomio
    * @param d - Término de grado uno del polinomio
    * @param e - Término de grado cero del polinomio
    */
    Polinomio(float c, float d, float e);
    /**
    * @brief Función que sobrecarga el operador de suma para el objeto de la clase Polinomio.
    * @param &obj - Rerencia constante a un objeto de la clase Polinomio.
    * @return Objeto de la clase Polinomio.
    */
    Polinomio operator+(Polinomio const &obj);
    /**
    * @brief Función que sobrecarga el operador de resta para el objeto de la clase Polinomio.
    * @param &obj - Rerencia constante a un objeto de la clase Polinomio.
    * @return Objeto de la clase Polinomio.
    */
    Polinomio operator-(Polinomio const &obj);
    /**
    * @brief Función que sobrecarga el operador de multiplicación para el objeto de la clase Polinomio.
    * @param &obj - Rerencia constante a un objeto de la clase Polinomio.
    * @return Objeto de la clase Polinomio.
    */
    Polinomio operator*(Polinomio const &obj);
    /**
    * @brief Función que sobrecarga el operador de división para el objeto de la clase Polinomio.
    * @param &obj - Rerencia constante a un objeto de la clase Polinomio.
    * @return Objeto de la clase Polinomio.
    */
    Polinomio operator/(Polinomio const &obj);
    /**
    * @brief Se declara la clase ostream como amiga de la clase Polinomio; se sobrecarga el operador de salida para el objeto de la clase Polinomio.
    * @parm &os - Referencia a un objeto de la clase ostream.
    * @parm &obj - Referencia a un objeto de la clase Polinomio.
    */
    friend ostream &operator<<(ostream &os, Polinomio &obj);
    /**
    * @brief Se declara la clase istream como amiga de la clase Polinomio; se sobrecarga el operador de entrada para el objeto de la clase Polinomio.
    * @parm &is - Referencia a un objeto de la clase istream.
    * @parm &obj - Referencia a un objeto de la clase Polinomio.
    */
    friend istream &operator>>(istream &is, Polinomio &obj);
};
