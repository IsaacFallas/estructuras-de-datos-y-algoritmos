#pragma once
#include <iostream>
#include <string>
#include <cstring>

using namespace std;

class Fraccion
{
private:
    float num; /**<Numerador de la fracción*/
    float dem; /**<Denominador de la fracción*/

public:
    /**
    * @brief Constructor por defecto de la clase.
    */
    Fraccion();
    /**
    * @brief Constructor de la clase.
    * @param num - Numerador de la fracción.
    * @param dem - Denominador de la fracción.
    */
    Fraccion(float num, float dem);
    /**
    * @brief Función que sobrecarga el operador de suma para el objeto de la clase fracción.
    * @param &obj - Rerencia constante a un objeto de la clase Fraccion.
    * @return Objeto de la clase Fraccion.
    */
    Fraccion operator+(Fraccion const &obj);
    /**
    * @brief Función que sobrecarga el operador de resta para el objeto de la clase Fraccion.
    * @param &obj - Rerencia constante a un objeto de la clase Fraccion.
    * @return Objeto de la clase Fraccion.
    */
    Fraccion operator-(Fraccion const &obj);
    /**
    * @brief Función que sobrecarga el operador de multiplicación para el objeto de la clase Fraccion.
    * @param &obj - Rerencia constante a un objeto de la clase Fraccion.
    * @return Objeto de la clase Fraccion.
    */
    Fraccion operator*(Fraccion const &obj);
    /**
    * @brief Función que sobrecarga el operador de división para el objeto de la clase Fraccion.
    * @param &obj - Rerencia constante a un objeto de la clase Fraccion.
    * @return Objeto de la clase Fraccion.
    */
    Fraccion operator/(Fraccion const &obj);
    /**
    * @brief Se declara la clase ostream como amiga de la clase Fraccion; se sobrecarga el operador de salida para el objeto de la clase Fraccion.
    * @parm &os - Referencia a un objeto de la clase ostream.
    * @parm &obj - Referencia a un objeto de la clase Fraccion.
    */
    friend ostream &operator<<(ostream &os, Fraccion &obj);
    /**
    * @brief Se declara la clase istream como amiga de la clase Fraccion; se sobrecarga el operador de entrada para el objeto de la clase Fraccion.
    * @parm &is - Referencia a un objeto de la clase istream.
    * @parm &obj - Referencia a un objeto de la clase Fraccion.
    */
    friend istream &operator>>(istream &is, Fraccion &obj);
};
