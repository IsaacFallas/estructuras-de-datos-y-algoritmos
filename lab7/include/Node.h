#pragma once
#include <iostream>

using namespace std;

/** @brief Clase utilizada para crear un nodo en una lista enlazada.
 * @author Esteban Rodríguez
 * @author Jorge Fallas
 * @author Gabriel Gutiérrez
 */

class Node{
    public:
        char data;  /**<Dato que se guarda en el elemento de la lista*/
        Node* next; /**<Puntero al siguiente elemento de la list*/
};