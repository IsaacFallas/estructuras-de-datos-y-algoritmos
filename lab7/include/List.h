#pragma once
#include <iostream>
#include "Node.h"

using namespace std;

/** @brief Clase que implementa una lista enlazada y sus métodos
 * @author Esteban Rodríguez
 * @author Jorge Fallas
 * @author Gabriel Gutiérrez
 */

class List{
    private:
        Node* head; /**<Puntero del objeto de la clase Node para apuntar al primer elemento de la lista*/

    public:
        /** 
         * @brief Constructor por defecto de la clase; inicializa una lista vacía.
        */
        List();
        /**
         * @brief Constructor de la clase; inicializa una lista con el primer elemento.
         * @param data - Dato de tipo char que será el primer elemento de la lista.
         */
        List(char data);
        /**
         * @brief Destructor por defecto de la clase.
         */
        ~List();
        /**
         * @brief Método para imprimir el primer elemento de la lista
         * @param n - Puntero de tipo Node al primer elemento de la lista.
         */
        void printList(Node* n);
        /**
         * @brief Método para obtener el puntero al primer elemento de la lista.
         * @return Puntero de tipo Node.
         */
        Node* get_head();
        /**
         * @brief Método para agregar un elemento al final de la lista.
         * @param letter - Elemento que va a ser agregado al final de la lista.
         * @param n - Puntero de tipo Node que apunta al primer elemento de la lista.
         */
        void push_back(char letter, Node* n);
        /**
         * @brief Método para eliminar un elemento al final de la lista.
         * @param n - Puntero de tipo Node que apunta al primer elemento de la lista.
         * @return Dato de tipo char que fue eliminado de la lista.
         */
        char pop_back(Node* n);
        /**
         * @brief Método para agregar un elemento al inicio de la lista.
         * @param letter - Elemento que va a ser agregado al inicio de la lista.
         * @param n - Puntero de tipo Node que apunta al primer elemento de la lista.
         */
        void push_front(char letter, Node* n);
        /**
         * @brief Método para eliminar un elemento al inicio de la lista.
         * @param n - Puntero de tipo Node que apunta al primer elemento de la lista.
         * @return Dato de tipo char que fue eliminado de la lista.
         */
        char pop_front(Node* n);
        /**
         * @brief Método estático para revisar si los paréntesis están balanceados; sólo saca los elementos por el final como un stack.
         * @param list - Objeto de la clase List
         */
        static void check_syntax(List list);
        /**
         * @brief Método estático para revisar si una palabra es palíndromo; saca los elementos por el final y el inicio como un queue.
         * @param list - Objeto de la clase List
         */
        static void check_word(List list);
        /**
         * @brief Método para obtener la longitud de la lista
         * @param n - Puntero de tipo Node que apunta al primer elemento de la lista.
         */
        int get_len(Node *n);
};