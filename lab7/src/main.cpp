#include <iostream>
#include <string>
#include "List.h"

using namespace std;

int main(int argc, char const *argv[])
{
    /*char a = 'A';
    List list(a);
    Node* head = list.get_head();
    list.printList(head);
    list.push_back('B',head);
    list.push_back('C',head);
    list.printList(head);
    char var1 = list.pop_back(head);
    cout<<var1<<endl;
    list.printList(head);
    list.push_front('Z', head);
    list.push_front('Y', head);
    list.printList(head);
    char var2 = list.pop_front(head);
    cout<<var2<<endl;
    list.printList(head);*/
    int chioce;
    cout << "Bienvenido"<< endl;
    cout << "Seleccione una de las dos opciones del programa"<< endl;
    cout << "1. Verificación de paréntesis balanceados."<< endl;
    cout << "2. Verificación de palíndromo"<< endl;
    cin >> chioce;
    if (chioce == 1)
    {
        cout << "Ingrese una línea de texto en el formato: (algo(algo)..))" << endl;
        string check;
        cin >> check;
        List stack(check[0]);
        Node *n = stack.get_head();
        for (size_t i = 1; i < check.length(); i++)
        {
            stack.push_front(check[i],n);
        }
        List::check_syntax(stack);
    }
    else if (chioce == 2)
    {
        cout << "Ingrese una palabra palindrómica" << endl;
        string check;
        cin >> check;
        List queue(check[0]);
        Node *n = queue.get_head();
        //queue.printList(n);
        //cout << endl;
        for (size_t i = 1; i <= check.length(); i++)
        {
            queue.push_front(check[i],n);
        }
        //queue.printList(n);
        List::check_word(queue);
    }
    else
    {
        cout << "No seleccionó una opción válida, termina el programa." << endl;
    }
}

