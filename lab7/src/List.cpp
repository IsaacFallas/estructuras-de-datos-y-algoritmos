#include "List.h"

List::List(){
    this->head = NULL;
}

List::List(char data){
    Node* first = new Node();
    first->data = data;
    first->next = NULL;
    this->head = first;
}

List::~List(){
    //Does nothing
}

void List::printList(Node* n){
    while (n != NULL)
    {
        cout<<n->data<<" ";
        n = n->next;
    }
    cout<<endl;
}

/* Node* List::Node::getHead...
Así hubiera tenido que ponerse si se hubieran anidado las clases.
*/

Node* List::get_head(){
    return this->head;
}

void List::push_back(char letter, Node* n)
{
    while (n->next != NULL) 
    {
        n = n->next;
    }
    Node* tail = new Node();
    tail->data = letter;
    tail->next = NULL;
    n->next = tail;
}

char List::pop_back(Node* n){
    if (n->next == NULL)
    {
        char var = n->data;
        delete n;
        return var;
    }
    else
    {
        Node* prev = new Node();
        while (n->next != NULL)
        {
            prev = n;
            n = n->next;
        }
        char var = n->data;
        delete (prev->next);
        prev->next = NULL;
        n->next = prev;
        return var;
    }
}

void List::push_front(char letter, Node *n){
    Node* aux = new Node();
    aux->data = n->data;
    aux->next = n->next;
    n->data = letter;
    n->next = aux;
}

char List::pop_front(Node *n){
    Node* aux = n->next;
    n->data = aux->data;
    n->next = aux->next;
    char var = aux->data;
    delete aux;
    return var;
}

void List::check_syntax(List lista){
    Node* n = lista.get_head();
    int l = lista.get_len(n);
    int count1 = 0;
    int count2 = 0;
    for (int i = 0; i < l; i++)
    {
        char var = lista.pop_back(n);
        if (var == '(')
        {
            count1++;
        }
        else if (var == ')')
        {
            count2++;
        }
    }
    if (count1 == count2)
    {
        cout << "Los paréntesis están balanceados" << endl;
    }
    else
    {
        cout << "Los paréntesis no están balanceados" << endl;
    }
}

void List::check_word(List lista){

bool palindromo = false;
Node *n = lista.get_head();
char data_head;
char data_tail;

for (int i = 0; i < lista.get_len(n); i++)
{
    
    data_head = lista.pop_front(n);
    data_tail = lista.pop_back(n);

    //cout << data_head << endl;
    //cout << data_tail << endl;
    //cout << endl;

    if (data_head == data_tail)
    {
        palindromo = true;

    }else
    {
        palindromo = false;
        break;
    }
    
}
if (palindromo)
{
    cout << "La palabra ingresada es un palíndromo" << endl;
}else
{
    cout << "La palabra ingresada no es un palíndromo" << endl;
}

}

int List::get_len(Node *n){
    int length = 0;
    while (n != NULL)
    {
        n = n->next;
        length ++;
    }
    return length;
}