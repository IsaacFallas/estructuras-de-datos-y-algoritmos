/**
 * @file lab0.cpp
 * @author Jorge Isaac Fallas Mejía B62562.
 * @author Esteban Rodríguez Quintana B66076.
 * @author Gabrile Gutierres Arguedas B63215.
 * @brief  Programa escrito en c++. La funcionalidad del programa es la de recibir una cadena de codónes al ser ejecutado, con el fin de determinar cuales son los aminoácidos que componen dicha cadena. En primer lugar se compila el código utilizando g++. Al ejecutar el binario se debe de agregar la cadena de codones en mayuscula y con una cantidad de letras multiplo de 3.
 * @version 1
 * @date 2020-01-7
 */

#include <string>
#include <iostream>



/**
 * @brief En el main se ejecuta toda la lógia del programa y es el encargado de realizar las impresiones en patalla.
 * @param Se recibe como parámetro la cadena de codones en el puntero argv[] en la ejecución del binario.
*/

int main(int argc, char* argv[]){

std::string str = argv[1];

char codones [100];

if (str.length() % 3 == 1)
{
    printf("\nPor favor, introduzca una cadena múltiplo de tres\n\n");
}else
{
int cont = 0;
for(int i = 0; i  < str.length(); i++){

    if (argv[1][i] == 'G')
    {
        if (argv[1][i+1] == 'U')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C' || argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'V';
                cont++;
                i = i + 2;
            } else {
                i = i + 2;
            }
        } else {

            if (argv[1][i+1] == 'C')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C' || argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'A';
                cont++;
                i = i + 2;
            } else {
                i = i + 2;
            }
        } else {
             if (argv[1][i+1] == 'A')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C')
            {
                codones[cont] = 'D';
                cont++;
                i = i + 2;
            } else {
                if (argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'E';
                cont++;
                i = i + 2;
            } else
            {
                i = i + 2;
            }
            }



        } else {
             if (argv[1][i+1] == 'G')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C' || argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'G';
                cont++;
                i = i + 2;
            } else {
                i = i + 2;
            }
        } else {
            i = i + 2;
        }

    }
        }
        }

        }else
        {
            if (argv[1][i] == 'U')
    {
            if (argv[1][i+1] == 'U')
            {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C')
            {
                codones[cont] = 'F';
                cont++;
                i = i + 2;
            } else {
                if (argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'L';
                cont++;
                i = i + 2;
            } else
            {
                i = i + 2;
            }
            }


        } else {
            if (argv[1][i+1] == 'C')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C' || argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'S';
                cont++;
                i = i + 2;
            } else {
                i = i + 2;
            }
        } else {
               if (argv[1][i+1] == 'A')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C')
            {
                codones[cont] = 'Y';
                cont++;
                i = i + 2;
            } else {
                if (argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                i = i + 2;
            }
            }
        } else {
            if (argv[1][i+1] == 'G')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C')
            {
                codones[cont] = 'C';
                cont++;
                i = i + 2;
            } else {
                if (argv[1][i+2] == 'G')
            {
                codones[cont] = 'W';
                cont++;
                i = i + 2;
            } else
            {
                if (argv[1][i+2] == 'A')
            {
                i = i + 2;
            }
            }
            }
        }
        }
    }
        }
        } else
    {
        if (argv[1][i] == 'C')
    {
        if (argv[1][i+1] == 'U')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C' || argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'L';
                cont++;
                i = i + 2;
            } else {
                i = i + 2;
            }
        } else {
            if (argv[1][i+1] == 'C')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C' || argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'P';
                cont++;
                i = i + 2;
            } else {
                i = i + 2;
            }
        } else {
            if (argv[1][i+1] == 'A')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C')
            {
                codones[cont] = 'H';
                cont++;
                i = i + 2;
            } else {
                if (argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'Q';
                cont++;
                i = i + 2;
            } else
            {
                i = i + 2;
            }
            }
        } else {
            if (argv[1][i+1] == 'G')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C' || argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'R';
                cont++;
                i = i + 2;
            } else {
                i = i + 2;
            }
        } else {
            i = i + 2;
        }
        }
        }
        }

    } else
    {
        if (argv[1][i] == 'A')
    {
        if (argv[1][i+1] == 'U')
        {
            if (argv[1][i+2] == 'A' || argv[1][i+2] == 'U' || argv[1][i+2] == 'C')
            {
                codones[cont] = 'I';
                cont++;
                i = i + 2;
            } else {
                if (argv[1][i+2] == 'G')
            {
                codones[cont] = 'M';
                cont++;
                i = i + 2;
            } else
            {
                i = i + 2;
            }
            }
        } else {
            if (argv[1][i+1] == 'C')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C' || argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'T';
                cont++;
                i = i + 2;
            } else {
                i = i + 2;
            }
        } else {
            if (argv[1][i+1] == 'A')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C')
            {
                codones[cont] = 'N';
                cont++;
                i = i + 2;
            } else {
                if (argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'K';
                cont++;
                i = i + 2;
            } else
            {
                i = i + 2;
            }
            }

        } else {
              if (argv[1][i+1] == 'G')
        {
            if (argv[1][i+2] == 'U' || argv[1][i+2] == 'C')
            {
                codones[cont] = 'S';
                cont++;
                i = i + 2;
            } else {
                if (argv[1][i+2] == 'A' || argv[1][i+2] == 'G')
            {
                codones[cont] = 'R';
                cont++;
                i = i + 2;
            } else
            {
                i = i + 2;
            }
            }


        } else {
            i = i + 2;
        }
        }
        }
        }

    } else
    {
        i = i + 2;
    }
    }
    }
        }

    }

    printf("\n");
// Impresión en terminal de los aminoácidos encontrados:
for (int i = 0; i < cont; i++)
{
    std::cout << codones[i];
}

    printf("\n");
    printf("\n");

}

}
