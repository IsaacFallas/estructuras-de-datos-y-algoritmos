 ##
  # \file lab0.py
  # \author Jorge Isaac Fallas Mejía B62562.
  # \author Esteban Rodríguez Quintana B66076.
  # \author Gabrile Gutierres Arguedas B63215.
  # \brief  Programa escrito en python. La funcionalidad del programa es la de recibir una cadena de codónes al ser ejecutado, con el fin de determinar cuales son los aminoácidos que componen dicha cadena. Al ejecutar el script se debe de agregar la cadena de codones en mayuscula y con una cantidad de letras multiplo de 3.
  # \version 1
  # \date 2020-01-7
 ##

import sys

##
 # \brief La función parametros() es la que permite leer una entrada en la ejecución del script de python. Lo anterior se logra mediante la utilizació de la librería sys importada al inicio del script.
 # \param No se reciben parámetros
 # \return Retorna dos datos. Retorna lista con los codones ingresados. Retorna un booleano que indica si la cantidad de letras en la cadena de codones sea un múltiplo de 3.
##
def parametros():
    codones = sys.argv[1]
    lista = list(codones)
    revisar = True
    if len(lista) % 3 == 1:
        revisar = False
    else:
        revisar = True
    return lista,revisar

##
 # \brief Esta es la función encargada de realizar el recorrido de la lista que recibe para determinar los aminoácidos que componen la cadena de codones.
 # \param Recibe una lista y un booleano.
 # \return Retorna un string en el cual se encuentran expresos símbolos de los aminoácidos que componen la cadena que entra en la lista de codones.
##
def cadenas(lista,revisar):
    if revisar == False:
        print("Por favor introduzca una cadena múltiplo de 3")
    else:
        codones = list()
        aminoacido = list()
        for i in range(0,len(lista),3):
            codones.append(lista[i:i+3])
        for j in range(len(codones)):
            #Casos que empiezan con U
            if codones[j][0]=='U':
                if codones[j][1]=='U':
                    if codones[j][2]=='U' or codones[j][2]=='C':
                        aminoacido.append('P')
                    if codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('L')
                if codones[j][1]=='C':
                    if codones[j][2]=='U' or codones[j][2]=='C' or codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('S')
                if codones[j][1]=='A':
                        if codones[j][2]=='U' or codones[j][2]=='C':
                            aminoacido.append('T')
                        if codones[j][2]=='A' or codones[j][2]=='G':
                            print("El proceso no forma aminoacido en este caso")
                if codones[j][1]=='G':
                        if codones[j][2]=='U' or codones[j][2]=='C':
                            aminoacido.append('C')
                        if codones[j][2]=='A':
                            print("El proceso no forma aminoacido en este caso")
                        if codones[j][2]=='G':
                            aminoacido.append('T')
            #Casos que empiezan con C
            if codones[j][0]=='C':
                if codones[j][1]=='U':
                    if codones[j][2]=='U' or codones[j][2]=='C' or codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('L')
                if codones[j][1]=='C':
                    if codones[j][2]=='U' or codones[j][2]=='C' or codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('P')
                if codones[j][1]=='A':
                    if codones[j][2]=='U' or codones[j][2]=='C':
                        aminoacido.append('H')
                    if codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('G')
                if codones[j][1]=='G':
                    if codones[j][2]=='U' or codones[j][2]=='C' or codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('A')
            #Casos que empiezan con A
            if codones[j][0]=='A':
                if codones[j][1]=='U':
                    if codones[j][2]=='U' or codones[j][2]=='C' or codones[j][2]=='A':
                        aminoacido.append('I')
                    if codones[j][2]=='G':
                        aminoacido.append('M')
                if codones[j][1]=='C':
                    if codones[j][2]=='U' or codones[j][2]=='C' or codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('T')
                if codones[j][1]=='A':
                    if codones[j][2]=='U' or codones[j][2]=='C':
                        aminoacido.append('A')
                    if codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('L')
                if codones[j][1]=='G':
                    if codones[j][2]=='U' or codones[j][2]=='C':
                        aminoacido.append('S')
                    if codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('A')
            #Casos que empiezan con G
            if codones[j][0]=='G':
                if codones[j][1]=='U':
                    if codones[j][2]=='U' or codones[j][2]=='C' or codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('V')
                if codones[j][1]=='C':
                    if codones[j][2]=='U' or codones[j][2]=='C' or codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('A')
                if codones[j][1]=='A':
                    if codones[j][2]=='U' or codones[j][2]=='C':
                        aminoacido.append('A')
                    if codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('G')
                if codones[j][1]=='G':
                    if codones[j][2]=='U' or codones[j][2]=='C' or codones[j][2]=='A' or codones[j][2]=='G':
                        aminoacido.append('G')
        amino = "".join(aminoacido)
        return amino

##
 # \brief Función principal de main del script donde se llama a las funciones creadas.
##
def main():
    lista,revisar = parametros()
    aminoacido = cadenas(lista,revisar)
    print("La cadena de aminoacido es:",aminoacido)
if __name__ == '__main__':
    main()
