#include "Lancer.h"

Lancer::Lancer() : Unit()
{
}

Lancer::Lancer(int id, char type, string name, int maxHitPoints, int hitPoints, int attack, int defense, int range, int level, int experience, int movement, int posX, int posY, int cost, int idCount) : Unit(id, type, name, maxHitPoints, hitPoints, attack, defense, range, level, experience, movement, posX, posY, cost, idCount)
{
}

bool Lancer::move(Cell &cell, int x, int y)
{

  int posX = get_posX(); //método obtener posX
  int posY = get_posY(); //método obtener posY

  if ((((x==posX-1)&&(y==posY)) || ((x==posX)&&(y==posY+1)) || ((x==posX+1)&&(y==posY)) || ((x==posX)&&(y==posY-1))) && (!(cell.get_passable())))
  {

    setPosX(x);
    setPosY(y);
    

    return true;
  }
  else
  {

    return false;
  }
}

Lancer::~Lancer()
{
}

bool Lancer::attack(int x, int y, Cell &cell)
{
  int posX = get_posX();
  int posY = get_posY();
  if ((((x == posX - 1) && (y == posY)) || ((x == posX) && (y == posY + 1)) || ((x == posX + 1) && (y == posY)) || ((x == posX) && (y == posY - 1))) || (((x == posX + 1) || (x == posX - 1)) && ((y == posY + 1) || (y == posY - 1))))
  {
      bool checkCell = cell.get_passable();
      if (checkCell == true)
      {
        Unit *unit = cell.getUnit();
        int unitAttack = get_attack();
        int foeDefense = (*(unit)).get_defense();
        int Hit = unitAttack - foeDefense;
        cout<<unitAttack<<endl;
        cout<<foeDefense<<endl;
        cout<<Hit<<endl;
        if (Hit <= 0)
        {
          cout << "El ataque no fue efectivo" << endl;
          return true;
        }
        else
        {
          int foeHitPoints = (*(unit)).get_hitPoints();
          int damage = foeHitPoints - Hit;
          (*(unit)).setHitPoints(damage);
          if (damage <= 0)
          {
            cell.setPassable(false);
            cell.setArmy(0);

            setExperience(get_experience() + 1);

            if ((get_experience()) == 2 * (get_level()))
            {
              setMaxHitPoints(int((0.25 * (get_maxHitPoints())) + (get_maxHitPoints())));
              setHitPoints(int((0.25 * (get_hitPoints())) + (get_hitPoints())));
              setAttack(int((0.25 * (get_attack()) + (get_attack()))));
              setDefense(int((0.25 * (get_defense()) + (get_defense()))));
              setLevel((get_level()) + 1);
              setExperience(0);
            }
            else
            {
              return true;
            }
          }
          else
          {
            return true;
          }
        }
      }
      else
      {
        cout << "No se encuentra ninguna unidad en esta celda" << endl;
        return false;
      }
    }
    else
    {
      return false;
    }
}