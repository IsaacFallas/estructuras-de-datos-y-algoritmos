#include "Cell.h"
#include "Unit.h"

Cell::Cell()
{
    this->passable = false;
    this->army = 0;
}

Cell::Cell(bool passable, Unit* army)
{
    this->passable = passable;
    this->army = army;
}

void Cell::show_cell(Cell c){
    
    cout << "       ID: " <<c.army->get_id() << endl
         << "       TYPE: " <<c.army->get_type() << endl
         << "       NAME: " <<c.army->get_name() << endl
         << "       MAX HIT POINTS: " <<c.army->get_maxHitPoints() << endl
         << "       HIT POINTS: " <<c.army->get_hitPoints() << endl
         << "       ATTACK: " <<c.army->get_attack() << endl
         << "       DEFENSE: " <<c.army->get_defense() << endl
         << "       RANGE: " <<c.army->get_range() << endl
         << "       LEVEL: " <<c.army->get_level() << endl
         << "       EXP: " <<c.army->get_experience() << endl
         << "       MOVEMENT: " <<c.army->get_movement() << endl
         << "       POS X: " <<c.army->get_posX() << endl
         << "       POS y: " <<c.army->get_posY() << endl
         << "       COST: " <<c.army->get_cost() << endl
         << "       ID COUNT: " <<c.army->get_idcost() << endl;

}

Cell::~Cell(){
    
}

bool Cell::get_passable()
{
    return this->passable;
};

void Cell::setPassable(bool pass){
    this->passable = pass;
}

Unit* Cell::getUnit(){
    return this->army;

}

void Cell::setArmy(Unit* army){

    this->army = army;


}