#include "Unit.h"
#include "Cell.h"

Unit::Unit(){

    this->id = 0;
    this->type = ' ';
    this->name = " ";
    this->maxHitPoints = 0;
    this->hitPoints = 0;
    this->attacke = 0;
    this->defense = 0;
    this->range = 0;
    this->level = 0;
    this->experience = 0;
    this->movement = 0;
    this->posX = 0;
    this->posY = 0;
    this->cost = 0;
    this->idCount = 0;

}

Unit::Unit(int id, char type, string name, int maxHitPoints, int hitPoints, int attacke, int defense, int range, int level, int experience, int movement, int posX, int posY, int cost, int idCount){
                                                                                                                                             
    this->id = id;
    this->type = type;
    this->name = name;
    this->maxHitPoints = maxHitPoints;
    this->hitPoints = hitPoints;
    this->attacke = attacke;
    this->defense = defense;
    this->range = range;
    this->level = level;
    this->experience = experience;
    this->movement = movement;
    this->posX = posX;
    this->posY = posY;
    this->cost = cost;
    this->idCount = idCount;


}

void Unit::setId(int id)
{

    this->id = id;

 }

 void Unit::setType(char type)
 {

     this->type = type;

 }

 void Unit::setName(string name)
 {

     this->name = name;

 }

 void Unit::setMaxHitPoints(int maxHitPoints)
 {

     this->maxHitPoints = maxHitPoints;

 }

 void Unit::setHitPoints(int hitPoints)
 {

     this->hitPoints = hitPoints;

 }

 void Unit::setAttack(int attacke)
 {

     this->attacke = attacke;

 }

 void Unit::setDefense(int defense)
 {

     this->defense = defense;

 }

 void Unit::setRange(int range)
 {

     this->range = range;
 }

 void Unit::setLevel(int level)
 {

     this->level = level;

 }

 void Unit::setExperience(int experience)
 {

     this->experience = experience;

 }

 void Unit::setMovement(int movement)
 {

     this->movement = movement;

 }

 void Unit::setPosX(int posX)
 {

     this->posX = posX;

 }

 void Unit::setPosY(int posY)
 {

     this->posY = posY;

 }

 void Unit::setCost(int cost)
 {

     this->cost = cost;

 }

 void Unit::setIdCount(int idCount)
 {

     this->idCount = idCount;

 }

Unit::~Unit(){


}

int Unit::get_id()
{
    return this->id;
}

char Unit::get_type()
{
    return this->type;
}

string Unit::get_name(){
    return this->name;
}

int Unit::get_maxHitPoints()
{
    return this->maxHitPoints;
}

int Unit::get_hitPoints()
{
    return this->hitPoints;
}

int Unit::get_attack()
{
    return this->attacke;
}

int Unit::get_defense()
{
    return this->defense;
}

int Unit::get_range()
{
    return this->range;
}

int Unit::get_level()
{
    return this->level;
}

int Unit::get_experience()
{
    return this->experience;
}

int Unit::get_movement()
{
    return this->movement;
}

int Unit::get_posX()
{
    return this->posX;
}

int Unit::get_posY()
{
    return this->posY;
}

int Unit::get_cost(){
    return this->cost;
}

int Unit::get_idcost(){
    return this->idCount;
}
