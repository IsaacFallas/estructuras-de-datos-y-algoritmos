#include "Player.h"

Player::Player(){
    this->maxCost = 0;
    this->playfield = 0; 
    this->name = "Null";
    this->score = 0;
}

Player::Player(int maxCost, Field *playfield, string name, int score){
    this->maxCost = maxCost; 
    this->playfield = playfield;
    this->name = name;
    this->score = score;
}

void Player::set_maxCost(int maxCost){
    this->maxCost = maxCost;
}

void Player::set_army(vector<Unit*> army){
    this->army = army;
}

void Player::set_playfield(Field *playfield){
    this->playfield = playfield;
}

void Player::set_name(string name){
    this->name = name;
}

void Player::set_score(int score){
    this->score = score;
}


int Player::get_maxCost(){

return this->maxCost;

}

vector<Unit*> Player::get_army()
{

    return this->army;

}

Field* Player::get_playfield(){

return this->playfield;

}

string Player::get_name(){

return this->name;

}

int Player::get_score(){

return this->score;

}

Player::~Player(){
    //Does nothing
}

    void Player::createArmy(int lancer, int cavalry, int archer)
{
    int L_cost = 3;
    int C_cost = 5;
    int A_cost = 4;
    int total_cost = (L_cost * lancer) + (C_cost * cavalry) + (A_cost * archer);
    int maxCost = this->maxCost;
    while (maxCost < total_cost)
    {
        string L;
        string C;
        string A;
        cout << "No puede construir su ejército de esta manera, ya que sobrepasa su presupuesto" << endl;
        cout << "Su presupuesto es de " << maxCost << endl;
        cout << "Introduzca la cantida de Lancer" << endl;
        cin >> L;
        lancer = stoi(L);
        cout << "Introduzca la cantida de Cavalry" << endl;
        cin >> C;
        cavalry = stoi(C);
        cout << "Introduzca la cantida de Archer" << endl;
        cin >> A;
        archer = stoi(A);
        total_cost = (L_cost * lancer) + (C_cost * cavalry) + (A_cost * archer);
    }
    int total_units = lancer + cavalry + archer;
    int count = 0;
    while (count < lancer)
    {
        int val = 0;
        string x;
        string y;
        cout<<"¿Dónde quiere colocar este Lancer?"<<endl;
        cout<<"Coordenada x: ";
        cin >> x;
        cout<<"Coordenada y: ";
        cin >> y;
        int X = stoi(x);
        int Y = stoi(y);
        string num = to_string(count + 1);
        string str = "Lancer" + num;
        army.push_back(new Lancer((count + 1), 'L', str, 20, 20, 7, 7, 1, 1, 0, 1, X, Y, 3, 10));
        count++;
        val++;
        int range = army.size();
        Unit* Army;
        Army = &(*(army[range - 1]));
        Cell &cell = ((*(playfield)).getCell(X, Y));
        cell.setPassable(true);
        cell.setArmy(Army);
    }
    while (count < (lancer + cavalry))
    {
        int val = 1;
        string x;
        string y;
        cout << "¿Dónde quiere colocar este Cavalry?" << endl;
        cout << "Coordenada x: ";
        cin >> x;
        cout << "Coordenada y: ";
        cin >> y;
        int X = stoi(x);
        int Y = stoi(y);
        string num = to_string(val);
        string str = "Cavalry" + num;
        army.push_back(new Cavalry(val, 'C', str, 25, 25, 15, 5, 1, 1, 0, 3, X, Y, 5, 10));
        count++;
        int range = army.size();
        Unit *Army;
        Army = &(*(army[range - 1]));
        Cell &cell = ((*(playfield)).getCell(X, Y));
        cell.setPassable(true);
        cell.setArmy(Army);
    }
    while (count < total_units)
    {
        int val = 1;
        string x;
        string y;
        cout << "¿Dónde quiere colocar este Archer?" << endl;
        cout << "Coordenada x: ";
        cin >> x;
        cout << "Coordenada y: ";
        cin >> y;
        int X = stoi(x);
        int Y = stoi(y);
        string num = to_string(val);
        string str = "Archer" + num;
        army.push_back(new Archer(val, 'A', str, 15, 15, 5, 5, 3, 1, 0, 1, X, Y, 4, 10));
        count++;
        int range = army.size();
        Unit *Army;
        Army = &(*(army[range - 1]));
        Cell &cell = ((*(playfield)).getCell(X, Y));
        cell.setPassable(true);
        cell.setArmy(Army);
    }
}

void Player::play(){


cout << "-------------new turn-------------" << endl;
cout << "-------------" << this->name << "-------------" << endl;

vector<Unit*> army = this->army;

int x;
int y;
int x_New;
int y_New;
char input;
bool move;
bool attack;

for (size_t i = 0; i < army.size(); i++)
{
    
    cout << "Unit " << (*(army[i])).get_name() << " (" << i << ") :"<< endl;
    cout << "move?" << endl;
    cin >> input;

    if (input == 's' || input == 'S')
    {
        cout << "where?";
        cout << "x: ";
        cin >> x_New;
        cout << "y: ";
        cin >> y_New;

        x = (*(army[i])).get_posX();
        y = (*(army[i])).get_posY();

        Cell &celda = (*(this->playfield)).getCell(x_New,y_New); 
        Cell &celda1 = (*(this->playfield)).getCell(x,y);

        move = (*(army[i])).move(celda,x_New,y_New);
        
        while (!move)
        {
            cout << "No se puede mover a esa posición, ingrese una nueva posición" << endl;
            cout << "where?";
            cout << "x: ";
            cin >> x_New;
            cout << "y: ";
            cin >> y_New;
            celda = (*(this->playfield)).getCell(x_New,y_New);
            move = (*(army[i])).move(celda,x_New,y_New);


        }

        (celda).setPassable(true);
        (celda).setArmy(army[i]);
        (celda1).setPassable(false);
        (celda1).setArmy(0);
        
        
    }

    cout << "attack?" << endl;
    cin >> input;

    if (input == 's' || input == 'S')
    {
        cout << "where?";
        cout << "x: ";
        cin >> x;
        cout << "y: ";
        cin >> y;

        Cell &celda2 = (*(this->playfield)).getCell(x,y);
    

        attack = (*(army[i])).attack(x,y,celda2);

        while (!attack)
        {
        cout << "No se puede atacar en esa posición, ingrese una nueva posición" << endl;
        cout << "where?";
        cout << "x: ";
        cin >> x;
        cout << "y: ";
        cin >> y;

        attack = (*(army[i])).attack(x,y,celda2);
            
            if (!celda2.get_passable())
            {
                army[i] = 0;
            }
            

        }
        
    }
    

}


}