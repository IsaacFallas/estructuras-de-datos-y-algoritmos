#include "Field.h"

Field::Field()
{
    this->rows = 10;
    this->cols = 10;
};

Field::~Field(){
    //Destrutor de la clase
};

Cell& Field::getCell(int x, int y)
{   
    //Cell &cell;
    return this->playfield[x][y]; 
    //return cell;
};

int Field::getRows()
{
    return this->rows;
};

int Field::getCols()
{
    return this->cols;
};

void Field::showField(){

    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            Cell &celda = getCell(i,j);
            Unit *unit = (celda).getUnit();
            bool passable = (celda).get_passable();
            if (passable != false)
            {
                cout << " " <<(*(unit)).get_type() << " ";

            }else
            {
                cout << " _ ";
            }
            
        
            
        }
        cout << endl;
    }
    

}