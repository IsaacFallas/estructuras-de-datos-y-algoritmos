#include "UI.h"

void UI::cargar(Player &P1, Player &P2, Field &field)
{
    string file1 = "P1.txt";
    char read1[1000];
    ifstream ifile1;
    ifile1.open(file1);
    while (!ifile1.eof())
    {
        ifile1.getline(read1, 1000);
        string s(read1);
        data1.push_back(s);
    }
    vector<Unit *> Army1;
    int army1 = stoi(data1[0]);
    int maxCost1 = stoi(data1[1]);
    P1.set_maxCost(maxCost1);
    for (int i = 0; i < army1; i += 15)
    {
        int id = stoi(data1[i + 2]);
        string s = data1[i + 3];
        char type = s[0];
        string name = data1[i + 4];
        int maxHitPoints = stoi(data1[i + 5]);
        int hitPoints = stoi(data1[i + 6]);
        int attack = stoi(data1[i + 7]);
        int defense = stoi(data1[i + 8]);
        int range = stoi(data1[i + 9]);
        int level = stoi(data1[i + 10]);
        int experience = stoi(data1[i + 11]);
        int movement = stoi(data1[i + 12]);
        int posX = stoi(data1[i + 13]);
        int posY = stoi(data1[i + 14]);
        int cost = stoi(data1[i + 15]);
        int idCount = stoi(data1[i + 16]);
        if (type == 'L')
        {
            Lancer lancer(id, type, name, maxHitPoints, hitPoints, attack, defense, range, level, experience, movement, posX, posY, cost, idCount);
            Army1.push_back(&lancer);
        }
        if (type == 'C')
        {
            Cavalry cavalry(id, type, name, maxHitPoints, hitPoints, attack, defense, range, level, experience, movement, posX, posY, cost, idCount);
            Army1.push_back(&cavalry);
        }
        if (type == 'A')
        {
            Archer archer(id, type, name, maxHitPoints, hitPoints, attack, defense, range, level, experience, movement, posX, posY, cost, idCount);
            Army1.push_back(&archer);
        }
    }
    P1.set_army(Army1);
    int num1 = data1.size();
    string playerName1 = data1[num1 - 1];
    P1.set_name(playerName1);
    int playerScore1 = stoi(data1[num1 - 2]);
    P1.set_score(playerScore1);

    string file2 = "P2.txt";
    char read2[1000];
    ifstream ifile2;
    ifile2.open(file2);
    while (!ifile2.eof())
    {
        ifile2.getline(read2, 1000);
        string s(read2);
        data2.push_back(s);
    }
    vector<Unit *> Army2;
    int army2 = stoi(data2[0]);
    int maxCost2 = stoi(data2[1]);
    P2.set_maxCost(maxCost2);
    for (int j = 0; j < army2; j += 15)
    {
        int id = stoi(data2[j + 2]);
        string s = data2[j + 3];
        char type = s[0];
        string name = data2[j + 4];
        int maxHitPoints = stoi(data2[j + 5]);
        int hitPoints = stoi(data2[j + 6]);
        int attack = stoi(data2[j + 7]);
        int defense = stoi(data2[j + 8]);
        int range = stoi(data2[j + 9]);
        int level = stoi(data2[j + 10]);
        int experience = stoi(data2[j + 11]);
        int movement = stoi(data2[j + 12]);
        int posX = stoi(data2[j + 13]);
        int posY = stoi(data2[j + 14]);
        int cost = stoi(data2[j + 15]);
        int idCount = stoi(data2[j + 16]);
        if (type == 'L')
        {
            Lancer lancer(id, type, name, maxHitPoints, hitPoints, attack, defense, range, level, experience, movement, posX, posY, cost, idCount);
            Army2.push_back(&lancer);
        }
        if (type == 'C')
        {
            Cavalry cavalry(id, type, name, maxHitPoints, hitPoints, attack, defense, range, level, experience, movement, posX, posY, cost, idCount);
            Army2.push_back(&cavalry);
        }
        if (type == 'A')
        {
            Archer archer(id, type, name, maxHitPoints, hitPoints, attack, defense, range, level, experience, movement, posX, posY, cost, idCount);
            Army2.push_back(&archer);
        }
    }
    P2.set_army(Army2);
    int num2 = data2.size();
    string playerName2 = data1[num2 - 1];
    P2.set_name(playerName2);
    int playerScore2 = stoi(data1[num2 - 2]);
    P2.set_score(playerScore2);
}

bool UI::guardar(Player &P1, Player &P2, Field &field)
{

    int id;
    char type;
    string name;
    int maxHitPoints;
    int hitPoints;
    int attack;
    int defense;
    int range;
    int level;
    int experience;
    int movement;
    int posX;
    int posY;
    static int cost;
    static int idCount;

    ofstream archivo;
    archivo.open("P1.txt", ios::out);
    int maxCost = P1.get_maxCost();
    vector<Unit *> army = P1.get_army();
    string name1 = P1.get_name();
    int score = P1.get_score();

    archivo << (sizeof(army) / sizeof(army[0])) << endl;
    archivo << maxCost << endl;

    for (size_t i = 0; i < (sizeof(army) / sizeof(army[0])); i++)
    {
        id = (*(army[i])).get_id();
        type = (*(army[i])).get_type();
        name = (*(army[i])).get_name();
        maxHitPoints = (*(army[i])).get_maxHitPoints();
        hitPoints = (*(army[i])).get_hitPoints();
        attack = (*(army[i])).get_attack();
        defense = (*(army[i])).get_defense();
        range = (*(army[i])).get_range();
        level = (*(army[i])).get_level();
        experience = (*(army[i])).get_experience();
        movement = (*(army[i])).get_movement();
        posX = (*(army[i])).get_posX();
        posY = (*(army[i])).get_posY();
        cost = (*(army[i])).get_cost();
        idCount = (*(army[i])).get_idcost();

        archivo << id << endl;
        archivo << type << endl;
        archivo << name << endl;
        archivo << maxHitPoints << endl;
        archivo << hitPoints << endl;
        archivo << attack << endl;
        archivo << defense << endl;
        archivo << range << endl;
        archivo << level << endl;
        archivo << experience << endl;
        archivo << movement << endl;
        archivo << posX << endl;
        archivo << posY << endl;
        archivo << cost << endl;
        archivo << idCount << endl;
    }

    archivo << name1 << endl;
    archivo << score << endl;
    archivo.close();

    ofstream archivo1;
    archivo1.open("P2.txt", ios::out);
    maxCost = P2.get_maxCost();
    army = P2.get_army();
    name1 = P2.get_name();
    score = P2.get_score();

    archivo1 << (sizeof(army) / sizeof(army[0])) << endl;
    archivo1 << maxCost << endl;

    for (size_t i = 0; i < (sizeof(army) / sizeof(army[0])); i++)
    {
        id = (*(army[i])).get_id();
        type = (*(army[i])).get_type();
        name = (*(army[i])).get_name();
        maxHitPoints = (*(army[i])).get_maxHitPoints();
        hitPoints = (*(army[i])).get_hitPoints();
        attack = (*(army[i])).get_attack();
        defense = (*(army[i])).get_defense();
        range = (*(army[i])).get_range();
        level = (*(army[i])).get_level();
        experience = (*(army[i])).get_experience();
        movement = (*(army[i])).get_movement();
        posX = (*(army[i])).get_posX();
        posY = (*(army[i])).get_posY();
        cost = (*(army[i])).get_cost();
        idCount = (*(army[i])).get_idcost();

        archivo1 << id << endl;
        archivo1 << type << endl;
        archivo1 << name << endl;
        archivo1 << maxHitPoints << endl;
        archivo1 << hitPoints << endl;
        archivo1 << attack << endl;
        archivo1 << defense << endl;
        archivo1 << range << endl;
        archivo1 << level << endl;
        archivo1 << experience << endl;
        archivo1 << movement << endl;
        archivo1 << posX << endl;
        archivo1 << posY << endl;
        archivo1 << cost << endl;
        archivo1 << idCount << endl;
    }

    archivo1 << name1 << endl;
    archivo1 << score << endl;
    archivo1.close();

    ofstream archivo2;
    archivo2.open("Field.txt", ios::out);

    for (int i = 0; i < field.getRows(); i++)
    {
        for (int j = 0; j < field.getCols(); j++)
        {
            Cell &celda = field.getCell(i, j);

            if (!((celda).get_passable()))
            {
                archivo2 << "-";
            }
            else
            {
                Unit *unidad = (celda).getUnit();
                archivo2 << (*(unidad)).get_type();
            }
        }
        archivo2 << endl;
    }
    archivo2.close();
    return true;
}