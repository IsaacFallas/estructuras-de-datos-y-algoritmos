#include "Unit.h"
#include "Lancer.h"
#include "Cavalry.h"
#include "Archer.h"
#include "Cell.h"
#include "Player.h"
#include "UI.h"

int main()
{
    cout << endl
         << "<<<   Bienvenido al juego  >>>" << endl
         << endl;

    int L;
    int C;
    int A;
    UI g;

    Field *playfield = new Field();
    Player p1(20, playfield, "P1", 0);
    Player p2(20, playfield, "P2", 0);

    cout << endl <<"Jugador 1, ingrese la cantidad de Lancers: ";
    cin >> L;

    cout << "Jugador 1, ingrese la cantidad de Archers: ";
    cin >> A;

    cout << "Jugador 1, ingrese la cantidad de Cavalrys: ";
    cin >> C;

    p1.createArmy(L, C, A);

    cout << endl << "Jugador 2, ingrese la cantidad de Lancers: ";
    cin >> L;

    cout << "Jugador 2, ingrese la cantidad de Archers: ";
    cin >> A;

    cout << "Jugador 2, ingrese la cantidad de Cavalrys: ";
    cin >> C;

    p2.createArmy(L, C, A);

    bool guard = false;
    char o;

    while (!guard)
    {
        cout << "" << endl;
        cout << "" << endl;
        (*(playfield)).showField();
        cout << "" << endl;
        cout << "" << endl;
        p1.play();

        cout << "¿Desea guardar el juego y salir?" << endl;
        cin >> o;
        if (o == 'S' || o == 's')
        {
            cout << "Juego guardado :)" << endl;
            g.guardar(p1, p2, *playfield);
            guard = true;
        }
        cout << "" << endl;
        cout << "" << endl;
        (*(playfield)).showField();
        cout << "" << endl;
        cout << "" << endl;
        p2.play();

        cout << "¿Desea guardar el juego y salir?" << endl;
        cin >> o;
        if (o == 'S' || o == 's')
        {
            cout << "Juego guardado :)" << endl;
            g.guardar(p1, p2, *playfield);
            guard = true;
        }
    }
}