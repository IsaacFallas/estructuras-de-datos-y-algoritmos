#pragma once
#include <iostream>
#include <vector>
#include "Unit.h"

using namespace std;

/** @brief Implementa un Cell y sus acciones

    @author Esteban Rodríguez
    @author Jorge Fallas
    @author Gabriel Gutiérrez
    */

class Unit;

class Cell{
    private:
        bool passable; /** @param passable - acceso a la celda.*/
        Unit* army; /** @param army - unidad en la celda*/

    public:
        /** 
         * @brief Constructor por defecto de la clase.
        */
        Cell();
         /**
        * @brief Constructor de la clase.
        * @param passable - acceso a la celda.
        * @param army - puntero a una unidad en la celda.
        */
        Cell(bool passable,Unit* army);
         /**
        * @brief Destructor por defecto de la clase.
        */
        ~Cell();
         /**
        * @brief Método que muestra la información de la celda.
        * @param c - objeto de una celda.
        */
        void show_cell(Cell c);
         /**
        * @brief Método para saber el acceso a la celda.
        */
        bool get_passable();
         /**
        * @brief Método que coloca el acceso a la celda.
        * @param pass - permiso a la celda.
        */
        void setPassable(bool pass);
         /**
        * @brief Método que devuelve un puntero de una unidad.
        */
        Unit* getUnit();
         /**
        * @brief Método que coloca una unidad en la celda del tablero.
        * @param army - puntero a una unidad.
        */
        void setArmy(Unit* army);
};