#pragma once
#include <iostream>
#include <string>
#include "Cell.h"

using namespace std;

/** @brief Implementa un Unit y sus acciones

    @author Esteban Rodríguez
    @author Jorge Fallas
    @author Gabriel Gutiérrez
    */

class Cell;

class Unit{
    private:
        int id; /** @param id - identificador de la unidad.*/
        char type; /** @param type - tipo de la unidad.*/
        string name; /** @param name - nombre de la unidad.*/
        int maxHitPoints; /** @param maxHitPoints - puntos de salud máximos de la unidad.*/
        int hitPoints; /** @param hitPoints - puntos de salud de la unidad.*/
        int attacke; /** @param attacke - ataque de la unidad.*/
        int defense; /** @param defense - defensa de la unidad.*/
        int range; /** @param range - rango de la unidad.*/
        int level; /** @param level - nivel de la unidad.*/
        int experience; /** @param experience - experiencia de la unidad.*/
        int movement; /** @param movement - movimiento de la unidad.*/
        int posX; /** @param posX - posición en x de la unidad.*/
        int posY; /** @param posY - posición en y de la unidad.*/
        int cost; /** @param cost - costo de la unidad.*/
        int idCount; /** @param idCount - contador id de la unidad.*/
    public:
        /** 
         * @brief Constructor por defecto de la clase.
        */
        Unit();
         /**
        * @brief Constructor de la clase.
        * @param id - identificador de la unidad.
        * @param type - tipo de la unidad.
        * @param name - nombre de la unidad.
        * @param maxHitPoints - puntos de salud máximos de la unidad.
        * @param hitPoints - puntos de salud de la unidad.
        * @param attacke - ataque de la unidad. 
        * @param defense - defensa de la unidad.
        * @param range - rango de la unidad.
        * @param level - nivel de la unidad.
        * @param experience - experiencia de la unidad.
        * @param movement - movimiento de la unidad.
        * @param posX - posición en x de la unidad.
        * @param posY - posición en y de la unidad.
        * @param cost - costo de la unidad.
        * @param idCount - contador id de la unidad.
        */
        Unit(int id, char type, string name, int maxHitPoints, int hitPoints, int attack, int defense, int range, int level, int experience, int movement, int posX, int posY, int cost, int idCount);
         /**
        * @brief Destructor por defecto de la clase.
        */
        ~Unit();
                 /**
        * @brief Método que coloca identificador de la unidad.
        * @param id - identificador de la unidad.
        */
        void setId(int id);
         /**
        * @brief Método que coloca el tipo de la unidad.
        * @param type - tipo de la unidad.
        */
        void setType(char type);
         /**
        * @brief Método que coloca nombre de la unidad.
        * @param name - nombre de la unidad.
        */
        void setName(string name);
         /**
        * @brief Método que coloca los puntos de salud máximos de la unidad.
        * @param maxHitPoints - puntos de salud máximos de la unidad.
        */
        void setMaxHitPoints(int maxHitPoints);
         /**
        * @brief Método que coloca los puntos de salud de la unidad.
        * @param hitPoints - puntos de salud de la unidad.
        */
        void setHitPoints(int hitPoints);
         /**
        * @brief Método que coloca el ataque de la unidad.
        * @param attacke - ataque de la unidad. 
        */
        void setAttack(int attack);
         /**
        * @brief Método que coloca la defensa de la unidad.
        * @param defense - defensa de la unidad.
        */
        void setDefense(int defense);
         /**
        * @brief Método que coloca el rango de la unidad.
        * @param range - rango de la unidad.
        */
        void setRange(int range);
         /**
        * @brief Método que coloca el nivel de la unidad.
        * @param level - nivel de la unidad.
        */
        void setLevel(int level);
         /**
        * @brief Método que coloca la experiencia de la unidad.
        * @param experience - experiencia de la unidad.
        */
        void setExperience(int experience);
         /**
        * @brief Método que coloca el movimiento de la unidad.
        * @param movement - movimiento de la unidad.
        */
        void setMovement(int movement);
         /**
        * @brief Método que coloca la posición en x de la unidad.
        * @param posX - posición en x de la unidad.
        */
        void setPosX(int posX);
         /**
        * @brief Método que coloca la posición en y de la unidad.
        * @param posY - posición en y de la unidad.
        */
        void setPosY(int posY);
         /**
        * @brief Método que coloca el costo de la unidad.
        * @param cost - costo de la unidad.
        */
        void setCost(int cost);
         /**
        * @brief Método que coloca el contador id de la unidad.
        * @param idCount - contador id de la unidad.
        */
        void setIdCount(int idCount);
          /**
        * @brief Método que obtiene el identificador de la unidad.
        */
        int get_id();
         /**
        * @brief Método que obtiene el tipo de la unidad.
        */
        char get_type();
         /**
        * @brief Método que obtiene el nombre de la unidad.
        */
        string get_name();
         /**
        * @brief Método que obtiene los puntos de salud máximos de la unidad.
        */
        int get_maxHitPoints();
         /**
        * @brief Método que obtiene los puntos de salud de la unidad.
        */
        int get_hitPoints();
         /**
        * @brief Método que obtiene el ataque de la unidad.
        */
        int get_attack();
         /**
        * @brief Método que obtiene la defensa de la unidad.
        */
        int get_defense();
         /**
        * @brief Método que obtiene el rango de la unidad.
        */
        int get_range();
         /**
        * @brief Método que obtiene el nivel de la unidad.
        */
        int get_level();
         /**
        * @brief Método que obtiene la experiencia de la unidad.
        */
        int get_experience();
         /**
        * @brief Método que obtiene el movimiento de la unidad.
        */
        int get_movement();
         /**
        * @brief Método que obtiene la posición en x de la unidad.
        */
        int get_posX();
         /**
        * @brief Método que obtiene la posición en y de la unidad.
        */
        int get_posY();
         /**
        * @brief Método que obtiene el costo de la unidad.
        */
        int get_cost();
         /**
        * @brief Método que obtiene contador de id de la unidad.
        */
        int get_idcost();
         /**
         *  @brief Método para mover una unidad.
         *  @param cell - referencia a un objeto cell.
         *  @param x - posición en x.
         *  @param y - posición en y.
         */
        virtual bool move(Cell &cell,int x,int y) = 0;
         /**
         *  @brief Método para atacar una unidad.
         *  @param cell - referencia a un objeto cell.
         *  @param corX - posición en x.
         *  @param corY - posición en y.
         */
        virtual bool attack(int corX,int corY,Cell &cell) = 0;
};