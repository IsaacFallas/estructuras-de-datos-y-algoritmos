#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include "Player.h"
#include "Field.h"

using namespace std;

/** @brief Implementa un UI y sus acciones

    @author Esteban Rodríguez
    @author Jorge Fallas
    @author Gabriel Gutiérrez
    */

class Player;
class Field;

class UI{
    private:
        vector<string> data1; /** @param data1 - vector de strings para guardar y cargar una partida.*/ 
        vector<string> data2; /** @param data2 - vector de strings para guardar y cargar una partida.*/
        vector<string> data3; /** @param data3 - vector de strings para guardar y cargar una partida.*/

    public:
        /**
        * @brief Método para cargar una partida.
        * @param P1 - referencia al jugador 1.
        * @param P2 - referencia al jugador 2.
        * @param field - referencia al tablero.
        */
        void cargar(Player &P1, Player &P2, Field &field);
        /**
        * @brief Método para guardar una partida.
        * @param P1 - referencia al jugador 1.
        * @param P2 - referencia al jugador 2.
        * @param field - referencia al tablero.
        */
        bool guardar(Player &P1, Player &P2, Field &field);
};