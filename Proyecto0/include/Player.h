#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Unit.h"
#include "Field.h"
#include "Lancer.h"
#include "Archer.h"
#include "Cavalry.h"

using namespace std;

/** @brief Implementa un Player y sus acciones

    @author Esteban Rodríguez
    @author Jorge Fallas
    @author Gabriel Gutiérrez
    */

class Player{
    private:
        int maxCost; /** @param  maxCost - Costo máximo de las unidades.*/
        vector<Unit*> army; /** @param army - vetor de punteros de unidad.*/
        Field *playfield; /** @param  playfield - puntero al campo de juego.*/
        string name; /** @param  name - nombre del jugador.*/
        int score; /** @param score - puntaje del jugador.*/
    public:
        /** 
         * @brief Constructor por defecto de la clase.
        */
        Player();
        /** 
         * @brief Constructor de la clase.
         * @param maxCost - maxCost - Costo máximo de las unidades.
         * @param  playfield - puntero al campo de juego.
         * @param  name - nombre del jugador.
         * @param score - puntaje del jugador.
        */
        Player(int maxCost, Field *playfield, string name, int score);
        /** 
         * @brief Destructor por defecto de la clase.
        */
        ~Player();
        /** 
         * @brief Método que coloca el costo máximo.
         * @param maxCost - Costo máximo de las unidades.
        */
        void set_maxCost(int maxCost);
        /** 
         * @brief Método que coloca un vector de punteros de unidad.
         * @param playfield - puntero al campo de juego.
        */
        void set_army(vector<Unit*> army);
        /** 
         * @brief Método que coloca un puntero del tablero.
         * @param playfield - puntero al campo de juego.
        */
        void set_playfield(Field *playfield);
        /** 
         * @brief Método que coloca un nombre al jugador.
         * @param name - nombre del jugador.
        */
        void set_name(string name);
        /** 
         * @brief Método que coloca un puntaje al jugador.
         * @param score - puntaje del jugador.
        */
        void set_score(int score);
        /** 
         * @brief Método que obtiene el costo máximo.
        */
        int get_maxCost();
        /** 
         * @brief Método que obtiene el vector de punteros de unidad.
        */
        vector<Unit*> get_army();
        /** 
         * @brief Método que obtiene el puntero del tablero.
        */
        Field* get_playfield();
        /** 
         * @brief Método que obtiene el nombre del jugador.
        */
        string get_name();
        /** 
         * @brief Método que obtiene el puntaje del jugador.
        */
        int get_score();
        /** 
         * @brief Método que crea un vector de punteros de unidad.
         * @param lancer - cantidad de lancers.
         * @param cavalry - cantidad de cavalrys.
         * @param archer - cantidad de archers.
        */
        void createArmy(int lancer,int cavalry,int archer);
        /** 
         * @brief Método para jugar un turno cada jugador.
        */
        void play();
};