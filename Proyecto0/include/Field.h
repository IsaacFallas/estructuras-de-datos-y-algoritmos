#pragma once
#include <iostream>
#include "Cell.h"

using namespace std;

/** @brief Implementa un Field y sus acciones

    @author Esteban Rodríguez
    @author Jorge Fallas
    @author Gabriel Gutiérrez
    */

class Field
{
private:

    Cell playfield[10][10]; /** @param playfield - Matriz del celdas*/
    int rows; /** @param rows - Filas del tablero */  
    int cols; /** @param cols - Columnas del tablero */ 

public:
    /** 
    * @brief Constructor por defecto de la clase.
    */
    Field();
    /**
    * @brief Destructor por defecto de la clase.
    */
    ~Field();
    /**
    * @brief Método para obtener una referencia de una celda del tablero.
    * @param posX - posición en x          
    * @param posY - posición en y
    */    
    Cell &getCell(int posX, int posY);
    /**
    * @brief Método para obtener las filas del tablero.
    */
    int getRows();
    /**
    * @brief Método para obtener las columnas del tablero.
    */
    int getCols();
    /**
    * @brief Método para mostrar el tablero.
    */
    void showField();
};
