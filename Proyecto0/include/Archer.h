#pragma once
#include <iostream>
#include "Unit.h"

using namespace std;

/** @brief Implementa un Archer y sus acciones

    @author Esteban Rodríguez
    @author Jorge Fallas
    @author Gabriel Gutiérrez
    */

class unit;

class Archer : public Unit{
    private:

    public: 
        /** 
         * @brief Constructor por defecto de la clase.
        */
        Archer();
        /**
        * @brief Constructor de la clase.
        * @param id - identificador de la unidad.
        * @param type - tipo de la unidad.
        * @param name - nombre de la unidad.
        * @param maxHitPoints - puntos de salud máximos de la unidad.
        * @param hitPoints - puntos de salud de la unidad.
        * @param attacke - ataque de la unidad. 
        * @param defense - defensa de la unidad.
        * @param range - rango de la unidad.
        * @param level - nivel de la unidad.
        * @param experience - experiencia de la unidad.
        * @param movement - movimiento de la unidad.
        * @param posX - posición en x de la unidad.
        * @param posY - posición en y de la unidad.
        * @param cost - costo de la unidad.
        * @param idCount - contador id de la unidad.
        */
        Archer(int id, char type, string name, int maxHitPoints, int hitPoints, int attack, int defense, int range, int level, int experience, int movement, int posX, int posY, int cost, int idCount);
        /**
        * @brief Destructor por defecto de la clase.
        */
        ~Archer();
        /**
         *  @brief Método para mover una unidad.
         *  @param cell - referencia a un objeto cell.
         *  @param x - posición en x.
         *  @param y - posición en y.
         */
        bool move(Cell &cell,int x,int y);
        /**
         *  @brief Método para atacar una unidad.
         *  @param cell - referencia a un objeto cell.
         *  @param corX - posición en x.
         *  @param corY - posición en y.
         */
        bool attack(int corX, int corY, Cell &cell);
};