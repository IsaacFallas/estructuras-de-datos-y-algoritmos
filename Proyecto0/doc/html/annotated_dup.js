var annotated_dup =
[
    [ "Archer", "class_archer.html", "class_archer" ],
    [ "Cavalry", "class_cavalry.html", "class_cavalry" ],
    [ "Cell", "class_cell.html", "class_cell" ],
    [ "Field", "class_field.html", "class_field" ],
    [ "Lancer", "class_lancer.html", "class_lancer" ],
    [ "Player", "class_player.html", "class_player" ],
    [ "UI", "class_u_i.html", "class_u_i" ],
    [ "Unit", "class_unit.html", "class_unit" ]
];