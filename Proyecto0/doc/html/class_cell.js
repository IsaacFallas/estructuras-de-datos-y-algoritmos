var class_cell =
[
    [ "Cell", "class_cell.html#a394510643e8664cf12b5efaf5cb99f71", null ],
    [ "Cell", "class_cell.html#a927aaa690202202472f87c8a28e5c37c", null ],
    [ "~Cell", "class_cell.html#a9fa559f7a28e2b4336c6879ca09304d8", null ],
    [ "get_passable", "class_cell.html#aafc2a1afdb69cd39e3d77da26e882c54", null ],
    [ "getUnit", "class_cell.html#a13757b774799990afac7f8d2f64dc776", null ],
    [ "setArmy", "class_cell.html#ae5b1b333c8911d6120acb60a6f66d559", null ],
    [ "setPassable", "class_cell.html#a0c067ef99032f0e0fc0c63c0090d8195", null ],
    [ "show_cell", "class_cell.html#a9a37b0278d37f45dbb4d88fe1a78cd90", null ]
];