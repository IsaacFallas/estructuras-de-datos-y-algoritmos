var class_player =
[
    [ "Player", "class_player.html#affe0cc3cb714f6deb4e62f0c0d3f1fd8", null ],
    [ "Player", "class_player.html#abba672ad334b89450baa764843ce6b43", null ],
    [ "~Player", "class_player.html#a749d2c00e1fe0f5c2746f7505a58c062", null ],
    [ "createArmy", "class_player.html#a174088729c0a8d6925e3989be7f9a76a", null ],
    [ "get_army", "class_player.html#a8006115d12f7d9f44237e71ff9e5b7d8", null ],
    [ "get_maxCost", "class_player.html#adfa5faafcd44321a9b98d83b7f7de309", null ],
    [ "get_name", "class_player.html#afca8141f38b60b526fd2f874523a2726", null ],
    [ "get_playfield", "class_player.html#a06a4e7a3a8167a1e3bfc76a72735ddd6", null ],
    [ "get_score", "class_player.html#ad65c379a083e7c6656721616f8784059", null ],
    [ "play", "class_player.html#a8cd819ec3812c26e038e74426bc2b90f", null ],
    [ "set_army", "class_player.html#adf497dbf8f67bb1bfb4d45e24c239d71", null ],
    [ "set_maxCost", "class_player.html#a970f2e5bcfe60164556848a655d8b418", null ],
    [ "set_name", "class_player.html#a888944ca99282eb9abb77f338b844637", null ],
    [ "set_playfield", "class_player.html#a9dea645635e1902b017c8bfdcb00744b", null ],
    [ "set_score", "class_player.html#aed5ebcd988a99fcbf8ef0e657a9dad24", null ]
];