var hierarchy =
[
    [ "Cell", "class_cell.html", null ],
    [ "Field", "class_field.html", null ],
    [ "Player", "class_player.html", null ],
    [ "UI", "class_u_i.html", null ],
    [ "Unit", "class_unit.html", [
      [ "Archer", "class_archer.html", null ],
      [ "Cavalry", "class_cavalry.html", null ],
      [ "Lancer", "class_lancer.html", null ]
    ] ]
];