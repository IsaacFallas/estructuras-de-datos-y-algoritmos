#include <iostream>
#include "point3d.h"

using namespace std;

int main(){
    Point3D p1;
    Point3D p2(1, 2, 3);    

    cout << "Distance between p1 and (5, 7, 9) is: " << p1.getDistance(5, 7, 9) << endl;
    cout << "Distance between p2 and (5, 7, 9) is: " << p2.getDistance(5, 7, 9) << endl;
    cout << "Distance between p1 and p2 is: " << p1.getDistance(p2) << endl;
    cout << "Distance between p2 and p1 is: " << p2.getDistance(p1) << endl;

    return 0;
}
