#pragma once

/** @brief Implements a representation of a point in 3D.

    Detailed description here.
    @author Ariel Mora
    @date January 2019
    */
class Point3D {
    public: 
        /** Default constructor. Initializes data in zero */
        Point3D();

		/** Custom constructor. Initializes data
            @param x - x coordinate of the point
            @param y - y coordinate of the point
            @param z - z coordinate of the point 
            */
        Point3D(float x, float y, float z);

        /** Clear your data here. */
        ~Point3D();

        /** Return x coordinate
            @return x coordinate of the point*/
        float getX();

        /** Return y coordinate
            @return y coordinate of the point*/
        float getY();

        /** Return z coordinate
            @return z coordinate of the point*/
        float getZ();

        /**Calculate distance from origin.
           The distance is calculated with the formula
            \f$\sqrt{(x-x0)^2+(y-y0)^2+(z-z0)^2}\f$.
            @param x - x coordinate of the point
            @param y - y coordinate of the point
            @param z - z coordinate of the point
            @return distance to point
            */
        float getDistance(float x, float y, float z);

        /**Calculate distance from another point.
           The distance is calculated with the formula
            \f$\sqrt{(x2-x1)^2+(y2-y1)^2+(z2-z1)^2}\f$.
            @param point - point in 3D space
            @return distance to point
            */
        float getDistance(Point3D point);

    private:
        float _x0;/**<initial x-coordinate*/
        float _y0;/**<initial y-coordinate*/
        float _z0;/**<initial z-coordinate*/
};
