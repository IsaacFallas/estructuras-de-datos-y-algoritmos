#include "replace.h"


replace::replace(int umbral, vector<string> entrada)
{
    this->umbral = umbral;
    this->entrada = entrada;
}

void replace::comparar(){

    int cnt = 0;
    int rem = 0;

    for (size_t i = 0; i < entrada.size(); i++)
    {
        for (size_t j = i; j < entrada.size(); j++)
        {
            
            if (entrada[i] == entrada[j])
            {
                cnt++;
    
            }
  
            
        }

        if (cnt >= umbral)
        {
            bool flag2 = false;

            for (size_t j = 0; j < reemplazo.size(); j++)
            {
                if (reemplazo[j] == entrada[i])
                {
                    flag2 = true;
                    break;
                }
                
            }
            if (!flag2)
            {
                rem++;
                reemplazo.push_back(entrada[i]);
            }
        }

        cnt = 0;
        
    }
   
}

void replace::escribirRep(){

    ofstream archivo;
    archivo.open("in.rep",ios::out);
    bool flag = false;

    for (size_t i = 0; i < entrada.size(); i++)
    {
        for (size_t j = 0; j < reemplazo.size(); j++)
        {
            if (entrada[i] == reemplazo[j])
            {
                archivo << "@" << j+1 << " ";
                flag = true;
                
            }else
            {
                
                     for (size_t z = 0; z < reemplazo.size(); z++)
                    {
                    if (entrada[i] == reemplazo[z])
                    {
                        flag = true;
                        
                    }
                    }
                    if (!flag)
                    {
                        archivo << entrada[i] << " ";
                        flag = true;
                    }
                    
                
                
            }
            
            
            
            
        }

        flag = false;
        
    }
     archivo.close();
        
    }

void replace::escribirTab(){

ofstream archivo;
archivo.open("in.tab",ios::out);

for (size_t i = 0; i < reemplazo.size(); i++)
{
    archivo << reemplazo[i] << " " << "@" << i+1 << endl;
}


archivo.close();
}
    
    



replace::~replace()
{
}