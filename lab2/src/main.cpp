#include "check.h"
#include "replace.h"
#include <ctime>

using namespace std;
unsigned t0, t1;

int main(int argc, char *argv[])
{
  t0 = clock();

  int umbral = stoi(argv[1], nullptr, 10);
  string archivo = argv[2];
  check prueba(archivo);
  vector<string> v = prueba.separate(archivo);
  replace outputFile(umbral, v);
  outputFile.comparar();
  outputFile.escribirRep();
  outputFile.escribirTab();

  t1 = clock();
  double execTime = (double(t1-t0)/CLOCKS_PER_SEC);
  cout << "Excecution time: " << execTime << " s"<< endl;
  return 0;
}
