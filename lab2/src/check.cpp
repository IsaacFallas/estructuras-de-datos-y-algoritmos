#include "check.h"

/*Class constructor*/
check::check(string name)
{
  this->name = name;
}

check::~check()
{
}

vector<string> check::separate(string name)
{
  char data[10000];
  ifstream ifile;
  ifile.open(name);
  while (!ifile.eof())
  {
    ifile.getline(data, 10000);
    string s(data);
    int a = 0;
    int count = 0;
    for (size_t i = 0; i <= s.length(); i++)
    {
      if ((s[i] == ' ') || (i == s.length()))
      {
        count = i - a;
        string word = returnWord(s, a, count);
        lines.push_back(word);
        a = i + 1;
      }
    }
  }
  ifile.close();
  return lines;
}

string check::returnWord(string s, int a, int count)
{
  string word = s.substr(a, count);
  return word;
}
