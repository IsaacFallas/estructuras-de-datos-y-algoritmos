import sys
"""Módulo para recibir argumentos por parámetro a la hora de ejecutar el programa."""
import time
"""Módulo para utilizar funciones relacionadas con el tiempo."""

class check():
    """
    Clase utilizada para leer una archivo y separar sus líneas en palabras individuales.

    Atributos
    ---------
    name : str
        El nombre del archivo a leer.
    lines : list
        Lista donde se almacenan la palabras separadas.

    Métodos
    -------
    __init__()
        Constructor de la clase.
    read()
        Lee el archivo y guarda las líneas en el atributo lines.
    separate(text)
        Toma cada línea extraída del texto y la separa en palabras.
    """
    name = None
    lines = list()

    def __init__(self):
        """Establece el atributo name con el nombre del archivo."""
        self.name = sys.argv[2]

    def read(self):
        """Abre el archivo y lee las líneas del texto."""
        f = open(self.name, "r")
        if f.mode == "r":
            self.lines = f.read().replace('\n',' ')
        f.close()
        return self.lines

    def separate(self,text):
        """
        Toma las líneas extraídas del texto y las separa en palabras,
        para sobre escribirlas en el atributo lines.

        Parámetros
        ----------
        text : list
            Lista que contiene las líneas extraídas del texto.

        Returns
        -------
            El atributo lines con las palabras del texto.
        """
        self.lines = text.split(" ")
        num = len(self.lines)-1
        self.lines.pop(num)
        return self.lines

class replace():
    """
    Clase utilizada para contar, comparar las palabras, sustuirlas por la codificación
    correspondiente, y escribir los archivos con los cambios y la tabla de codificación.

    Atributos
    ---------
    name : str
        El nombre del archivo a leer.
    thrs : int
        Umbral de reemplazo de las palabras.
    words : list
        Palabras extraídas del text.
    repl : list
        Palabras que serán sustituidas en el texto.

    Métodos
    -------
    __init__()
        Constructor de la clase.
    compare()
        Recorre la lista de palabras para comparlas y contar cuantas veces se repiten.
    writeFile()
        Escribe un archivo con las palabras repetidas sustituidas por la codificación.
    writeTable()
        Escribe un archivo con la codificación de las palabras sustituidas en el texto.
    """
    name = None
    thrs = None
    words = list()
    repl = list()

    def __init__(self,words):
        """
        Establece los atributos name,thrs y words.

        Parámetros
        ----------
        words : list
            Lista que contiene las palabras extraídas del texto.
        """
        self.name = sys.argv[2]
        self.thrs = int(sys.argv[1])
        self.words = words

    def compare(self):
        """
        Recorre la lista de palabras mientras compara y cuenta la cantidad
        de veces que se repiten; si dicha cantidad es igual o mayor al umbral
        de palabras introducido, la palabra se almacenan en la lista de palabras
        que se van a sustuir.
        """
        count = 0
        for i in range(len(self.words)):
            for j in range(len(self.words)):
                if self.words[i] == self.words[j]:
                    count += 1
            if count >= self.thrs:
                check = False
                for k in range(len(self.repl)):
                    if self.repl[k] == self.words[i]:
                        check = True
                        break
                if check == False:
                    self.repl.append(self.words[i])
            count = 0

    def writeFile(self):
        """
        Abre un archivo para empezar a escribir el texto codificado; recorre
        la lista de palabras y las compara con las palabras que se deben sustuir,
        si son iguales, se sustiyen por la codificación y se escriben en el archivo,
        si son distontas simplemente se escriben en el archivo.
        """
        f = open("In.rep", "w")
        if f.mode == "w":
            flag = False
            for i in range(len(self.words)):
                for j in range(len(self.repl)):
                    if self.words[i] == self.repl[j]:
                        st = "@" + str(j+1) + " "
                        f.write(st)
                        flag = True
                    else:
                        for k in range(len(self.repl)):
                            if self.words[i] == self.repl[k]:
                                flag = True
                        if flag == False:
                            st = self.words[i] + " "
                            f.write(st)
                            flag = True
                flag = False
            f.close()

    def writeTable(self):
        """
        Abre un archivo de texto para empezar a escribir la tabla de codificación
        para las palabras sustituidas; se recore la lista y cada una se le asigna
        un código númerico ascendente, para luego ser escritas en el archivo.
        """
        f = open("In.tab","w")
        if f.mode == "w":
            for i in range(len(self.repl)):
                st = self.repl[i] + " " + "@" + str(i+1)
                f.write(st)
            f.close()

def main():
    """Función main del programa"""
    read = check()
    lines = read.read()
    lines = read.separate(lines)
    write = replace(lines)
    write.compare()
    write.writeFile()
    write.writeTable()
    stat_time = time.time()
    print("El programa tardó %s segundos en completarse" % (time.time() - stat_time))
if __name__ == '__main__':
    main()
