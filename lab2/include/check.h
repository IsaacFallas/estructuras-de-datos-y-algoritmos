#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

class check
{
private:
  string name;          /**<Text file name*/
  vector<string> lines; /**<Line from the text file*/

public:
  /**
    * @brief Default constructor.
    */
  check(string name);
  /**
    * @brief Default destructor.
    */
  ~check();
  /**
    * @brief Reads the file and separates it in lines. It also put each word into the vector.
    * @param name - String that contains the file's name.
    * @ return String vector.
    */
  vector<string> separate(string name);
  /**
    * @brief Extract a word from the string object.
    * @param s - A string which is extrated from the file.
    * @param a - The position where the word starts.
    * @param count - The amount of characters in the word.
    * @return String with the word.
    */
  string returnWord(string s, int a, int count);
};
