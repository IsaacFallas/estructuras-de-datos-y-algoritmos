#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

class replace
{
private:
    int umbral;               /**<*/
    vector<string> reemplazo; /**<*/
    vector<string> entrada;   /**<*/

public:
    /**
    * @brief Default constructor of the class.
    * @param umbral - The minimum threshold for counting the words.
    * @param entrada -A vector which contains the words of the file.
    */
    replace(int umbral, vector<string> entrada);
    /**
    * @brief Default destructor of the class.
    */
    ~replace();
    /**
    * @brief Compare the amount of time that each word apears in the text.
    */
    void comparar();
    /**
    * @brief Write the text but with the words codified.
    */
    void escribirRep();
    /**
    * @brief Write an output file with the changes in the original file.
    */
    void escribirTab();
};
