#pragma once

#include <string>

using namespace std; 

/** @brief Implements a type for Pokemon and moves.

    Implements a type with its strenghts and weaknesses.
    @author Ariel Mora
    @date January 2019
    */
class Type{
    private:
        string name;/**<Type name*/
        string weak[4];/**<Type weaknesses*/
        string strength[4];/**<Type strengths*/

    public:
        /** Default constructor. */
        Type();
        /** Custom constructor. Initializes data
            @param name - Type name
            */
        Type(string name);
        /** Custom constructor. Initializes data
            @param name - Type name
            @param weakness - Type weaknesses
            @param strengths - Type strengths
            */
        Type(string name, string weakness[4], string strengths[4]);
        /** Default destructor */
        ~Type();
        /** Set type weaknesses
            @param weakness - Weakness types name
            */
        void setWeakness(string weakness[4]);
        /** Set type strenghts
            @param strength - Strength types name
            */
        void setStrengths(string strength[4]);
        /** Get type name
            @return type name
            */
        string getName();
        /** Get type weaknesses
            @return type weakness
            */
        string getWeakness();
        /** Get type strenghts
            @return type strengths
            */
        string getStrengths();
        /** Check if Type is weak against another Type.  
            @param t - Pokemon
            @return weak or not
            */
        bool weakAgainst(Type t);
        /** Check if Type is strong against another Type.  
            @param t - Pokemon
            @return strong or not
            */
        bool strongAgainst(Type t); 
}; 