#pragma once

#include <string>
#include "type.h"

using namespace std;

/** @brief Implements a Pokemon move.

    Implements a move used by a Pokemon.
    @author Ariel Mora
    @date January 2019
    */
class Move{
    private:
        string  name;/**<Move name*/
        string  TM;/**<Move TM*/
        string  effect;/**<Move effect*/
        Type    t;/**<Move type*/
        int     pow;/**<move power*/
        int     acc;/**<Move accuracy*/
        int     pp;/**<Move pp*/

    public:
        /**Default constructor. */
        Move();
        /** Custom constructor. Initializes data
            @param name - Move name
            */
        Move(string name);
        /** Custom constructor. Initializes data
            @param name - Move name
            @param pow - Move power
            @param acc - Move accuracy
            @param pp - Move pp
            */
        Move(string name, int pow, int acc, int pp);
        /** Custom constructor. Initializes data
            @param name - Move name
            @param pow - Move power
            @param acc - Move accuracy
            @param pp - Move pp
            @param t - Move type
            */
        Move(string name, int pow, int acc, int pp, Type t);
        /**Default destructor */
        ~Move();
        /** Get move name
            @return move name
            */
        string getName();
        /** Get move type 
            @return move type
            */
        Type getType();
        /** Get move info
            @return move summary
            */
        string getInfo();
        /** Get move power
            @return move power
            */
        int getPow();
        /** Get move accuracy
            @return move acc
            */
        int getAcc();
        /** Get move pp
            @return move pp
            */
        int getPP();
};