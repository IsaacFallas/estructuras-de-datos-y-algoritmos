#pragma once

#include <string>

#include "move.h"
#include "type.h"

using namespace std;

/** @brief Implements a Pokemon.

    Implements a Pokemon with its type and own moveset.
    @author Ariel Mora
    @date January 2019
    */
class Pokemon{
    private:
        string  name;           /**<Pokemon name*/
        int     pokedexNumber;  /**<Pokedex number*/
        int     level;          /**<Pokemon level*/
        int     exp;            /**<Pokemon experience*/
        Type    types[2];       /**<Pokemon types*/
        Move    moves[4];       /**<Pokemon moveset*/

    public:
        /** Default constructor. */
        Pokemon();
        /** Default constructor.  
            @param name - Pokemon name
            @param number - Pokedex number
            */
        Pokemon(string name, int number);
        /** Default constructor.  
            @param name - Pokemon name
            @param number - Pokedex number
            @param p - Pokemon type
            */
        Pokemon(string name, int number, Type p);
        /** Destructor. Clears data */
        ~Pokemon();
        /** Set Pokemon type or types.  
            @param types - Pokemon types
            */
        void setType(Type types[2]);
        /** Set Pokemon moves.  
            @param moves - Pokemon moves
            */
        void setMove(Move moves[4]);
        /** Get Pokemon name
            @return Pokemon name 
            */
        string getName();
        /** Get Pokemon type.  
            @param number - Pokemon types
            */
        Type getType(int number);
        /** Get Pokemon move.  
            @param number - Pokemon moves
            */
        Move getMove(int number);
        /** Print Pokemon constructor. */
        void printInfo();
        /** Print types against Pokemon is strong. */
        void printStrengths();
        /** Print types against Pokemon is weak. */
        void printWeaknesses();
        /** Print Pokemon moves. */
        void printMoves();
        /** Check if Pokemon is weak against another Pokemon.  
            @param p - Pokemon
            @return weak or not
            */
        bool weakAgainst(Pokemon p);
        /** Check if Pokemon is strong against another Pokemon.  
            @param p - Pokemon
            @return strong or not
            */
        bool strongAgainst(Pokemon p);
        /** Check if Pokemon is weak against a Pokemon move.  
            @param m - Move
            @return weak or not
            */
        bool weakAgainst(Move m);
        /** Check if Pokemon is strong against a Pokemon move.  
            @param m - Move
            @return strong or not
            */
        bool strongAgainst(Move m);
};

