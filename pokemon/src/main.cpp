#include <iostream>
#include <string>
#include "move.h"
#include "pokemon.h"
#include "type.h" 

using namespace std;

int main(){
    string fireWeakness[] = {"Water", " ", " ", " "};
    string fireStrength[] = {"Grass", " ", " ", " "};
    string waterWeakness[] = {"Grass", " ", " ", " "};
    string waterStrength[] = {"Fire", " ", " ", " "};
    string grassWeakness[] = {"Fire", " ", " ", " "};
    string grassStrength[] = {"Water", " ", " ", " "};
    Type fire("Fire", fireWeakness, fireStrength);
    Type water("Water", waterWeakness, waterStrength);
    Type grass("Grass", grassWeakness, grassStrength);

    Move none;
    Move waterGun("Water Gun", 40, 100, 25, water);
    Move vineWhip("Vine Whip", 45, 100, 25, grass);
    Move ember("Ember", 40, 100, 25, fire);

    Pokemon bulbasaur("Bulbasaur", 1, grass);
    Pokemon charmander("Charmander", 4, fire);
    Pokemon squirtle("Squirtle", 7, water);

    Move bulbasaurMoves[] = {vineWhip, none, none, none};
    Move charmanderMoves[] = {ember, none, none, none};
    Move squirtleMoves[] = {waterGun, none, none, none};

    bulbasaur.setMove(bulbasaurMoves);
    charmander.setMove(charmanderMoves);
    squirtle.setMove(squirtleMoves);

    bulbasaur.printInfo();
    charmander.printInfo();
    squirtle.printInfo();

    if(bulbasaur.weakAgainst(charmander)){
        cout << bulbasaur.getName() << " is weak against " << charmander.getName() << endl;
    }else{
        cout << bulbasaur.getName() << " is strong against " << charmander.getName() << endl;
    }
    
    if(bulbasaur.weakAgainst(squirtle)){
        cout << bulbasaur.getName() << " is weak against " << squirtle.getName() << endl;
    }else{
        cout << bulbasaur.getName() << " is strong against " << squirtle.getName() << endl;
    }

    if(squirtle.weakAgainst(vineWhip)){
        cout << squirtle.getName() << " is weak against " << vineWhip.getName() << endl;
    }else{
        cout << squirtle.getName() << " is strong against " << vineWhip.getName() << endl;
    }

    if(squirtle.weakAgainst(ember)){
        cout << squirtle.getName() << " is weak against " << ember.getName() << endl;
    }else{
        cout << squirtle.getName() << " is strong against " << ember.getName() << endl;
    }

    return 0;
}
