#include "move.h"

using namespace std;

Move::Move(){
    this->name = "none";
    this->TM = " ";
    this->effect = "No effect";
    this->pow = 0; 
    this->acc = 0;
    this->pp = 0;
}

Move::Move(string name){
    this->name = name;
    this->TM = " ";
    this->effect = "No effect";
    this->pow = 0; 
    this->acc = 0;
    this->pp = 0;
}

Move::Move(string name, int pow, int acc, int pp){
    this->name = name;
    this->TM = " ";
    this->effect = "No effect";
    this->pow = pow; 
    this->acc = acc;
    this->pp = pp;
}

Move::Move(string name, int pow, int acc, int pp, Type t){
    this->name = name;
    this->TM = " ";
    this->effect = "No effect";
    this->t = t;
    this->pow = pow; 
    this->acc = acc;
    this->pp = pp;
}

Move::~Move(){
    //Do nothing
}

string Move::getName(){
    return this->name;
}

Type Move::getType(){
    return this->t;
}

string Move::getInfo(){
    string info = " ";

    return info;
}

int Move::getPow(){
    return this->pow;
}

int Move::getAcc(){
    return this->acc;
}

int Move::getPP(){
    return this->pp;
}