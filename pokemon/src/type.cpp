#include "type.h"

using namespace std;

Type::Type(){
    this->name = "default";

    for(int i = 0; i < 4; i++){
        this->weak[i] = "none";
        this->strength[i] = "none";
    }

}

Type::Type(string name){
    this->name = name;

    for(int i = 0; i < 4; i++){
        this->weak[i] = "none";
        this->strength[i] = "none";
    }
}

Type::Type(string name, string weakness[4], string strengths[4]){
    this->name = name;

    for(int i = 0; i < 4; i++){
        this->weak[i] = weakness[i];
        this->strength[i] = strengths[i];
    }
}

Type::~Type(){
    //Do nothing
}

void Type::setWeakness(string weakness[4]){
    for(int i = 0; i < 4; i++){
        this->weak[i] = weakness[i];
    }
}

void Type::setStrengths(string strength[4]){
    for(int i = 0; i < 4; i++){
        this->strength[i] = strength[i];
    }
}

string Type::getName(){
    return this->name;
}

string Type::getWeakness(){
    string weaknessess = " ";

    return weaknessess;
}

string Type::getStrengths(){
    string strength = " ";

    return strength;
}

bool Type::weakAgainst(Type t){
    bool weak = false;

    for(int i = 0; i<4; i++){
        if(t.getName() == this->weak[i]){
            weak = true;
        }
    }

    return weak;
}

bool Type::strongAgainst(Type t){
    bool strong = false;

    for(int i = 0; i<4; i++){
        if(t.getName() == this->strength[i]){
            strong = true;
        }
    }

    return strong;
}