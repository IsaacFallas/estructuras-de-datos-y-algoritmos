#include <iostream>
#include "pokemon.h"

Pokemon::Pokemon(){
    this->name = "default";           
    this->pokedexNumber = 0;  
    this->level = 1;          
    this->exp = 0;                    
}

Pokemon::Pokemon(string name, int number){
    this->name = name;           
    this->pokedexNumber = number;  
    this->level = 1;          
    this->exp = 0;            
}

Pokemon::Pokemon(string name, int number, Type p){
    this->name = name;           
    this->pokedexNumber = number;  
    this->level = 1;          
    this->exp = 0;            
    this->types[0] = p;       
}

Pokemon::~Pokemon(){
    //Do nothing
}

void Pokemon::setType(Type types[2]){
    for(int i = 0; i<2; i++){
        this->types[i] = types[i];
    } 
}

void Pokemon::setMove(Move moves[4]){
    for(int i = 0; i<4; i++){
        this->moves[i] = moves[i];
    }
}

string Pokemon::getName(){
    return this->name;
}

Type Pokemon::getType(int pos){
    return this->types[pos];
}

Move Pokemon::getMove(int pos){
    return this->moves[pos];
}

void Pokemon::printInfo(){
    cout << "Pokemon: " << this->name << endl;
    cout << "Pkdx No: " << this->pokedexNumber << endl;
    cout << "Type 1: " << this->types[0].getName() << endl;
    cout << "Type 2: " << this->types[1].getName() << endl;
}

void Pokemon::printStrengths(){
    cout << endl;
}

void Pokemon::printWeaknesses(){
    cout << endl;
}

void Pokemon::printMoves(){
    cout << endl;
}

bool Pokemon::weakAgainst(Pokemon p){
    bool weak = false;
    int i = 0;
    Type vsType = p.getType(0);

    while(!weak && (i < 2)){
        weak = this->types[i].weakAgainst(vsType);
        i++; 
    }

    i = 0;
    vsType = p.getType(1);
    while(!weak && (i < 2)){
        weak = this->types[i].weakAgainst(vsType);
        i++;
    }

    return weak;
}

bool Pokemon::strongAgainst(Pokemon p){
    bool strong = false;

    return strong;
}

bool Pokemon::weakAgainst(Move m){
    bool weak = false;
    int i = 0;
    Type vsType = m.getType();

    while(!weak && (i < 2)){
        weak = this->types[i].weakAgainst(vsType);
        i++; 
    }

    return weak;
}

bool Pokemon::strongAgainst(Move m){
    bool strong = false;

    return strong;
}