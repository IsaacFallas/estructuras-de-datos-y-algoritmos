# Estructuras de Datos y Algoritmos

* Jorge Isaac Fallas Mejía B62562
* Gabriel Gutiérrez Arguedas B63215
* Esteban Rodríguez Quintana B66076
________________________________________________
________________________________________________


# Command line instructions for git use:
You can also upload existing files from your computer using the instructions below.

#### Git global setup (Cambie su nombre y correo)
    git config --global user.name "Esteban Rodríguez Quintana"      
    git config --global user.email "erquintana0805@gmail.com"
    
#### Push an existing folder
    cd existing_folder
    git init
    git remote add origin git@gitlab.com:erquintana0805/robot-diferencial-1.git
    git add .
    git commit -m "commit"
    git push -u origin master

#### To pull and push in repo
    git pull origin master
    git add .
    git commit -m "Descripción de commit"
    git push origin master