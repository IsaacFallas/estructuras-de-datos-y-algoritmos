#include "tentomon.h"
#include "kabuterimon.h"

using namespace std;

Tentomon::Tentomon() : Digimon{ "Tentomon", rookie }, 
                             Attribute{ "Vaccine" },
                             Family{ "Nature Spirits" }
{
    numAttacks = 4;
    attacks = new string[numAttacks];

    attacks[0] = "Super Shocker";
    attacks[1] = "Talon Attack";
    attacks[2] = "Tai Atari";
    attacks[3] = "Double Punch";   
}

Tentomon::~Tentomon(){
    //delete attacks;
}

string Tentomon::getInfo(){
    string info = "";

    info += "Name: " + getName() + "\n";
    info += "Attribute: " + getAttribute() + "\n";
    info += "Family: " + getFamily() + "\n";
    info += "Attacks: \n";

    for(int i = 0; i<numAttacks; i++){
        info += "         " + attacks[i] + "\n";
    }

    return info;
}

Digimon* Tentomon::getEvolution(){
    Digimon *nextEvolution;

    nextEvolution = new Kabuterimon();

    return nextEvolution;
}