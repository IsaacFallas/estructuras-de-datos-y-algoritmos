#include "digimon.h"

Digimon::Digimon() : name("")
{
    this->evolution = baby;
}

Digimon::Digimon(string name) : name(name)
{
    this->evolution = baby;
}

Digimon::Digimon(string name, Evolution evolution) : name(name)
{
    this->evolution = evolution;
}

Digimon::~Digimon(){
    //nothing to do   
}

void Digimon::setName(string name){
    this->name = name;
}

string Digimon::getName(){
    return this->name;
}

string Digimon::getEvolution(){
    string evolutionName = "";

    //ToDo: add other levels
    switch (this->evolution){
        case rookie:
            evolutionName = "Rookie";
            break;
        case champion:
            evolutionName = "Champion";
            break;
    }

    return evolutionName;
}

string Digimon::getInfo(){
    string info = "";

    info += "Name: " + this->name + "\n";
    info += "Level: " + this->getEvolution() + "\n";

    return info;
}