#include "kabuterimon.h"

using namespace std;

Kabuterimon::Kabuterimon() : Digimon{ "Kabuterimon", champion }, 
                             Attribute{ "Vaccine" },
                             Family{ "Nature Spirits" }
{
    numAttacks = 5;
    attacks = new string[numAttacks];

    attacks[0] = "Electro Shocker";
    attacks[1] = "Big Horn";
    attacks[2] = "Electric Storm";
    attacks[3] = "Shocking Touch";
    attacks[4] = "Quick Thrust";   
}

Kabuterimon::~Kabuterimon(){
    //delete attacks;
}

string Kabuterimon::getInfo(){
    string info = "";

    info += "Name: " + getName() + "\n";
    info += "Attribute: " + getAttribute() + "\n";
    info += "Family: " + getFamily() + "\n";
    info += "Attacks: \n";

    for(int i = 0; i<numAttacks; i++){
        info += "         " + attacks[i] + "\n";
    }

    return info;
}

Digimon* Kabuterimon::getEvolution(){
    Digimon *nextEvolution;

    nextEvolution = NULL;

    return nextEvolution;
}