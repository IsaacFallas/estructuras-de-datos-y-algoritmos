#include <iostream>
#include "digimon.h"
#include "tentomon.h"
#include "kabuterimon.h"

using namespace std;

int main(){
    Digimon *digimon;
    
    Tentomon tentomon;
    digimon = &tentomon;

    cout << tentomon.getInfo() << endl;
    cout << digimon->getInfo() << endl;

    Digimon *nextEvolution;

    nextEvolution = tentomon.getEvolution();

    cout << nextEvolution->getInfo() << endl;

    // Kabuterimon kabuterimon = *nextEvolution;
    // cout << kabuterimon.getInfo() << endl;

    Kabuterimon *kabuterimon = static_cast<Kabuterimon*>(nextEvolution);
    cout << kabuterimon->getInfo() << endl;

    return 0;
}
