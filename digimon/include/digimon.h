#pragma once

#include <string>

using namespace std;

typedef enum Evolution{
    baby,
    inTraining,
    rookie,
    champion,
    ultimate,
    mega
} Evolution;

class Digimon{
    public:
        Digimon();
        Digimon(string name);
        Digimon(string name, Evolution evo);
        ~Digimon();
        void setName(string name);
        string getName();
        string getEvolution();
        string getInfo();
    private:
        string name;
        Evolution evolution;
};