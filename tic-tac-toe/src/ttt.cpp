#include "ttt.h"

//class::constructor()
TicTacToe::TicTacToe() {
    //Do nothing
}
//  class::method() 
int TicTacToe::move(int row, int col, int player) {
    matrix[row][col]=player;

    //check row
    bool win=true;
    for(int i=0; i<3; i++){
        if(matrix[row][i]!=player){
            win=false;
            break;
        }
    }

    if(win) return player;

    //check column
    win=true;
    for(int i=0; i<3; i++){
        if(matrix[i][col]!=player){
            win=false;
            break;
        }
    }

    if(win) return player;

    //check back diagonal
    win=true;
    for(int i=0; 3; i++){
        if(matrix[i][i]!=player){
            win=false;
            break;
        }
    }

    if(win) return player;

    //check forward diagonal
    win=true;
    for(int i=0; i<3; i++){
        if(matrix[i][(3-i)-1]!=player){
            win=false;
            break;
        }
    }

    if(win) return player;

    return 0;
}

TicTacToe::~TicTacToe() {
    //Do nothing
}
