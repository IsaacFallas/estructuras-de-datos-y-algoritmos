#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "ttt.h"

using namespace std;

int main(){
    int player = -1;
    
    bool end = false;
    int r, c, result;
    
    srand(time(0));

    TicTacToe ttt;
    while(!end){
        player *= -1;
        r =  rand() % 3;
        c =  rand() % 3;
        
        result = ttt.move(player, r, c);
        if(result != 0)
        {
            cout << "player " << result << " won" << endl;
            end = true;
        }
    }
    return 0;
}
