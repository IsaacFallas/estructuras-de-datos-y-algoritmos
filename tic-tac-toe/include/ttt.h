#pragma once

/** @brief Implements a Tic-Tac-Toe game.

    Detailed description here.
    @author Ariel Mora
    @date January 2019
    */
class TicTacToe {
    public: 
        
        /** Initialize your data structure here. */
        TicTacToe();

        /** Clear your data here. */
        ~TicTacToe();

        /** Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or -1.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                -1: Player 2 wins. */
        int move(int row, int col, int player);

    private:
        int matrix[3][3];/**<board data*/
};
