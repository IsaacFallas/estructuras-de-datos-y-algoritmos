#pragma once
#include <iostream>

using namespace std;

/** @brief Clase que implementa un árbol binario con sus métodos básicos
 * @author Esteban Rodríguez
 * @author Jorge Fallas
 * @author Gabriel Gutiérrez
 */

template <typename Datum>

class Node{
    private:
        Datum d; /**<Dato emplantillado que maneja el nodo el árbol.*/
        Node *father; /**<Puntero que apunta al nodo ancestro.*/
        Node *left_son;  /**<Puntero que apunta al nodo hijo por la izquierda.*/
        Node *right_son; /**<Puntero que apunta al nodo hijo por la derecha.*/
        bool root; /**<Booleano para saber si el nodo es la raíz.*/
    public:
        /**
         * @brief Contructor por defecto de la clase
         */
        Node();
        /**
         * @brief Contructor de la clase, crea e inicializa un nodo.
         * @param d - Dato emplantillado que se establece en el nodo.
         */
        Node(Datum d);
        /**
         * @brief Destructor por defecto de la clase.
         */
        ~Node();
        /**
         * @brief Método utilizado para establecer como True el atributo booleano root de la clase Node.
         */
        void setRoot();
        /**
         * @brief Método utilizado para insertar un elemento en un nuevo nodo.
         * @param d - Dato emplantillado que se establece en el nodo.
         */
        void insert(Datum d);
        /**
         * @brief Método utilizado para insertar un elemento en un nuevo nodo.
         * @param d - Dato emplantillado que se establece en el nodo.
         */
        void remove(Datum d);
        /**
         * @brief Método utilizado para encontrar un nodo a partir del dato que contega.
         * @param d - Dato emplantillado que se establece en el nodo.
         * @param n - Puntero tipo de la clase Node.
         * @return Puntero tipo de la clase Node.
         */
        Node* find(Datum d, Node *n);
        /**
         * @brief Método que se encarga de encontrar el valor más grande hacia la izquierda en el árbol binario.
         * @param n - Puntero tipo de la clase Node.
         * @return El dato emplantillado que se encontró como el valor más grande hacia la izquierda en el árbol binario.
         */
        Datum findLargestToTheLeft(Node *n);
        /**
         * @brief Método que se encarga de encontrar el valor más pequeño hacia la derecha en el árbol binario.
         * @param n - Puntero tipo de la clase Node.
         * @return El dato emplantillado que se encontró como el valor más pequeño hacia la derecha en el árbol binario.
         */
        Datum findSmallestToTheRight(Node *n);
        /**
         * @brief Método que imprime los elementos del árbol binario utilizando la lógica preOrden.
         * @param n - Puntero tipo de la clase Node.
         */
        void preOrden(Node *n);
        /**
         * @brief Método que imprime los elementos del árbol binario utilizando la lógica inOrden.
         * @param n - Puntero tipo de la clase Node.
         */
        void inOrden(Node *n);
        /**
         * @brief Método que imprime los elementos del árbol binario utilizando la lógica posOrden.
         * @param n - Puntero tipo de la clase Node.
         */
        void posOrden(Node *n);
        /**
         * @brief Método que inserta recursivamente un nodo en el árbol binario.
         * @param d - Dato emplantillado que se establece en el nodo.
         * @param n - Puntero tipo de la clase Node.
         * @param padre - Puntero tipo de la clase Node.
         */
        void insert_recursive(Datum d, Node *&n, Node *&padre);
        /**
         * @brief Método que elimina recursivamente un nodo en el árbol binario.
         * @param d - Dato emplantillado que se establece en el nodo.
         * @param n - Puntero tipo de la clase Node.
         */
        void remove_recursive(Datum d, Node *&n);
};

template <typename Datum>
Node<Datum>::Node()
{
    this->d = NULL;
    this->father = NULL;
    this->left_son = NULL;
    this->right_son = NULL;
    this->root = false;
}

template <typename Datum>
Node<Datum>::Node(Datum d)
{
    this->d = d;
    this->father = NULL;
    this->left_son = NULL;
    this->right_son = NULL;
    this->root = false;
}

template <typename Datum>
void Node<Datum>::setRoot()
{
    this->root = true;
}

template <typename Datum>
Node<Datum>::~Node()
{
    //Does nothing
}

template <typename Datum>
Node<Datum>* Node<Datum>::find(Datum dat, Node *n)
{

    if (n != 0)
    {
        if (dat < (n->d))
        {
            return find(dat, n->left_son);
        }
        else if (dat > (n->d))
        {
            return find(dat, n->right_son);
        }
        else if (dat == (n->d))
        {
            return n;
        }
    }
}

template <typename Datum>
Datum Node<Datum>::findSmallestToTheRight(Node *n)
{
    if (n == 0)
    {
        return 0;
    }
    else if ((n->right_son == 0) && (n->root == true))
    {
        return (n->d);
    }
    else if ((n->root == true) && (n->right_son != 0))
    {
        return findSmallestToTheRight(n->right_son);
    }
    else if ((n->root == false) && (n->left_son == 0))
    {
        return (n->d);
    }
    else if ((n->root == false) && (n->left_son != 0))
    {
        return findSmallestToTheRight(n->left_son);
    }
}

template <typename Datum>
Datum Node<Datum>::findLargestToTheLeft(Node *n)
{
    if (n == 0)
    {
        return 0;
    }
    else if ((n->left_son == 0) && (n->root == true))
    {
        return n->d;
    }
    else if ((n->root == true) && (n->left_son != 0))
    {
        return findLargestToTheLeft(n->left_son);
    }
    else if ((n->root == false) && (n->right_son == 0))
    {
        return n->d;
    }
    else if ((n->root == false) && (n->right_son != 0))
    {
        return findLargestToTheLeft(n->right_son);
    }
}

template <typename Datum>
void Node<Datum>::insert(Datum d)
{
    insert_recursive(d);
}

template <typename Datum>
void Node<Datum>::insert_recursive(Datum d, Node *&n, Node *&padre)
{
    if (n == 0 && padre == 0)
    {
        Node<Datum> *new_node = new Node(d);
        n = new_node;

    }else if (n == 0 && padre != 0)
    {
         Node<Datum> *new_node = new Node(d);
         n = new_node;
         n->father = padre;
    }
    else if (n->d < d)
    {
        return insert_recursive(d, n->right_son, n);
    }
    else if (n->d > d)
    {

        return insert_recursive(d, n->left_son, n);
    }
}

template <typename Datum>
void Node<Datum>::remove(Datum d)
{
    remove_recursive(d);
}

template <typename Datum> 
void Node<Datum>::remove_recursive(Datum d, Node *&n)
{
    Node *node = find(d, n);
    if ((node->left_son == 0) && (node->right_son == 0))
    {

      Datum aux = node->d;
        if ((node->father->d) < aux)
        {
            node->father->right_son = 0;
            delete node;
        }
        else if ((node->father->d) > aux)
        {
            node->father->left_son = 0;
            delete node; 
        }
    }
    else if ((node->left_son != 0) && (node->right_son == 0))
    {
        node->father->left_son = node->left_son;
        node->left_son = 0;
        delete node;
    }
    else if ((node->left_son == 0) && (node->right_son != 0))
    {
        node->father->right_son = node->right_son;
        node->right_son = 0;
        delete node;
    }
    else if ((node->left_son != 0) && (node->right_son != 0))
    {
        Datum num = findSmallestToTheRight(node->right_son);
        remove_recursive(num, node->right_son);
        node->d = num;
    }
}

template <typename Datum>
void Node<Datum>::preOrden(Node *n)
{

    if (n == 0)
    {
        return;
    }
    else
    {

        cout << n->d << " - ";
        preOrden(n->left_son);
        preOrden(n->right_son);
    }
}

template <typename Datum>
void Node<Datum>::inOrden(Node *n)
{

    if (n == 0)
    {
        return;
    }
    else
    {

        inOrden(n->left_son);
        cout << n->d << " - ";
        inOrden(n->right_son);
    }
}

template <typename Datum>
void Node<Datum>::posOrden(Node *n)
{

    if (n == 0)
    {
        return;
    }
    else
    {

        posOrden(n->right_son);
        cout << n->d << " - ";
        posOrden(n->left_son);
    }
}
