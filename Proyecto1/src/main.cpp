#include "Node.h"

using namespace std;

int main(){


//Node<int> inicio(3);
Node<int> *raiz = new Node<int>(3);

(*raiz).setRoot();

//raiz = &inicio;

//Datum d = 5;

//(inicio).insert_recursive(1, raiz);
//(*raiz).insert_recursive(3, raiz);
//(*raiz).insert_recursive(7, raiz, raiz);
//(*raiz).insert_recursive(6, raiz, raiz);
//(*raiz).insert_recursive(2, raiz, raiz);
//(*raiz).insert_recursive(1, raiz);
//(*raiz).insert_recursive(10, raiz);
//(*raiz).insert_recursive(4, raiz, raiz);
//(*raiz).insert_recursive(10, raiz, raiz);

(*raiz).insert_recursive(10, raiz, raiz);
(*raiz).insert_recursive(8, raiz, raiz);
(*raiz).insert_recursive(9, raiz, raiz);
(*raiz).insert_recursive(5, raiz, raiz);
(*raiz).insert_recursive(6, raiz, raiz);
(*raiz).insert_recursive(4, raiz, raiz);
(*raiz).insert_recursive(2, raiz, raiz);
(*raiz).insert_recursive(15, raiz, raiz);
(*raiz).insert_recursive(14, raiz, raiz);
(*raiz).insert_recursive(18, raiz, raiz);
(*raiz).insert_recursive(20, raiz, raiz);
(*raiz).insert_recursive(24, raiz, raiz);

cout << "El menor a la derecha es: " << (*raiz).findSmallestToTheRight(raiz) << endl;
cout << "El mayor a la izquierda es: " << (*raiz).findLargestToTheLeft(raiz) << endl;
cout << "Impresión preOrden del árbol" << endl;
(*raiz).preOrden(raiz);
cout << endl;
cout << "Impresión inOrden del árbol" << endl;
(*raiz).inOrden(raiz);
cout << endl;
cout << "Impresión posOrden del árbol" << endl;
(*raiz).posOrden(raiz);
cout << endl;
cout << "Eliminación de un nodo con 2 hijos" << endl;
(*raiz).remove_recursive(8, raiz);
cout << "Impresión inOrden del árbol" << endl;
(*raiz).inOrden(raiz);
cout << endl;
cout << "Eliminación de un nodo con un hijo" << endl;
(*raiz).remove_recursive(18, raiz);
cout << "Impresión inOrden del árbol" << endl;
(*raiz).inOrden(raiz);
cout << endl;
cout << "Eliminación de un nodo sin hijos" << endl;
(*raiz).remove_recursive(2, raiz);
cout << "Impresión inOrden del árbol" << endl;
(*raiz).inOrden(raiz);
cout << endl;
}