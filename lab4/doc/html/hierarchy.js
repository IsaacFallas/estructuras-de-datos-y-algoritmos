var hierarchy =
[
    [ "Pokemon", "class_pokemon.html", [
      [ "Electric", "class_electric.html", [
        [ "Zapdos", "class_zapdos.html", null ]
      ] ],
      [ "Fire", "class_fire.html", [
        [ "Moltres", "class_moltres.html", null ]
      ] ],
      [ "Flying", "class_flying.html", [
        [ "Articuno", "class_articuno.html", null ],
        [ "Moltres", "class_moltres.html", null ],
        [ "Zapdos", "class_zapdos.html", null ]
      ] ],
      [ "Ice", "class_ice.html", [
        [ "Articuno", "class_articuno.html", null ]
      ] ]
    ] ]
];