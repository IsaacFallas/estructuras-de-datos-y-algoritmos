var annotated_dup =
[
    [ "Articuno", "class_articuno.html", "class_articuno" ],
    [ "Electric", "class_electric.html", "class_electric" ],
    [ "Fire", "class_fire.html", "class_fire" ],
    [ "Flying", "class_flying.html", "class_flying" ],
    [ "Ice", "class_ice.html", "class_ice" ],
    [ "Moltres", "class_moltres.html", "class_moltres" ],
    [ "Pokemon", "class_pokemon.html", "class_pokemon" ],
    [ "Zapdos", "class_zapdos.html", "class_zapdos" ]
];