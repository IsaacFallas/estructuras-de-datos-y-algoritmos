var class_pokemon =
[
    [ "Pokemon", "class_pokemon.html#a059e2fca08b08cd218c262f937399ed9", null ],
    [ "Pokemon", "class_pokemon.html#a3f1ce023f07d5d868f58fb0667ba1bdd", null ],
    [ "~Pokemon", "class_pokemon.html#a5ac781a4f4ffe47cad85a5d977733413", null ],
    [ "atk1", "class_pokemon.html#a2348e5119f06a8531178be3e864dc0a7", null ],
    [ "atk2", "class_pokemon.html#ad9ddd5d831f5d17f851f96a73e888e9a", null ],
    [ "atk3", "class_pokemon.html#aaa1b8dc1ac53fce6dc1fa172d8994bd3", null ],
    [ "atk4", "class_pokemon.html#ae3db1873d67b7d96be4181ba9fd53f1c", null ],
    [ "call1", "class_pokemon.html#a0c20d53f2959bafa0ed4bd2f4b72e730", null ],
    [ "getHP", "class_pokemon.html#ad91531adc33486fee630bec3e5bfa388", null ],
    [ "getName", "class_pokemon.html#ac7ad3344754620e3387bff30a73882b3", null ],
    [ "getType", "class_pokemon.html#a96e2292af28228a9bf2fd487822dfdf8", null ],
    [ "printInfo", "class_pokemon.html#aefef3af0aa35fe9a65c9e4db43b06155", null ],
    [ "setHP", "class_pokemon.html#ae295a92d14fcbfdb734b5da67bf9588a", null ]
];