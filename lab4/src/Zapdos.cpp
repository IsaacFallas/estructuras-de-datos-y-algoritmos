#include "Zapdos.h"

Zapdos::Zapdos() : Pokemon(){
  this->Type[0] = "None";
  this->Type[1] = "None";
  this->strongVs = "None";
  this->weakVs = "None";
}

Zapdos::Zapdos(string name,string specie,int HP,int ATK,int DEF,int sATK,int sDEF,int SPD,int EXP,string call) : Pokemon(name,specie,HP,ATK,DEF,sATK,sDEF,SPD,EXP,call){
  this->Type[0] = Electric::type();
  this->Type[1] = Flying::type();
  this->strongVs = Flying::strongVs();
  this->weakVs = Flying::weakVs();
}

Zapdos::~Zapdos(){
  //Does nothing
}


string Zapdos::getType(){

  return this->Type[0];

}


void Zapdos::atk1(Pokemon &pokemon){

  string name = pokemon.getName();
  string atk1 = "Rayo";
  string type1 = pokemon.getType();
  int pokemonHP = pokemon.getHP();

  cout << "Zapdos ha usado " << atk1 << endl;
  
  pokemonHP = pokemonHP - 100;
  pokemon.setHP(pokemonHP);
    
  cout << name << " ahora tiene sus HP en " << pokemonHP << endl;
}

void Zapdos::atk2(Pokemon &pokemon){

  string name = pokemon.getName();
  string atk2 = "Onda trueno";   
  string type1 = pokemon.getType();
  int pokemonHP = pokemon.getHP();

  cout << "Zapdos ha usado " << atk2 << endl;
  
  pokemonHP = pokemonHP - 110;
  pokemon.setHP(pokemonHP);
    
  cout << name << " ahora tiene sus HP en " << pokemonHP << endl;


}


void Zapdos::atk3(Pokemon &pokemon){
  string name = pokemon.getName();
  string atk3 = "Ataque aéreo";   
  string type1 = pokemon.getType();
  int pokemonHP = pokemon.getHP();

  cout << "Zapdos ha usado " << atk3 << endl;
  
  pokemonHP = pokemonHP - 80;
  pokemon.setHP(pokemonHP);
    
  cout << name << " ahora tiene sus HP en " << pokemonHP << endl;


}


void Zapdos::atk4(Pokemon &pokemon){

  string name = pokemon.getName();
  string atk4 = "Aire afilado";   
  string type1 = pokemon.getType();
  int pokemonHP = pokemon.getHP();

  cout << "Zapdos ha usado " << atk4 << endl;
  
  pokemonHP = pokemonHP - 50;
  pokemon.setHP(pokemonHP);
    
  cout << name << " ahora tiene sus HP en " << pokemonHP << endl;


}

void Zapdos::print(){
  printInfo();
  cout<<"Tipos, debilidades y efectivades:"<<endl;
  cout<<"Es de tipo: "<<this->Type[0]<<" y "<<this->Type[1]<<endl;
  cout<<"Es fuerte contra: "<<this->strongVs<<endl;
  cout<<"Es débil contra: "<<this->weakVs<<endl;
}
