#include "Ice.h"

Ice::Ice(){
  //Does nothing
}

Ice::~Ice(){
  //Does nothing
}

string Ice::type(){
  string type = "Ice";
  return type;
}

string Ice::strongVs(){
  string strong = "Flying";
  return strong;
}

string Ice::weakVs(){
  string weak = "Fire";
  return weak;
}
