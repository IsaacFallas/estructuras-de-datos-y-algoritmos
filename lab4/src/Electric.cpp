#include "Electric.h"

Electric::Electric(){
  //Does nothing
}

Electric::~Electric(){
  //Does nothing
}

string Electric::type(){
  string type = "Electric";
  return type;
}

string Electric::strongVs(){
  string strong = "Flying";
  return strong;
}

string Electric::weakVs(){
  string weak = "Ground";
  return weak;
}
