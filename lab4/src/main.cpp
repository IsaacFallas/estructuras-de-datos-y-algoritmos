/**
 * @file main.cpp
 * @author Jorge Isaac Fallas Mejía B62562.
 * @author Esteban Rodríguez Quintana B66076.
 * @author Gabrile Gutierres Arguedas B63215.
 * @brief  Programa escrito en c++ para el laboratorio #4 del curso de Estructutas Abstractas de Datos y Algoritmos para Ingneiería.
 * @version 1
 * @date Enero-2020
 */

#include "Ice.h"
#include "Fire.h"
#include "Electric.h"
#include "Flying.h"
#include "Zapdos.h"
#include "Moltres.h"
#include "Articuno.h"
#include "Pokemon.h"

int main()
{

        //Pokemon *pokemon1;//("Zapdos","Legendary", 1000, 500, 500, 200, 200, 250, 150, "Sanguche");
        //Pokemon *pokemon2;//("Moltres","Legendary", 1000, 500, 500, 200, 200, 250, 150, "Sanguche");
        //Pokemon *pokemon3;//("Articuno","Legendary", 1000, 500, 500, 200, 200, 250, 150, "Sanguche");

        Zapdos zapdos("Zapdos", "Legendary", 1000, 500, 500, 200, 200, 250, 150, "Sanguche");
        Moltres moltres("Moltres", "Legendary", 1000, 500, 500, 200, 200, 250, 150, "Sanguche");
        Articuno articuno("Articuno", "Legendary", 1000, 500, 500, 200, 200, 250, 150, "Sanguche");

        //pokemon1 = &zapdos;
        //pokemon2 = &moltres;
        //pokemon3 = &articuno; *pokemon3

        zapdos.print();
        cout << endl;
        cout << endl;
        moltres.print();
        cout << endl;
        cout << endl;
        articuno.print();
        cout << endl;
        cout << endl;
        zapdos.atk1(articuno);
        cout << endl;
        cout << endl;
        articuno.atk1(zapdos);
        cout << endl;
        cout << endl;
        zapdos.atk2(articuno);
        cout << endl;
        cout << endl;
        articuno.atk2(zapdos);
        cout << endl;
        cout << endl;
        zapdos.atk3(articuno);
        cout << endl;
        cout << endl;
        articuno.atk3(zapdos);
        cout << endl;
        cout << endl;
        zapdos.atk4(articuno);
        cout << endl;
        cout << endl;
        articuno.atk4(zapdos);
        cout << endl;
        cout << endl;
        zapdos.print();
        cout << endl;
        cout << endl;
        moltres.print();
        cout << endl;
        cout << endl;
        articuno.print();

        //zapdos.atk1(articuno);
}