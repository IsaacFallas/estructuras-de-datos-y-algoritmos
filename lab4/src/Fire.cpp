#include "Fire.h"

Fire::Fire(){
  //Does nothing
}

Fire::~Fire(){
  //Does nothing
}

string Fire::type(){
  string type = "Fire";
  return type;
}

string Fire::strongVs(){
  string strong = "Ice";
  return strong;
}

string Fire::weakVs(){
  string weak = "Water";
  return weak;
}
