#include "Moltres.h"

Moltres::Moltres() : Pokemon(){
  this->Type[0] = "None";
  this->Type[1] = "None";
  this->strongVs = "None";
  this->weakVs = "None";
}

Moltres::Moltres(string name,string specie,int HP,int ATK,int DEF,int sATK,int sDEF,int SPD,int EXP,string call) : Pokemon(name,specie,HP,ATK,DEF,sATK,sDEF,SPD,EXP,call){
  this->Type[0] = Fire::type();
  this->Type[1] = Flying::type();
  this->strongVs = Fire::strongVs();
  this->weakVs = Fire::weakVs();
}

Moltres::~Moltres(){
   //Does nothing
}

void Moltres::print(){
  printInfo();
  cout<<"Tipos, debilidades y efectivades:"<<endl;
  cout<<"Es de tipo: "<<this->Type[0]<<" y "<<this->Type[1]<<endl;
  cout<<"Es fuerte contra: "<<this->strongVs<<endl;
  cout<<"Es débil contra: "<<this->weakVs<<endl;
}

string Moltres::getType(){

  return this->Type[0];

}

void Moltres::atk1(Pokemon &pokemon){
  string name = pokemon.getName();
  string atk1 = "Lanzallamas";
  string type1 = pokemon.getType();
  int pokemonHP = pokemon.getHP();
  if (type1 == "Ice") {
    pokemonHP = pokemonHP - 100;
  } else {
    pokemonHP = pokemonHP - 50;
  }
  pokemon.setHP(pokemonHP);
  cout<<"Moltres usó "<<atk1<<endl;
  cout<<name<<" ahora tiene sus HP en "<<pokemonHP<<endl;
}

void Moltres::atk2(Pokemon &pokemon){
  string name = pokemon.getName();
  string atk2 = "Onda ígnea";
  string type1 = pokemon.getType();
  int pokemonHP = pokemon.getHP();
  if (type1 == "Ice") {
    pokemonHP = (pokemonHP)-100;
  } else {
    pokemonHP = (pokemonHP)-50;
  }
  pokemon.setHP(pokemonHP);
  cout<<"Moltres usó "<<atk2<<endl;
  cout<<name<<" ahora tiene sus HP en "<<pokemonHP<<endl;
}

void Moltres::atk3(Pokemon &pokemon){
  string name = pokemon.getName();
  string atk3 = "Vendaval";
  int pokemonHP = pokemon.getHP();
  pokemonHP = (pokemonHP)-60;
  pokemon.setHP(pokemonHP);
  cout<<"Moltres usó "<<atk3<<endl;
  cout<<name<<" ahora tiene sus HP en "<<pokemonHP<<endl;
}

void Moltres::atk4(Pokemon &pokemon){
  string name = pokemon.getName();
  string atk4 = "Ataque ala";
  int pokemonHP = pokemon.getHP();
  pokemonHP = (pokemonHP)-40;
  pokemon.setHP(pokemonHP);
  cout<<"Moltres usó "<<atk4<<endl;
  cout<<name<<" ahora tiene sus HP en "<<pokemonHP<<endl;
}
