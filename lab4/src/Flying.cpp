#include "Flying.h"

Flying::Flying(){
  //Does nothing
}

Flying::~Flying(){
  //Does nothing
}

string Flying::type(){
  string type = "Flying";
  return type;
}

string Flying::strongVs(){
  string strong = "Fight";
  return strong;
}

string Flying::weakVs(){
  string weak = "Electric";
  return weak;
}
