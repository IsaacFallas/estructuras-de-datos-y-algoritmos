#include "Pokemon.h"

Pokemon::Pokemon(){
  this->_name = "None";
  this->_specie = "None";
  this->_HP = 0;
  this->_ATK = 0;
  this->_DEF = 0;
  this->_sATK = 0;
  this->_sDEF = 0;
  this->_SPD = 0;
  this->_EXP = 0;
  this->call = "Default";
}

Pokemon::Pokemon(string name,string specie,int HP,int ATK,int DEF,int sATK,int sDEF,int SPD,int EXP,string call){
  this->_name = name;
  this->_specie = specie;
  this->_HP = HP;
  this->_ATK = ATK;
  this->_DEF = DEF;
  this->_sATK = sATK;
  this->_sDEF = sDEF;
  this->_SPD = SPD;
  this->_EXP = EXP;
  this->call = call;
}

Pokemon::~Pokemon(){
  //Does nothing
}

string Pokemon::call1(){
  string warCry = this->call;
  return warCry;
}

void Pokemon::printInfo(){
  cout<<"Info:"<<endl;
  cout<<"Name: "<<this->_name<<endl;
  cout<<"Specie: "<<this->_specie<<endl;
  cout<<"Statistics:"<<endl;
  cout<<"HP: "<<this->_HP<<endl;
  cout<<"ATK: "<<this->_ATK<<endl;
  cout<<"DEF: "<<this->_DEF<<endl;
  cout<<"sATK: "<<this->_sATK<<endl;
  cout<<"sDEF: "<<this->_sDEF<<endl;
  cout<<"SPD: "<<this->_SPD<<endl;
  cout<<"Experience: "<<this->_EXP<<endl;
}

int Pokemon::getHP(){

    return this->_HP;
}

string Pokemon::getName(){

  return this->_name;

}

void Pokemon::setHP(int HP){

    this->_HP = HP;

}