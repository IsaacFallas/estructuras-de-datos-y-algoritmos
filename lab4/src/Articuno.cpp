#include "Articuno.h"

Articuno::Articuno() : Pokemon(){
  this->Type[0] = "None";
  this->Type[1] = "None";
  this->strongVs = "None";
  this->weakVs = "None";
}

Articuno::Articuno(string name,string specie,int HP,int ATK,int DEF,int sATK,int sDEF,int SPD,int EXP,string call) : Pokemon(name,specie,HP,ATK,DEF,sATK,sDEF,SPD,EXP,call){
  this->Type[0] = Ice::type();
  this->Type[1] = Flying::type();
  this->strongVs = Ice::strongVs();
  this->weakVs = Ice::weakVs();
}

string Articuno::getType(){

  return this->Type[0];

}

void Articuno::atk1(Pokemon &pokemon){
  string name = pokemon.getName();
  string atk1 = "Rayo hielo";
  string type1 = pokemon.getType();
  int pokemonHP = pokemon.getHP();
  if (type1 == "Fire")
  {
      pokemonHP = pokemonHP - 50;
  }else
  {
      pokemonHP = pokemonHP - 110;
  }
  pokemon.setHP(pokemonHP);
  
  cout << "Articuno ha usado " << atk1 << endl;
    
  cout << name << " ahora tiene sus HP en " << pokemonHP << endl;
}

void Articuno::atk2(Pokemon &pokemon){
  string name = pokemon.getName();
  string atk2 = "Ventisca";   
  string type1 = pokemon.getType();
  int pokemonHP = pokemon.getHP();

  if (type1 == "Fire")
  {
      pokemonHP = pokemonHP - 80;
  }else
  {
      pokemonHP = pokemonHP - 150;
  }
  
  pokemon.setHP(pokemonHP);

  cout << "Articuno ha usado " << atk2 << endl;
    
  cout << name << " ahora tiene sus HP en " << pokemonHP << endl;


}


void Articuno::atk3(Pokemon &pokemon){
  string name = pokemon.getName();
  string atk3 = "Picotazo";   
  string type1 = pokemon.getType();
  int pokemonHP = pokemon.getHP();

  cout << "Articuno ha usado " << atk3 << endl;
  
  pokemonHP = pokemonHP - 80;
  pokemon.setHP(pokemonHP);
    
  cout << name << " ahora tiene sus HP en " << pokemonHP << endl;


}


void Articuno::atk4(Pokemon &pokemon){

  string name = pokemon.getName();
  string atk4 = "Tornado";   
  string type1 = pokemon.getType();
  int pokemonHP = pokemon.getHP();

  cout << "Articuno ha usado " << atk4 << endl;
  
  pokemonHP = pokemonHP - 60;
  pokemon.setHP(pokemonHP);
    
  cout << name << " ahora tiene sus HP en " << pokemonHP << endl;


}

Articuno::~Articuno(){
  //Does nothing
}

void Articuno::print(){
  printInfo();
  cout<<"Tipos, debilidades y efectivades:"<<endl;
  cout<<"Es de tipo: "<<this->Type[0]<<" y "<<this->Type[1]<<endl;
  cout<<"Es fuerte contra: "<<this->strongVs<<endl;
  cout<<"Es débil contra: "<<this->weakVs<<endl;
}
