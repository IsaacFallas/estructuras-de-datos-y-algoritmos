#pragma once
#include <iostream>

using namespace std;

class Pokemon
{
private:
    string _name;   /**<Nombre del Pokemon*/
    string _specie; /**<Especie del Pokemon*/
    int _HP;        /**<Puntos de salud*/
    int _ATK;       /**<Ataque físico*/
    int _DEF;       /**<Defensa física*/
    int _sATK;      /**<Ataque especial*/
    int _sDEF;      /**<Defensa especial*/
    int _SPD;       /**<Velocidad*/
    int _EXP;       /**<Experiencia*/
    string call;    /**<Grito de guerra*/

public:
    /**
    * @brief Constructor por defecto de la clase.
    */
    Pokemon();
    /**
    * @brief Constructor de la clase.
    * @param name - Nombre del Pokemon.
    * @param specie - Especie del Pokemon.
    * @param HP - Puntos de salud del Pokemon.
    * @param ATK - Puntos de ataque físico del Pokemon.
    * @param DEF - Puntos de defensa física del Pokemon.
    * @param sATK - Puntos de ataque especial del Pokemon.
    * @param sDEF - Puntos de defensa especial del Pokemon.
    * @param SPD - Puntos de velocidad del Pokemon.
    * @param EXP - Puntos de experiencia del Pokemon.
    * @param call - "Grito de guerra" del Pokemon.
    */
    Pokemon(string name, string specie, int HP, int ATK, int DEF, int sATK, int sDEF, int SPD, int EXP, string call);
    /**
    * @brief Destructor por defecto de la clase.
    */
    ~Pokemon();
    /**
    * @brief Método virtual del ataque 1 del Pokemon.
    * @param Referencia a un objeto de la clase Pokemon.
    */
    virtual void atk1(Pokemon &other) = 0;
    /**
    * @brief Método virtual del ataque 2 del Pokemon.
    * @param Referencia a un objeto de la clase Pokemon.
    */
    virtual void atk2(Pokemon &other) = 0;
    /**
    * @brief Método virtual del ataque 3 del Pokemon.
    * @param Referencia a un objeto de la clase Pokemon.
    */
    virtual void atk3(Pokemon &other) = 0;
    /**
    * @brief Método virtual del ataque 4 del Pokemon.
    * @param Referencia a un objeto de la clase Pokemon.
    */
    virtual void atk4(Pokemon &other) = 0;
    /**
    * @brief Función para obtener el tipo del Pokemon.
    * @return El string que contiene el tipo del Pokemon.
    */
    virtual string getType() = 0;
    /**
    * @brief Función para obtener los puntos de salud del Pokemon.
    * @return El entero que contiene los puntos de salud del Pokemon.
    */
    int getHP();
    /**
    * @brief Función para obtener el nombre del Pokemon.
    * @return El string que contiene el nombre del Pokemon.
    */
    string getName();
    /**
    * @brief Establece los puntos de salud del Pokemon.
    * @param HP - Los nuevos puntos de salud para sobre escribir.
    */
    void setHP(int HP);
    /**
    * @brief Función para obtener el "grito de guerra" del Pokemon.
    * @return El string que contiene el "grito de guerra" del Pokemon.
    */
    string call1();
    /**
    * @brief Función para imprimir la información del Pokemon.
    */
    void printInfo();
};
