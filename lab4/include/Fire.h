#pragma once
#include "Pokemon.h"

using namespace std;

class Fire : virtual public Pokemon{
private:

public:
  /**
    * @brief Constructor para pokémon de tipo fuego
    */
  Fire();
    /**
    * @brief Destrucor de objetos de la clase fuego
    */
  ~Fire();
    /**
    * @brief Retorna un string con la palabra fuego
    */
  static string type();
    /**
    * @brief Esta función retornará un string con el tipo de pokémon contra el que el tipo es fuerte.
    */
  static string strongVs();
    /**
    * @brief  @brief Esta función retornará un string con el tipo de pokémon contra el que el tipo es débil.
    */
  static string weakVs();
};
