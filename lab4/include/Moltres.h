#pragma once
#include "Fire.h"
#include "Flying.h"

using namespace std;

class Moltres : public Fire, public Flying{
private:
  string Type[2];
  string strongVs;
  string weakVs;
public:
  /**
    * @brief Constructor por defecto para la clase Moltres con atributos seteados como none.
    */
  Moltres();
  /**
    * @brief Constructor para clase Moltres con parámetros con atributos inicializados desde el constructor.
    * @param name - recibe el nombre del pokémon.
    * @param specie - recibe la especie del pokémon.
    * @param HP - puntos de salud.
    * @param  ATK - ataque físico.
    * @param DEF - defensa física
    * @param sATK - ataque especial.
    * @param sDEF - defensa especial.
    * @param SPD - velocidad.
    * @param EXP - experiencia.
    * @param call - grito de guerra.
    */
  Moltres(string name,string specie,int HP,int ATK,int DEF,int sATK,int sDEF,int SPD,int EXP,string call);
    /**
    * @brief Destructor para objetos de la clase Moltres
    */
  ~Moltres();
    /**
    * @brief Esta función es la encargada de realizar ataques de Moltres a otro pokémon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokémon.
    */
  void atk1(Pokemon &pokemon);
    /**
    * @brief Esta función es la encargada de realizar ataques de Moltres a otro pokémon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokémon.
    */
  void atk2(Pokemon &pokemon);
    /**
    * @brief Esta función es la encargada de realizar ataques de Moltres a otro pokémon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokémon.
    */
  void atk3(Pokemon &pokemon);
    /**
    * @brief Esta función es la encargada de realizar ataques de Moltres a otro pokémon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokémon.
    */
  void atk4(Pokemon &pokemon);
    /**
    * @brief Se encarga de retornar el tipo de pokémon.
    * @return Retorna el campo cero del array type.
    */
  string getType();
    /**
    * @brief Se encarga de imprimir toda la información correpondiente al tipo, debilidades y efectividades del pokémon de tipo Moltres.
    */
  void print();
};
