#pragma once
#include "Pokemon.h"

using namespace std;

class Ice : virtual public Pokemon
{
private:
public:
    /*
     * brief Constructor para pokémon de tipo fuego
     */
    Ice();
    /**
    * @brief
    * @param
    * @return
    */
    ~Ice();
    /*
    * @brief Destrucor de objetos de la clase ice
    */
    static string type();
    /**
    * @brief Esta función retornará un string con el tipo de pokémon contra el que el tipo es fuerte.
    */
    static string strongVs();
    /**
    * @brief  @brief Esta función retornará un string con el tipo de pokémon contra el que el tipo es débil.
    */
    static string weakVs();
};
