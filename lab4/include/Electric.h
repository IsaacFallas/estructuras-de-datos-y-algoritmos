#pragma once
#include "Pokemon.h"

using namespace std;

class Electric : virtual public Pokemon{
private:

public:
    /**
    * @brief Constructor por defecto de la clase.
    */
    Electric();
    /**
    * @brief Destructor por defecto de la clase.
    */
    ~Electric();
    /**
    * @brief Función para obtener el tipo del Pokemon.
    * @return El string que contiene el tipo del Pokemon.
    */
    static string type();
    /**
    * @brief Función para obtener el tipo contra el cual el Pokemon es fuerte.
    * @return El string que contiene el tipo contra el cual el Pokemon es fuerte.
    */
    static string strongVs();
    /**
    * @brief Función para obtener el tipo contra el cual el Pokemon es débil.
    * @return El string que contiene el tipo contra el cual el Pokemon es débil.
    */
    static string weakVs();
};
