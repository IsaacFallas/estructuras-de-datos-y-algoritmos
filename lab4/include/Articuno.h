#pragma once
#include "Ice.h"
#include "Flying.h"

using namespace std;

class Articuno : public Ice, public Flying
{
private:
  string Type[2];
  string strongVs;
  string weakVs;

public:
  /**
    * @brief Constructor por defecto para la clase Articuno con atributos seteados como none.
    */
  Articuno();
  /**
    * @brief Constructor para clase Articuno con parámetros con atributos inicializados desde el constructor.
    * @param name - recibe el nombre del pokémon.
    * @param specie - recibe la especie del pokémon.
    * @param HP - puntos de salud.
    * @param  ATK - ataque físico.
    * @param DEF - defensa física
    * @param sATK - ataque especial.
    * @param sDEF - defensa especial.
    * @param SPD - velocidad.
    * @param EXP - experiencia.
    * @param call - grito de guerra.
    */
  Articuno(string name, string specie, int HP, int ATK, int DEF, int sATK, int sDEF, int SPD, int EXP, string call);
    /**
    * @brief Destructor para objetos de la clase Articuno
    */
  ~Articuno();
    /**
    * @brief Esta función es la encargada de realizar ataques de articuno a otro pokemon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokemon.
    */
  void atk1(Pokemon &pokemon);
    /**
    * @brief Esta función es la encargada de realizar ataques de articuno a otro pokemon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokemon.
    */
  void atk2(Pokemon &pokemon);
    /**
    * @brief Esta función es la encargada de realizar ataques de articuno a otro pokemon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokemon.
    */
  void atk3(Pokemon &pokemon);
    /**
    * @brief Esta función es la encargada de realizar ataques de articuno a otro pokemon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokemon.
    */
  void atk4(Pokemon &pokemon);
    /**
    * @brief Se encarga de retornar el tipo de pokemon.
    * @return Retorna el campo cero del array type.
    */
  string getType();
    /**
    * @brief Se encarga de impromir toda la información correpondiente al tipo, debilidades y efectividades del pokemón de tipo Articuno.
    */
  void print();
};
