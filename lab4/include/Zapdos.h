#pragma once
#include "Electric.h"
#include "Flying.h"

using namespace std;

class Zapdos : public Electric, public Flying
{
private:
  string Type[2]; /**<Denominador de la fracción*/
  string strongVs;
  string weakVs;

public:
  /**
    * @brief Constructor por defecto para la clase Zapdos con atributos seteados como none.
    */
  Zapdos();
  /**
    * @brief Constructor para clase Zapdos con parámetros con atributos inicializados desde el constructor.
    * @param name - recibe el nombre del pokémon.
    * @param specie - recibe la especie del pokémon.
    * @param HP - puntos de salud.
    * @param  ATK - ataque físico.
    * @param DEF - defensa física
    * @param sATK - ataque especial.
    * @param sDEF - defensa especial.
    * @param SPD - velocidad.
    * @param EXP - experiencia.
    * @param call - grito de guerra.
    */
  Zapdos(string name, string specie, int HP, int ATK, int DEF, int sATK, int sDEF, int SPD, int EXP, string call);
  /**
    * @brief
    * @param
    * @return
    */
  ~Zapdos();
    /**
    * @brief Esta función es la encargada de realizar ataques de Zapdos a otro pokémon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokémon.
    */
  void atk1(Pokemon &pokemon);
    /**
    * @brief Esta función es la encargada de realizar ataques de Zapdos a otro pokémon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokémon.
    */
  void atk2(Pokemon &pokemon);
    /**
    * @brief Esta función es la encargada de realizar ataques de Zapdos a otro pokémon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokémon.
    */
  void atk3(Pokemon &pokemon);
    /**
    * @brief Esta función es la encargada de realizar ataques de Zapdos a otro pokémon y de realizar las impresiones correspondientes.
    * @param &pokemon - referencia a un tipo de objeto pokémon.
    */
  void atk4(Pokemon &pokemon);
    /**
    * @brief Se encarga de retornar el tipo de pokémon.
    * @return Retorna el campo cero del array type.
    */
  string getType();
    /**
    * @brief Se encarga de imprimir toda la información correpondiente al tipo, debilidades y efectividades del pokémon de tipo Zapdos.
    */
  void print();
};