#include "jugador.h"

using namespace std;

jugador::jugador()
{
  this->dinero = 1500;
  this->posicion = 0;
  this->nombre = "jugador";
  this->quebrado = false;
}

jugador::~jugador()
{
}

void jugador::setName(string name)
{
  this->nombre = name;
}

void jugador::setDinero(int dinero)
{
  this->dinero = dinero;
}

int jugador::getPosicion()
{
  return this->posicion;
}

int jugador::getDinero()
{
  return this->dinero;
}

string jugador::getName()
{
  return this->nombre;
}
void jugador::setQuebrado()
{
  this->quebrado = true;
}

bool jugador::getQuebrado()
{
  return this->quebrado;
}

void jugador::impuestos(box box)
{
  string type = box.getBoxType();
  if (type == "tax")
  {
    int price = box.getBoxPrice();
    int dinero = this->dinero;
    this->dinero = dinero - price;
    cout << "    -> Esta es una casilla de impuestos." << endl
         << "    -> " << (this->nombre) << " pagó " << price << " en impuestos." << endl
         << endl;
    if (this->dinero <= 0)
    {
      this->quebrado = true;
    }
  }
}

void jugador::cobrar(box &box, int cantidadJugadores, jugador &jugad, jugador *jugadores)
{
  string type = box.getBoxType();
  if (type == "property")
  {
    string owner = box.getBoxOwner();
    if (owner != "NULL")
    {
      string nombre = jugad.getName();
      for (int i = 0; i < cantidadJugadores; i++)
      {
        if (nombre == owner)
        {
          int dinero_owner = jugadores[i].getDinero();
          int dinero_jugador = jugad.getDinero();
          int price = box.getBoxPrice();
          int res1 = dinero_owner + price;
          int res2 = dinero_jugador - price;
          jugadores[i].setDinero(res1);
          jugad.setDinero(res2);
          break;
        }
      }
    }
  }
}

int jugador::moverse()
{
  srand(time(NULL));
  int mov = (1 + rand() % (7 - 1));
  this->posicion = (this->posicion) + mov;
  cout << endl
       << "    -> Usted avanza " << mov << " espacios." << endl
       << endl;
  if (this->posicion > 35)
  {
    this->dinero = 1500 + this->dinero;
    this->posicion = this->posicion - 35 - 1;
  }

  return this->posicion;
}

void jugador::comprar(box &box, int cantidadJugadores, jugador &jugad, jugador *jugadores)
{
  string type = box.getBoxType();

  int aux=0;
  string boxOwner = box.getBoxOwner();
  string nombreJugador = jugad.getName();
  int dineroJugador = jugad.getDinero();
  int precio = box.getBoxPrice();
  //cout << "Esta casilla:" << endl << "      -> Dueño = " << boxOwner<<endl<< "      -> Precio = " << precio << endl;

  if (boxOwner == "NULL") // Se puede compar
  {
    box.setBoxOwner(type, nombreJugador);
    int res = dineroJugador - precio;
    this->dinero = res;
    cout << "    -> Usted ha decidido comprar " << box.getBoxName() << endl
         << "    -> Ahora su dinero es: " << jugad.getDinero() << endl
         << endl;
    if (res <= 0)
    {

      jugad.setQuebrado();
    }
    jugad.setDinero(res);
  }

  else // casilla ya tiene dueño.
  {
    if (box.getBoxOwner() == jugad.getName())
    {
      //si el dueño es el mismo jugador del turno NO SE HACE NADA
      cout << "    -> Usted ya es dueño de esta propiedad." << endl;
    }
    else // Cuando el dueño es otro se procede  a pagarle y se rebaja dinero del jugador de turno
    {
      cout << "Esta propiedad tiene dueño" << endl; // hay que pagarle al dueño de la casilla
      for (int i = 0; i < cantidadJugadores; i++)   // buscar quien es el dueño
      {
        if (box.getBoxOwner() == jugadores[i].getName())
        {
          jugadores[i].setDinero(jugadores[i].getDinero() + precio); // pagarle al dueno con el dinero sumandole precio de propiedad
          jugad.setDinero(jugad.getDinero() - precio);               // quitar dinero al jugador que cuesta la propiedad
          aux = i;
          cout << "    -> El dueño de esta propiedad es: " << jugadores[aux].getName() << endl;
          break;
        }
      }
      string jugadorOwnerDeCasilla = jugadores[aux].getName();
      cout << "    -> Se ha pagado " << precio << " al jugador " << jugadores[aux].getName() << endl;
    }
  }
}

void jugador::goToJail(jugador jugador)
{
  int posicion = jugador.getPosicion();
  if (posicion == 11)
  {
    srand(time(NULL));
    int mov = (1 + rand() % 6);
    this->posicion = (this->posicion) - mov;
    cout << "    Lo sentimos, usted ha caído en la carcel :( , retrocederá " << mov << " espacios." << endl
         << "    -> Ahora usted se encuentra en la casilla " << this->posicion << endl;
  }
}
//
//    /*    PRUEBAS PARA JUGADOR.CPP    */
//
//    int main()
//    {
//      cout << "Pruebas para box." << endl;
//
//      box casilla("ky", "caca", 1, 100);
//      casilla.setBoxOwner("caca", "Esteban");
//      casilla.setBoxColor("rojo");
//
//      cout << casilla.getBoxName() << endl;
//      cout << casilla.getBoxColor() << endl;
//      cout << casilla.getBoxOwner() << endl;
//      cout << casilla.getBoxPose() << endl;
//      cout << casilla.getBoxPrice() << endl;
//      cout << casilla.getBoxType() << endl;
//
//      cout << "Pruebas para jugador.cpp" << endl;
//
//      jugador j;
//      j.setName("Fallitas");
//      j.setDinero(1200);
//      j.setQuebrado(true);
//
//      cout << j.getDinero() << endl;
//      j.setDinero(j.getDinero() + 700);
//      cout << j.getDinero() << endl;
//      cout << j.getName() << endl;
//      cout << j.getPosicion() << endl;
//      cout << j.getQuebrado() << endl;
//      j.setQuebrado(false);
//      cout << j.getQuebrado() << endl;
//      return 0;
//    }//