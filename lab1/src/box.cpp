#include <iostream>
#include "box.h"

/*  Constructor de la clase*/
box::box()
{
  this->name = "default";
  this->boxtype = "default";
  this->boxPose = 0;
  this->price = 0;
}
/*  Destructor de la clase*/
box::~box()
{
}

/*   SETTERS    */

void box::setBoxColor(string color)
{
  this->color = color;
}

void box::setBoxOwner(string type, string owner)
{
  if (type == "property")
  {
    this->owner = owner;
  }
  else
  {
    this->owner = "NULL";
  }
}

void box::setBoxName(string name)
{
  this->name = name;
}

void box::setBoxType(string type)
{
  this->boxtype = type;
}

void box::setBoxPose(int pose)
{
  this->boxPose = pose;
}

void box::setBoxPrice(int price)
{
  this->price = price;
}

/*    GETTERS   */
string box ::getBoxName()
{
  return this->name;
};

string box::getBoxOwner()
{
  return this->owner;
};

string box::getBoxColor()
{
  return this->color;
};

string box::getBoxType()
{
  return this->boxtype;
}

int box::getBoxPose()
{
  return this->boxPose;
};

int box::getBoxPrice()
{
  return this->price;
};

//
//  /* PRUEBAS BOX.CPP  */
//
//  int main (){
//    cout<<"Pruebas para box.cpp"<<endl;
//
//    box casilla("ky","caca",1,100);
//    casilla.setBoxOwner("caca", "Esteban");
//    casilla.setBoxColor("rojo");
//
//    cout << casilla.getBoxName() << endl;
//    cout << casilla.getBoxColor() << endl;
//    cout << casilla.getBoxOwner() << endl;
//    cout << casilla.getBoxPose() << endl;
//    cout << casilla.getBoxPrice() << endl;
//    cout << casilla.getBoxType() << endl;
//
//  }