#include "Tablero.h"
#include "jugador.h"

jugador *Tablero::Jugadores(int cantidadJugadores, string nombres[])
{
  this->jugadores = new jugador[cantidadJugadores];
  for (int i = 0; i < cantidadJugadores; i++)
  {
    jugadores[i].setName(nombres[i]);
  }
  return this->jugadores;
}

Tablero::Tablero()
{
  string Nombres[] = {
      "Salida", "Xcape", "Caccio's Calle", "Monster Pizza", "Tierra U", "Impuesto",
      "Einstein", "TicoBurguesas", "Fitos", "Búfalos", "Impuesto", "Jail",
      "Frat House", "Parqueo de Xcape", "Soda U", "Licorera de la Calle", "Impuesto", "P y P",
      "Reventados", "Karaoke 88", "Omar Khayyam", "Impuesto", "Caccio's Latina", "AM PM",
      "La Concha", "El mercadito", "Impuesto", "Chupitos", "Bar La California", "Caccio's Cali",
      "October", "Impuesto", "El Cuartel", "Rafa's", "Area", ""};
  string Owners[36];
  for (int j = 0; j < 36; j++)
  {
    Owners[j] = "NULL";
  }
  string Colores[] = {
      "Rojo", "Rojo", "Rojo", "Azul", "Azul", "Azul", "Azul",
      "Verde", "Verde", "Verde", "Morado", "Morado", "Morado",
      "Rojo", "Rojo", "Rojo", "Azul", "Azul", "Azul", "Azul",
      "Verde", "Verde", "Verde", "Morado", "Morado", "Morado",
      "Rojo", "Rojo", "Rojo", "Azul", "Azul", "Azul", "Azul",
      "Verde", "Verde", "Verde", "Morado", "Morado", "Morado"};
  string Types[] = {
      "start", "property", "property", "property", "property", "tax",
      "property", "property", "property", "property", "tax", "jail",
      "property", "property", "property", "property", "tax", "property",
      "property", "property", "property", "tax", "property", "property",
      "property", "property", "tax", "property", "property", "property",
      "property", "tax", "property", "property", "property", "property"};
  int Precios[] = {
      200, 1000, 900, 850, 1100, 500,
      1050, 975, 800, 1000, 500, 0,
      750, 800, 850, 900, 500, 950,
      1000, 1300, 1250, 500, 1200, 1150,
      1100, 1050, 500, 1000, 950, 900,
      850, 500, 1000, 1100, 1200, 900};
  for (int k = 0; k < 36; k++)
  {
    tablero[k].setBoxName(Nombres[k]);
    tablero[k].setBoxPrice(Precios[k]);
    tablero[k].setBoxColor(Colores[k]);
    tablero[k].setBoxOwner(Types[k], Owners[k]);
    tablero[k].setBoxPose(k);
    tablero[k].setBoxType(Types[k]);
  }
}

box *Tablero::getTablero()
{
  return this->tablero;
}

Tablero::~Tablero()
{
}

void Tablero::Estado(jugador &jugador)
{
  string name = jugador.getName();
  int position = jugador.getPosicion();
  int money = jugador.getDinero();
  bool broke = jugador.getQuebrado();
  if (broke == true)
  {
    cout << "El jugador " << name << " ya no participa más durante esta partida" << endl;
  }
  else
  {
    cout << "El jugador " << name << " se encuentra en la posición " << position << " y su dinero es de " << money << endl;
  }
}

bool Tablero::Ganador(jugador *jugadores, int cantidadJugadores)
{

  int cnt = 0;

  for (int i = 0; i < cantidadJugadores; i++)
  {
    if (jugadores[i].getQuebrado() == true)
    {

      cnt++;
    }
  }

  if (cnt == (cantidadJugadores - 1))
  {
    return true;
  }
  else
  {
    return false;
  }
}