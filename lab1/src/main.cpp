#include "Tablero.h"
#include "jugador.h"

using namespace std;

int main()
{

    int numeroJugadores;
    cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
    cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
    cout << "<<<<<<<<                                        >>>>>>>>" << endl;
    cout << "<<<<<<<<     Bienvenido a Gran Callifornia      >>>>>>>>" << endl;
    cout << "<<<<<<<<                                        >>>>>>>>" << endl;
    cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
    cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl
         << endl;
    cout << "Ingrese la cantidad de jugadores:   >>> ";
    cin >> numeroJugadores;

    string nombres[numeroJugadores];

    for (int i = 0; i < numeroJugadores; i++)
    {
        cout << endl
             << "Ingrese el nombre del jugador " << (i + 1) << ":   >>> ";
        cin >> nombres[i];
    }

    jugador *jugadores;

    Tablero tablero;

    box *board = tablero.getTablero();

    jugadores = tablero.Jugadores(numeroJugadores, nombres);

    bool ganador = tablero.Ganador(jugadores, numeroJugadores);

    int opcion;
    int posicion;
    int dinero;

    while (ganador == false)
    {

        for (int i = 0; i < numeroJugadores; i++)
        {
            cout << endl
                 << "_________________________________" << endl;
            cout << "Es el turno de " << jugadores[i].getName() << endl;
            cout << jugadores[i].getName() << ", sus opciones son:" << endl;
            cout << "   -> 1. Moverse" << endl;
            cin >> opcion;

            while (opcion != 1)
            {
                cout << "Elija la opcion moverse" << endl;
                cin >> opcion;
            }

            if (opcion == 1)
            {
                cout << "    ->";
                tablero.Estado(jugadores[i]);
                posicion = jugadores[i].moverse();
                cout << "   ------------------------------------------------" << endl;
                cout << "   Información de la casilla " << jugadores[i].getPosicion() << ":" << endl
                     << "        < Tipo de propiedad: " << board[posicion].getBoxType() << endl
                     << "        < Nombre: " << board[posicion].getBoxName() << endl
                     << "        < Dueño: " << board[posicion].getBoxOwner() << endl
                     << "        < Precio: " << board[posicion].getBoxPrice() << endl;
                cout << "   ------------------------------------------------" << endl;

                string type = board[posicion].getBoxType();

                if (type == "start")
                {
                    dinero = 1500 + jugadores[i].getDinero();
                    jugadores[i].setDinero(dinero);

                }
                else
                {
                    if (type == "property")
                    { // ver si tiene owner, en ese caso paga impuestos
                        cout << endl
                             << jugadores[i].getName() << ", sus opciones son:" << endl;
                        cout << "    -> 1. Pasar turno" << endl;
                        cout << "    -> 2. Comprar" << endl;
                        cin >> opcion;

                        bool valid = false;

                        while (!valid)
                        {
                            switch (opcion)
                            {
                            case 1:
                                valid = true;
                                break;

                            case 2:
                                valid = true;
                                break;

                            default:
                                cout << "Elija alguna de las posibles opciones" << endl;
                                cin >> opcion;
                                break;
                            }
                        }

                        if (opcion == 1)
                        {
                             cout << jugadores[i].getName() <<"ha pasado turno" << endl;
                        }
                        else
                        {
                            if (opcion == 2)
                            {

                                jugadores[i].comprar(board[posicion], numeroJugadores, jugadores[i], jugadores);

                                //cout << "Nombre del dueño: " << board[posicion].getBoxOwner() << endl;
                            }
                        }
                    }
                    else
                    {
                        if (type == "tax")
                        {
                            cout << "Paga un impuesto" << endl;
                            jugadores[i].impuestos(board[posicion]);
                        }
                        else
                        {
                            if (type == "jail")
                            {
                                jugadores[i].goToJail(jugadores[i]);
                                //metodo de Estebitan :v
                            }
                        }
                    }
                }
            }
        }

        ganador = tablero.Ganador(jugadores, numeroJugadores);
    }

    //casillas = tablero.crearTablero();

    //jugadores[0].getName();
    //jugadores[1].getName();

    //Tablero jugar;
    //jugador jugadores [numeroJugadores];
}
