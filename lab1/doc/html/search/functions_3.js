var searchData=
[
  ['ganador',['Ganador',['../class_tablero.html#a523c1acdd0650a32eea1d385c128f194',1,'Tablero']]],
  ['getboxcolor',['getBoxColor',['../classbox.html#aca43ec175c7aaaba3d5527a58677b5d5',1,'box']]],
  ['getboxname',['getBoxName',['../classbox.html#ad4c584f4b997b9050e553f150e54a4fd',1,'box']]],
  ['getboxowner',['getBoxOwner',['../classbox.html#a5b2ee56e4660047fc21fd66455f6e90c',1,'box']]],
  ['getboxpose',['getBoxPose',['../classbox.html#a5120cc94046ca5f2c933b93a2ed0f036',1,'box']]],
  ['getboxprice',['getBoxPrice',['../classbox.html#aedceccadc8309bd0425f3c3b0d050b8e',1,'box']]],
  ['getboxtype',['getBoxType',['../classbox.html#a4789449d73fdf93fbc8e15e3331afc74',1,'box']]],
  ['getdinero',['getDinero',['../classjugador.html#a0eca8f41c7979e2e42e3de81e9e79e69',1,'jugador']]],
  ['getname',['getName',['../classjugador.html#aa7f1d1496c2e45c8bcecd0d1ec256c3b',1,'jugador']]],
  ['getposicion',['getPosicion',['../classjugador.html#a2d3a2d4b1b44aa70cc129eb39b62e404',1,'jugador']]],
  ['getquebrado',['getQuebrado',['../classjugador.html#a84b9ad6e6e3f20d38a8a7068c7116a1f',1,'jugador']]],
  ['gettablero',['getTablero',['../class_tablero.html#aebdba43c2a2976bfd0d8e690c2b2dbec',1,'Tablero']]],
  ['gotojail',['goToJail',['../classjugador.html#afa6d078f93a0dfefc059f8f8fbb7c00c',1,'jugador']]]
];
