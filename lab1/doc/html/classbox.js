var classbox =
[
    [ "box", "classbox.html#a1ca802dd9502ab576e4786c06325fd94", null ],
    [ "~box", "classbox.html#ad9af3cd34d85201cce706f00dab584c9", null ],
    [ "getBoxColor", "classbox.html#aca43ec175c7aaaba3d5527a58677b5d5", null ],
    [ "getBoxName", "classbox.html#ad4c584f4b997b9050e553f150e54a4fd", null ],
    [ "getBoxOwner", "classbox.html#a5b2ee56e4660047fc21fd66455f6e90c", null ],
    [ "getBoxPose", "classbox.html#a5120cc94046ca5f2c933b93a2ed0f036", null ],
    [ "getBoxPrice", "classbox.html#aedceccadc8309bd0425f3c3b0d050b8e", null ],
    [ "getBoxType", "classbox.html#a4789449d73fdf93fbc8e15e3331afc74", null ],
    [ "setBoxColor", "classbox.html#a2a80ef09c17f0c01b670f8e7a4f235c0", null ],
    [ "setBoxName", "classbox.html#abe92f7bf7807ec0e865dcc4049369ed6", null ],
    [ "setBoxOwner", "classbox.html#a29b27b0268334f4b280a55f8570a9e4b", null ],
    [ "setBoxPose", "classbox.html#ac1fae289804da616e6c85ee232b91178", null ],
    [ "setBoxPrice", "classbox.html#abb648c29ce982ba6d330870e67c67ba2", null ],
    [ "setBoxType", "classbox.html#a6bb55166ca4313d595aee3723f54e325", null ]
];