var classjugador =
[
    [ "jugador", "classjugador.html#abc105dac7c7123bde3eddfd14aca46b6", null ],
    [ "~jugador", "classjugador.html#a63b1018f6a9809afeaacb0ad4d94f70a", null ],
    [ "cobrar", "classjugador.html#ac5ac8cf10561b84661929bd856e234df", null ],
    [ "comprar", "classjugador.html#ade1ab8cdfabde83535981d3b29458454", null ],
    [ "getDinero", "classjugador.html#a0eca8f41c7979e2e42e3de81e9e79e69", null ],
    [ "getName", "classjugador.html#aa7f1d1496c2e45c8bcecd0d1ec256c3b", null ],
    [ "getPosicion", "classjugador.html#a2d3a2d4b1b44aa70cc129eb39b62e404", null ],
    [ "getQuebrado", "classjugador.html#a84b9ad6e6e3f20d38a8a7068c7116a1f", null ],
    [ "goToJail", "classjugador.html#afa6d078f93a0dfefc059f8f8fbb7c00c", null ],
    [ "impuestos", "classjugador.html#a1750dfb931ac277340b870eecb9529c3", null ],
    [ "moverse", "classjugador.html#a0313b106f94ec7cfc642de8c3f306896", null ],
    [ "setDinero", "classjugador.html#a3d201e22dd3e2de9161f41c2ec70827a", null ],
    [ "setName", "classjugador.html#a8f3f71cc968230e098205c3c660cd1bf", null ],
    [ "setQuebrado", "classjugador.html#abcc12201f798ad8a4673c4809e199ded", null ]
];