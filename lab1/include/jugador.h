#pragma once

#include <iostream>
#include <string>
#include "box.h"

using namespace std;

/** @brief Implementa un jugador y sus acciones

    @author Esteban Rodríguez
    @author Jorge Fallas
    @author Gabriel Gutiérrez
    @data Enero 2020
    */

class jugador
{
private:
    int dinero;    /**<Dinero del jugador*/
    int posicion;  /**<Posicion del jugador en el tablero*/
    string nombre; /**<Nombre del jugador*/
    bool quebrado; /**<Para revisar si el jugador es válido*/

public:
    /** Constructor por defecto de la clase. */
    jugador();
    /** Destructor por defecto de la clase. */
    ~jugador();
    /**
      * @brief Establecer el nombre del jugador.
      * @param name - Nombre del jugador.
      */
    void setName(string name);
    /**
      * @brief Establecer el dinero del jugador
      * @param dinero - Nuevo cantidad de dinero que tiene el jugador.
      */
    void setDinero(int dinero);
    /**
      * @brief Ver la posicion del jugador en el tablero.
      * @return - posicion del jugador.
      */
    int getPosicion();
    /**
      * @brief Ver el dinero del jugador.
      * @Devuelve la cantidad de dinero del jugador.
      */
    int getDinero();
    /**
      * @brief Ver el nombre del jugador.
      * @return - nombre del jugador.
      */
    string getName();
    /**
      * @brief Ver si jugador no tiene dinero para seguir jugando.
      * @return - si el jugador está en números rojos.
      */
    bool getQuebrado();
    /**
      * @brief Se le cobra un impuesto al jugador si la casilla no tiene dueño.
      * @param box - box donde se encuentra el jugador
      */
    void impuestos(box box);
    /**
      * @brief El jugador le paga al dueño de la casilla.
      * @param box - box donde se encuentra el jugador.
      * @param jugadores - arreglo de jugadores.
      * @param jugador - jugador actual.
      */
    void cobrar(box &box, int cantidadJugadores, jugador &jugad, jugador *jugadores);
    /**
      * @brief El jugador se mueve por el tablero.
      * Tira el dado para ver cuanto de se mueve el jugador.
      */
    int moverse();
    /**
      * @brief El jugador puede comprar la propiedad si no tiene dueño.
      * En caso de que la propiedad tenga dueño el jugador le paga al dueño
      * por haber caído en su propiedad
      * @param box - box donde se encuentra el jugador.
      * @param cantidadJugadores cantidad de jugadores en el juego.
      * @param jugador - jugador actual.
      * @param jugadores el conjunto de jugadores para buscar al dueño de la 
      * casilla box.
      */
    void comprar(box &box, int cantidadJugadores, jugador &jugad, jugador *jugadores);

    /**
      * @brief Revisa si el jugador cayó en la posición de jail del tablero
      * (posición 11).
      * @param  jugador actual del turno
      */
    void goToJail(jugador jugador);

    /**
      * @brief Guarda un booleano en el atributo quebrado de una instancia jugador
      * @param  q booleano que se desea asignar.
      */
    void setQuebrado();
};