#pragma once
#include <iostream>
#include <string>
#include "box.h"
#include "jugador.h"

using namespace std;

class Tablero
{
private:
  box *tablero = new box[36]; /**<Arreglo de casillas*/
  jugador *jugadores;         /**<Arreglo de jugadores*/
  bool ganador;               /**<Condición de ganador*/

public:
  /**
    * @brief Constructor por defecto de la clase.
    */
  Tablero();
  /**
    * @brief Destructor por defecto de la clase.
    */
  ~Tablero();
  /**
    * @brief Permite conocer el estado del jugador.
    * @param jugador - Objeto de tipo jugador.
    */
  void Estado(jugador &jugador);
  /**
    * @brief Asigna los nombres de los jugadores a sus respectivos objetos.
    * @param numeroJugadores - Cantidad de jugadores en el juego.
    * @param nombres - Arreglo con los nombres de los jugadores.
    */
  jugador *Jugadores(int numeroJugadores, string nombres[]);
  /**
    * @brief
    * @param 
    */
  bool Ganador(jugador *jugadores, int cantidadJugadores);

  box *getTablero();
};