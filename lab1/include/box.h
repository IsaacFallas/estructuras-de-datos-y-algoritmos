#pragma once
#include <iostream>
#include <string>

using namespace std;

class box
{
private:
    string name;    /**<Box name*/
    string owner;   /**<Box owner*/
    string color;   /**<Box color*/
    string boxtype; /**<Box type*/
    int boxPose;    /**<Box position in game board*/
    int price;      /**<Bodx price*/

public:
    /**
      * @brief Constructor por defecto de la clase.
      * @param name - Nombre de la casilla.
      * @param type - Tipo de la casilla.
      * @param pose - Posición de la casilla.
      * @param price - Precio de la casilla.
      */
    box();
    /**
      * @brief Destructor por defecto de la clase.
      */
    ~box();

    /*   Setters for box class  */

    /**
      * @brief Establece el nombre del dueño de la casilla.
      * @param type - Tipo de la casilla:
      * @param owner - Nombre del dueño de la casilla.
      */
    void setBoxOwner(string type, string owner);
    void setBoxName(string name);
    void setBoxType(string type);
    void setBoxPose(int pose);
    void setBoxPrice(int price);
    /**
      * @brief Establece el color de la casilla.
      * @param boxColor - Color de la casilla.
      */
    void setBoxColor(string color);

    /*   getters for box class   */

    /**
      * @brief Retorna el nombre de la casilla.
      * @return - nombre de la casilla.
      */
    string getBoxName();
    /**
      * @brief Retorna el dueño de la casilla.
      * @return - nombre del dueño de la casilla.
      */
    string getBoxOwner();
    /**
      * @brief Retorna el color de la casilla.
      * @return - Color de la casilla.
      */
    string getBoxColor();
    /**
      * @brief Retorna el tipo de la casilla.
      * @return - Tipo de la casilla.
      */
    string getBoxType();
    /**
      * @brief Retorna la posición de la casilla.
      * @return - Tipo de la casilla.
      */
    int getBoxPose();
    /**
      * @brief Retorna el precio de la casilla.
      * @return - Precio de la casilla.
      */
    int getBoxPrice();
};