#include "complex.h"

Complex::Complex(){
    this->real = 0;
    this->imag = 0;
}

Complex::Complex(float real, float imag){
    this->real = real;
    this->imag = imag;
}

Complex Complex::operator + (Complex const &obj){
    Complex result;
    result.real = this->real + obj.real;
    result.imag = this->imag + obj.imag;
    
    return result;
}

Complex Complex::operator - (Complex const &obj){
    Complex result;
    result.real = this->real - obj.real;
    result.imag = this->imag - obj.imag;
    
    return result;
}

Complex::operator float() const{
    return this->real;
}

ostream& operator << (ostream& os, Complex const &obj){
    os << obj.real << " + " << obj.imag << "i";
    return os;
}