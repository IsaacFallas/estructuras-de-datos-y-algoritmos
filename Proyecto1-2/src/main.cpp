// General includes
#include <iostream>
#include <vector>
#include "grafo.h"

// LEMON includes
#include <lemon/list_graph.h>

using namespace lemon;

int main(){
    // Bi-Directional graph
    ListGraph graph1;

    // Graph nodes
ListGraph::Node a = graph1.addNode();
ListGraph::Node b = graph1.addNode();
ListGraph::Node c = graph1.addNode();
ListGraph::Node d = graph1.addNode();
ListGraph::Node e = graph1.addNode();
ListGraph::Node f = graph1.addNode();
ListGraph::Node g = graph1.addNode();
ListGraph::Node h = graph1.addNode();
ListGraph::Node i = graph1.addNode(); 


// Graph edges
ListGraph::Edge ab = graph1.addEdge(a, b); 
ListGraph::Edge ah = graph1.addEdge(a, h);
ListGraph::Edge bc = graph1.addEdge(b, c);
ListGraph::Edge bh = graph1.addEdge(b, h);
ListGraph::Edge cd = graph1.addEdge(c, d);
ListGraph::Edge cf = graph1.addEdge(c, f);
ListGraph::Edge ci = graph1.addEdge(c, i);
ListGraph::Edge de = graph1.addEdge(d, e);
ListGraph::Edge df = graph1.addEdge(d, f);
ListGraph::Edge ef = graph1.addEdge(e, f);
ListGraph::Edge fg = graph1.addEdge(f, g);
ListGraph::Edge gh = graph1.addEdge(g, h);
ListGraph::Edge gi = graph1.addEdge(g, i);
ListGraph::Edge hi = graph1.addEdge(h, i); 

ListGraph::Edge ba = graph1.addEdge(b, a);
ListGraph::Edge ha = graph1.addEdge(h, a);
ListGraph::Edge cb = graph1.addEdge(c, b);
ListGraph::Edge hb = graph1.addEdge(h, b);
ListGraph::Edge dc = graph1.addEdge(d, c);
ListGraph::Edge fc = graph1.addEdge(f, c);
ListGraph::Edge ic = graph1.addEdge(i, c);
ListGraph::Edge ed = graph1.addEdge(e, d);
ListGraph::Edge fd = graph1.addEdge(f, d);
ListGraph::Edge fe = graph1.addEdge(f, e);
ListGraph::Edge gf = graph1.addEdge(g, f);
ListGraph::Edge hg = graph1.addEdge(h, g);
ListGraph::Edge ig = graph1.addEdge(i, g);
ListGraph::Edge ih = graph1.addEdge(i, h);



// Edges costs
ListGraph::EdgeMap<int> cost1(graph1);
cost1[ab] = 4;
cost1[ah] = 8;
cost1[bc] = 8;
cost1[bh] = 11;
cost1[cd] = 7;
cost1[cf] = 4;
cost1[ci] = 2;
cost1[de] = 9;
cost1[df] = 14;
cost1[ef] = 10;
cost1[fg] = 2;
cost1[gh] = 1;
cost1[gi] = 6;
cost1[hi] = 7;

cost1[ba] = 4;
cost1[ha] = 8;
cost1[cb] = 8;
cost1[hb] = 11;
cost1[dc] = 7;
cost1[fc] = 4;
cost1[ic] = 2;
cost1[ed] = 9;
cost1[fd] = 14;
cost1[fe] = 10;
cost1[gf] = 2;
cost1[hg] = 1;
cost1[ig] = 6;
cost1[ih] = 7;

ListGraph graph2;

ListGraph::Node A = graph2.addNode();
ListGraph::Node B = graph2.addNode();
ListGraph::Node C = graph2.addNode();
ListGraph::Node D = graph2.addNode();
ListGraph::Node E = graph2.addNode();
ListGraph::Node F = graph2.addNode();
ListGraph::Node G = graph2.addNode();
ListGraph::Node H = graph2.addNode();
ListGraph::Node I = graph2.addNode();
ListGraph::Node J = graph2.addNode(); 
// Graph edges
ListGraph::Edge AF = graph2.addEdge(A, F);
ListGraph::Edge AH = graph2.addEdge(A, H);
ListGraph::Edge AI = graph2.addEdge(A, I);
ListGraph::Edge BF = graph2.addEdge(B, F);
ListGraph::Edge BG = graph2.addEdge(B, G);
ListGraph::Edge BH = graph2.addEdge(B, H);
ListGraph::Edge BJ = graph2.addEdge(B, J);
ListGraph::Edge CF = graph2.addEdge(C, F);
ListGraph::Edge CE = graph2.addEdge(C, E);
ListGraph::Edge DG = graph2.addEdge(D, G);
ListGraph::Edge DJ = graph2.addEdge(D, J);
ListGraph::Edge EF = graph2.addEdge(E, F);
ListGraph::Edge EJ = graph2.addEdge(E, J);
ListGraph::Edge GH = graph2.addEdge(G, H); 

ListGraph::Edge FA = graph2.addEdge(F, A);
ListGraph::Edge HA = graph2.addEdge(H, A);
ListGraph::Edge IA = graph2.addEdge(I, A);
ListGraph::Edge FB = graph2.addEdge(F, B);
ListGraph::Edge GB = graph2.addEdge(G, B);
ListGraph::Edge HB = graph2.addEdge(H, B);
ListGraph::Edge JB = graph2.addEdge(J, B);
ListGraph::Edge FC = graph2.addEdge(F, C);
ListGraph::Edge EC = graph2.addEdge(E, C);
ListGraph::Edge GD = graph2.addEdge(G, D);
ListGraph::Edge JD = graph2.addEdge(J, D);
ListGraph::Edge FE = graph2.addEdge(F, E);
ListGraph::Edge JE = graph2.addEdge(J, E);
ListGraph::Edge HG = graph2.addEdge(H, G);

// Edges costs
ListGraph::EdgeMap<int> cost2(graph2) ;
cost2[AF] = 343;
cost2[AH] = 1435;
cost2[AI] = 464;
cost2[BF] = 879;
cost2[BG] = 954;
cost2[BH] = 811;
cost2[BJ] = 524;
cost2[CF] = 1054;
cost2[CE] = 1364;
cost2[DG] = 433;
cost2[DJ] = 1053;
cost2[EF] = 1106;
cost2[EJ] = 766;
cost2[GH] = 837;

cost2[FA] = 343;
cost2[HA] = 1435;
cost2[IA] = 464;
cost2[FB] = 879;
cost2[GB] = 954;
cost2[HB] = 811;
cost2[JB] = 524;
cost2[FC] = 1054;
cost2[EC] = 1364;
cost2[GD] = 433;
cost2[JD] = 1053;
cost2[FE] = 1106;
cost2[JE] = 766;
cost2[HG] = 837;

grafo grafo;

std::cout << "Métodos para el grafo 1" << std::endl;

std::cout << std::endl;
grafo.profundidad(a,graph1);
std::cout << std::endl;
grafo.anchitud(a,graph1);
std::cout << std::endl;
grafo.Kruskel(graph1, cost1);
std::cout << std::endl;
grafo.Prim(a, graph1, cost1);
std::cout << std::endl;

std::cout << "Métodos para el grafo 2" << std::endl;

std::cout << std::endl;
grafo.profundidad(A,graph2);
std::cout << std::endl;
grafo.anchitud(a,graph2);
std::cout << std::endl;
grafo.Kruskel(graph2, cost2);
std::cout << std::endl;
grafo.Prim(A, graph2, cost2);
std::cout << std::endl;

// // Traversing graph edges
// std::cout << "**Imprimiendo información de aristas**" << std::endl;
// for(ListGraph::EdgeIt it(graph); it != INVALID; ++it){
//     std::cout << "Arista " << graph.id(it)  << " entre los nodos " <<
//     graph.id(graph.u(it)) << " y " << graph.id(graph.v(it)) <<
//     " tiene un costo de " << cost[it] << std::endl;
// }

// // Traversing graph nodes using an iterator
// std::cout << "**Imprimiendo nodos**" << std::endl;
// for(ListGraph::NodeIt it(graph); it != INVALID; ++it){
//     std::cout << "Nodo " << graph.id(it) << std::endl;
// }

// // Obtaining neighbours from a given node
// std::cout << "**Imprimiendo cantidad de vecinos del nodo 0**" << std::endl;
// ListGraph::Node temp = graph.nodeFromId(0);
// std::vector<int> neighbours;
// int count = 0;
// for(ListGraph::EdgeIt it(graph); it != INVALID; ++it){
//     if( graph.id(graph.u(it)) == graph.id(temp) ){
//         neighbours.push_back(graph.id(graph.v(it)));
//         count++;
//     }
// }
// std::cout << "Nodo " << graph.id(temp) << " tiene " << count << " vecinos" << std::endl;
// std::cout << "Vecinos de 0 son: " << std::endl;
// for(int i = 0; i < neighbours.size(); i++){
//     std::cout << "\t-" << neighbours[i] << std::endl;
// }

// Put your methods calls here

// exit(0);
}