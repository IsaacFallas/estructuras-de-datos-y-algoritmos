#include "grafo.h"

grafo::grafo()
{
}

grafo::~grafo()
{
}

void grafo::profundidad(ListGraph::Node &N, ListGraph &G)
{

    std::cout << "**Recorrido en profundidad desde el nodo " << G.id(N) << "**" << std::endl;

    std::vector<int> vecinos_int;
    std::stack<int> aux;

    //int count_vecinos = 0;
    int count_nodos = 0;

    for (ListGraph::NodeIt it(G); it != INVALID; ++it)
    {
        count_nodos++;
    }

    bool nodos[count_nodos];

    for (int i = 0; i < count_nodos; i++)
    {
        nodos[i] = false;
    }

    bool recorrido = false;
    bool flag = false;

    vecinos_int.push_back(G.id(N));
    aux.push(G.id(N));
    nodos[G.id(N)] = true;
    //std::cout << G.id(N) << " " << std::endl;

    int count = 0;

    while (!recorrido)
    {
        flag = false;

        if (count == (count_nodos - 1))
        {
            recorrido = true;
        }

        if (!recorrido)
        {
            for (ListGraph::EdgeIt E_it(G); E_it != INVALID; ++E_it)
            {
                if ((G.id(G.u(E_it)) == G.id(N)) && (nodos[G.id(G.v(E_it))] == false))
                {
                    //std::cout << "Hola" << std::endl;
                    std::cout << "Pila: " << aux.top() << "(fuente) " << G.id(G.v(E_it)) << "(vecino)" << std::endl;
                    //std::cout << "Vector: " << vecinos_int[count] << std::endl;
                    vecinos_int.push_back(G.id(G.v(E_it)));
                    aux.push(G.id(G.v(E_it)));
                    nodos[G.id(G.v(E_it))] = true;
                    N = G.v(E_it);
                    count++;
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
                //std::cout << "hola" << std::endl;
                aux.pop();
                N = G.nodeFromId(aux.top());
            }
        }
    }

    for (size_t i = 0; i < vecinos_int.size(); i++)
    {
        std::cout << vecinos_int[i] << " ";
    }

    std::cout << std::endl;
}

void grafo::anchitud(ListGraph::Node &N, ListGraph &G)
{

    /*

    1. create a queue Q 
    2. mark v as visited and put v into Q 
    3. while Q is non-empty 
    4. remove the head u of Q 
    5. mark and enqueue all (unvisited) neighbours of u
    */

    cout << endl
         << endl
         << "   --> Recorrido por anchitud  <--   " << endl;

    bool visitados[countNodes(G)]; // Visitados booleanos
    int inicial = G.id(N);

    ////////////////////////////////////////////////////////////////
    //Impresiones de visitas inicial:
    cout << "    Visitados[i]: " << endl;
    for (int i = 0; i < countNodes(G); i++)
    {

        cout << i << " "; // impresión de visitados
    }
    cout << endl;

    //Seteo de visitas en false e impresión:
    for (int i = 0; i < countNodes(G); i++)
    {
        visitados[i] = false;
        cout << visitados[i] << " ";
    }

    cout << endl;
    ////////////////////////////////////////////////////////////////

    queue<int> Q; // Cola para BFS

    cout << endl
         << "Nodo de entrada: " << G.id(N) << endl;

    Q.push(G.id(N)); // agrega nodo de entrada a la cola
    int c = 0;

    queue<int> aux; // auxiliar para guardar recorrido anchitud e imprimir al final

    while (!Q.empty())
    {

        for (ListGraph::EdgeIt E_it(G); E_it != INVALID; ++E_it)
        {
            c++;
            if (c < (countNodes(G)))
            {
                ////////////////////////////////////////////////////////////////
                //Impresiones de visitas:
                cout << "    Visitados[i]: " << endl;
                for (int i = 0; i < countNodes(G); i++)
                {

                    cout << i << " "; // impresión de visitados
                }
                cout << endl;
                for (int i = 0; i < countNodes(G); i++)
                {

                    cout << visitados[i] << " ";
                }
                cout << endl;
                ////////////////////////////////////////////////////////////////
            }
            if (visitados[G.id(G.v(E_it))] == false)
            {
                Q.push(G.id(G.v(E_it)));
                visitados[G.id(G.v(E_it))] = true;
                cout << G.id(N) << endl;
                aux.push(G.id(N));
                N = G.v(E_it);
                Q.pop();
            }

            if (visitados[G.id(G.u(E_it))] == false)
            {
                Q.push(G.id(G.u(E_it)));
                visitados[G.id(G.u(E_it))] = true;
                cout << G.id(N) << endl;
                aux.push(G.id(N));
                N = G.u(E_it);
                Q.pop();
            }
        }
        Q.pop();
    }
    cout << "El recorrido en anchitud desde " << inicial << ": " << endl;
    for (int i = 0; i < aux.size(); i++)
    {
        cout << aux.front() << " ";
        aux.pop();
    }

    cout << endl;
}

void grafo::Kruskel(ListGraph &G, ListGraph::EdgeMap<int> &cost)
{

    std::cout << "**Algoritmo de Kruskal**" << std::endl;

    std::vector<int> nodosVisitados;
    std::vector<int> recorrido;
    std::vector<int> recorrido_aux;
    std::stack<int> nodosRecorridos;

    int count_nodos = 0;
    int count_aristas = 0;
    int count_nodosVisitados = 0;

    for (ListGraph::EdgeIt it(G); it != INVALID; ++it)
    {

        count_aristas++;
    }

    int aristas[count_aristas];
    int pesoAristas[count_aristas];

    int k = 0;

    for (ListGraph::EdgeIt it(G); it != INVALID; ++it)
    {

        pesoAristas[k] = cost[it];
        aristas[k] = G.id(it);
        k++;
    }

    for (int i = 0; i < (count_aristas - 1); i++)
    {
        for (int j = 0; j < (count_aristas - i - 1); j++)
        {
            if (pesoAristas[j] > pesoAristas[j + 1])
            {
                int temp = pesoAristas[j];
                int *x_1 = &pesoAristas[j];
                int *y_1 = &pesoAristas[j + 1];
                *x_1 = *y_1;
                *y_1 = temp;

                int temp2 = aristas[j];
                int *x_2 = &aristas[j];
                int *y_2 = &aristas[j + 1];
                *x_2 = *y_2;
                *y_2 = temp2;
            }
        }
    }

    for (ListGraph::NodeIt it(G); it != INVALID; ++it)
    {
        count_nodos++;
    }

    bool nodos[count_nodos];

    for (int i = 0; i < count_nodos; i++)
    {
        nodos[i] = false;
    }

    for (int i = 0; i < count_aristas; i++)
    {
        ListGraph::Edge temp = G.edgeFromId(aristas[i]);
        if ((nodos[G.id(G.u(temp))] == false) && (nodos[G.id(G.v(temp))] == false))
        {
            recorrido.push_back(G.id(temp));
            recorrido_aux.push_back(G.id(temp));
            nodos[G.id(G.u(temp))] = true;
            nodos[G.id(G.v(temp))] = true;
            count_nodosVisitados = count_nodosVisitados + 2;
        }
        else if ((nodos[G.id(G.u(temp))] == false) && (nodos[G.id(G.v(temp))] == true))
        {

            recorrido.push_back(G.id(temp));
            recorrido_aux.push_back(G.id(temp));
            nodos[G.id(G.u(temp))] = true;
            count_nodosVisitados++;
        }
        else if ((nodos[G.id(G.u(temp))] == true) && (nodos[G.id(G.v(temp))] == false))
        {
            recorrido.push_back(G.id(temp));
            recorrido_aux.push_back(G.id(temp));
            nodos[G.id(G.v(temp))] = true;
            count_nodosVisitados++;
        }
        else if ((nodos[G.id(G.u(temp))] == true) && (nodos[G.id(G.v(temp))] == true))
        {

            int Inicio = G.id(G.u(temp));
            int Final = G.id(G.v(temp));
            int ptr;

            bool nodoR[count_nodos];

            for (int j = 0; j < count_nodos; j++)
            {
                nodoR[j] = false;
            }

            bool aristaR[recorrido.size()];

            for (size_t j = 0; j < recorrido.size(); j++)
            {
                aristaR[j] = false;
            }

            nodoR[Inicio] = true;

            nodosRecorridos.push(Inicio);

            ptr = Inicio;

            bool kruskel = false;

            while (!kruskel)
            {

                bool sinVecinos = true;

                for (size_t x = 0; x < recorrido.size(); x++)
                {
                    ListGraph::Edge a = G.edgeFromId(recorrido[x]);
                    int a_inicio = G.id(G.u(a));
                    int a_final = G.id(G.v(a));

                    for (size_t y = 0; y < recorrido.size(); y++)
                    {
                        ListGraph::Edge b = G.edgeFromId(recorrido[y]);
                        int b_inicio = G.id(G.u(b));
                        int b_final = G.id(G.v(b));

                        if ((aristaR[y] == false) && ((b_inicio == ptr) || (b_final == ptr)))
                        {
                            sinVecinos = false;
                            break;
                        }
                    }

                    if ((aristaR[x] == false) && (a_inicio == ptr))
                    {

                        ptr = a_final;

                        nodoR[ptr] = true;
                        aristaR[x] = true;

                        nodosRecorridos.push(ptr);

                        break;
                    }
                    else if ((aristaR[x] == false) && (a_final == ptr))
                    {

                        ptr = a_inicio;

                        nodoR[ptr] = true;
                        aristaR[x] = true;
                        nodosRecorridos.push(ptr);

                        break;
                    }
                    else if (ptr == Final)
                    {
                        while (!nodosRecorridos.empty())
                        {
                            nodosRecorridos.pop();
                        }

                        kruskel = true;
                        break;
                    }
                    else if ((nodoR[ptr] == true) && (sinVecinos == true))
                    {
                        nodosRecorridos.pop();

                        if (nodosRecorridos.size() == 0)
                        {
                            recorrido.push_back(G.id(temp));
                            kruskel = true;
                            break;
                        }
                        else
                        {
                            ptr = nodosRecorridos.top();
                            break;
                        }
                    }
                }
            }
        }
    }
    for (size_t i = 0; i < recorrido.size(); i++)
    {
        ListGraph::Edge caminos = G.edgeFromId(recorrido[i]);
        std::cout << "Arista " << G.id(caminos) << " entre los nodos "
                  << G.id(G.u(caminos)) << " y " << G.id(G.v(caminos)) << std::endl;
    }
}

void grafo::Prim(ListGraph::Node &node, ListGraph &graph, ListGraph::EdgeMap<int> &cost)
{
    std::cout << "**Algoritmo de Prim**" << std::endl;
    std::vector<int> visited;
    std::vector<int> edgeValues;
    std::vector<ListGraph::Edge> Edges;
    int countBool = 0;
    int nodesNumbers = countNodes(graph);  
    while (countBool <= nodesNumbers)
    {
        visited.push_back(graph.id(node));
        for (ListGraph::EdgeIt it(graph); it != INVALID; ++it)
        {
            if (graph.id(graph.u(it)) == graph.id(node))
            {
                edgeValues.push_back(cost[it]);
                Edges.push_back(it);
            }
        }
        int less = edgeValues[0];
        for (size_t i = 0; i < edgeValues.size(); i++)
        {
            if (edgeValues[i] < less)
            {
                less = edgeValues[i];
            }
        }
        int ID = 0;
        for (size_t i = 0; i < edgeValues.size(); i++)
        {
            if (edgeValues[i] == less)
            {
                ID = i;
                break;
            }
        }
        bool isVisited = false;
        for (size_t i = 0; i < visited.size(); i++)
        {
            if (visited[i] == graph.id(graph.v(Edges[ID])))
            {
                isVisited = true;
                break;
            }
        }
        if (isVisited == false)
        {
            ListGraph::Node next = graph.nodeFromId(graph.id(graph.v(Edges[ID])));
            node = next;
            Edges.erase(Edges.begin() + ID);
            edgeValues.erase(edgeValues.begin() + ID);
        }
        else if (isVisited == true)
        {
            if (countBool < (nodesNumbers)-1)
            {
                while (isVisited == true)
                {
                    Edges.erase(Edges.begin() + ID);
                    edgeValues.erase(edgeValues.begin() + ID);
                    less = edgeValues[0];
                    for (size_t i = 0; i < edgeValues.size(); i++)
                    {
                        if (edgeValues[i] < less)
                        {
                            less = edgeValues[i];
                        }
                    }
                    ID = 0;
                    for (size_t i = 0; i < edgeValues.size(); i++)
                    {
                        if (edgeValues[i] == less)
                        {
                            ID = i;
                            break;
                        }
                    }
                    isVisited = false;
                    for (size_t i = 0; i < visited.size(); i++)
                    {
                        if (visited[i] == graph.id(graph.v(Edges[ID])))
                        {
                            isVisited = true;
                            break;
                        }
                    }
                }
                ListGraph::Node next = graph.nodeFromId(graph.id(graph.v(Edges[ID])));
                node = next;
                Edges.erase(Edges.begin() + ID);
                edgeValues.erase(edgeValues.begin() + ID);
            }
        }
        countBool++;
    }
    visited.pop_back();
    std::cout << "Orden de visita" << std::endl;
    for (size_t i = 0; i < visited.size(); i++)
    {
        std::cout << visited[i] << " ";
    }
    std::cout << std::endl;
}

/*void grafo::Prim(ListGraph::Node &node, ListGraph &graph, ListGraph::EdgeMap<int> &cost)
{
    //Pruebas de Lemon e ideas para implementar Prim
    //Ya pude probar por separado mis dudas, ahora sólo me unirlas
    std::vector<int> neighbours;
    std::vector<int> edge;
    std::vector<int> visited;
    int count = 0;
    int Nodes = countNodes(graph);
    ListGraph::NodeMap<bool> Check(graph);
    for (ListGraph::NodeIt it(graph); it != INVALID; ++it)
    {
        int node = graph.id(it);
        std::cout << "El booleano del nodo " << node << " es: " << Check[it] << std::endl;
    }
    for (ListGraph::EdgeIt it(graph); it != INVALID; ++it)
    {
        if (graph.id(graph.u(it)) == graph.id(node))
        {
            ListGraph::Node N = graph.nodeFromId(graph.id(graph.u(it)));
            Check[N] = true;
            neighbours.push_back(graph.id(graph.v(it)));
            edge.push_back(cost[it]);
            visited.push_back(graph.id(node));
        }
    }
    for (size_t i = 0; i < visited.size(); i++)
    {
        std::cout << "El nodo es: " << visited[i] << std::endl;
        std::cout << "Los vecinos son: " << std::endl;
        for (size_t i = 0; i < neighbours.size(); i++)
        {
            std::cout << neighbours[i] << " y la arista que lo conecta es: " << edge[i] << std::endl;
        }
    }
    int less = edge[0];
    for (size_t i = 0; i < edge.size(); i++)
    {
        if (edge[i] < less)
        {
            less = edge[i];
        }
    }
    std::cout << "El menor es: " << less << std::endl;
    for (ListGraph::EdgeIt it(graph); it != INVALID; ++it)
    {
        if ((graph.id(graph.u(it)) == graph.id(node)) && (cost[it] == less))
        {
            int Node = graph.id(graph.v(it));
            std::cout << "El nodo que conecta con la arista menor es: " << Node << std::endl;
        }
    }
    std::cout << std::endl;
    std::cout << "Pruebas para checkear el mapa de booleanos" << std::endl;
    std::cout << std::endl;
    std::cout << "Booleano: " << Check[node] << std::endl;
    std::cout << "Valor de count: " << count << std::endl;
    bool allVisited = false;
    int countBool = 0;
    while (allVisited == false)
    {
        std::cout << "Hi" << std::endl;
        std::cout << "Valor de count: " << count << std::endl;
        for (ListGraph::NodeIt it(graph); it != INVALID; ++it)
        {
            int node = graph.id(it);
            std::cout << "El booleano del nodo " << node << " es: " << Check[it] << std::endl;
            std::cout << "El valor de countBool es: " << countBool << std::endl;
        }
        countBool++;
        if (countBool == Nodes)
        {
            allVisited = true;
        }
        std::cout << "Valor de allVisited: " << allVisited << std::endl;
        ListGraph::Node nodes = graph.nodeFromId(count);
        Check[nodes] = true;
        count++;
    }
    std::cout << "***Comienzo del último for***" << std::endl;
    for (ListGraph::NodeIt it(graph); it != INVALID; ++it)
    {
        int node = graph.id(it);
        std::cout << "El booleano del nodo " << node << " es: " << Check[it] << std::endl;
    }
    std::cout << "*****" << std::endl;
    std::cout << "Valor final de allVisited: " << allVisited << std::endl;
    std::cout << std::endl;
    std::cout << "***Empezando pruebas pare el id de los edges***" << std::endl;
    std::cout << std::endl;
    std::vector<ListGraph::Edge> Edges;
    std::vector<int> valores;
    for (ListGraph::EdgeIt it(graph); it != INVALID; ++it)
    {
        Edges.push_back(it);
        valores.push_back(cost[it]);
    }
    std::cout << "La longitud de Edges es : " << Edges.size() << std::endl;
    std::cout << "La longitud de valores es : " << valores.size() << std::endl;
    for (size_t i = 0; i < Edges.size(); i++)
    {
        std::cout << "El id del edge es " << graph.id(Edges[i]) << " y el valor del edge es " << valores[i] << std::endl;
        std::cout << "El nodo que le sigue es " << graph.id(graph.v(Edges[i])) << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Pruebas para que el 'próximo nodo' sea el actual en la siguiente iteración" << std::endl;
    std::cout << std::endl;
    std::cout << "El nodo actual es " << graph.id(node) << std::endl;
    ListGraph::Node next = graph.nodeFromId(3);
    node = next;
    std::cout << "El nodo actualizado es " << graph.id(node) << std::endl;
}*/
