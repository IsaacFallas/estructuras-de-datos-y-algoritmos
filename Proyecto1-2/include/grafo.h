#pragma once
#include <iostream>
#include <vector>
#include <queue>

#include <bits/stdc++.h>

// LEMON includes
#include <lemon/list_graph.h>

using namespace lemon;

using namespace std;

/** @brief Clase que implementa un árbol binario con sus métodos básicos
 * @author Esteban Rodríguez
 * @author Jorge Fallas
 * @author Gabriel Gutiérrez
 */

class grafo
{
private:
    /* data */
public:
    /**
     * @brief Contructor por defecto de la clase 
     */
    grafo();
    /**
     * @brief Destructor por defecto de la clase 
     */
    ~grafo();
    /**
     * @brief Método que muestra el por recorrido por anchitud de un grafo.
     * @param N - Referencia a un objeto de tipo nodo de la clase ListGraph, de la biblioteca Lemon. 
     * @param G - Referencia a un objeto de la biblioteca Lemon; es un grafo.
     */
    void anchitud(ListGraph::Node &N, ListGraph &G);
    /**
     * @brief Método que muestra el recorrido por profundidad de un grafo.
     * @param N - Referencia a un objeto de tipo nodo de la clase ListGraph de la biblioteca Lemon. 
     * @param G - Referencia a un objeto de la biblioteca Lemon; es un grafo.
     */
    void profundidad(ListGraph::Node &N, ListGraph &G);
    /**
     * @brief Método que muestra el resultado de aplicar el algoritmo de Prim a un grafo.
     * @param node - Referencia a un objeto de tipo nodo de la clase ListGraph de la biblioteca Lemon. 
     * @param graph - Referencia a un objeto de la biblioteca Lemon; es un grafo.
     * @param cost - Referencia a un objeto tipo mapa de aristas de la clase ListGraph de la biblioteca Lemon.
     */
    void Prim(ListGraph::Node &node, ListGraph &graph, ListGraph::EdgeMap<int> &cost);
    /**
     * @brief Método que muestra el resultado de aplicar el algoritmo de Kruskel a un grafo.
     * @param G - Referencia a un objeto de la biblioteca Lemon; es un grafo.
     * @param cost - Referencia a un objeto tipo mapa de aristas de la clase ListGraph de la biblioteca Lemon.
     */
    void Kruskel(ListGraph &G, ListGraph::EdgeMap<int> &cost);
};
