#include <iostream>
#include "calculadora.h"

using namespace std;

int main(int argc, char const *argv[])
{
    int a = 5;
    int b = 10;
    Calculadora<int> result1;
    int A = result1.add(a,b);
    cout<<"Suma con enteros"<<endl;
    result1.print(A);
    int B = result1.sub(a, b);
    cout << "Resta con enteros" << endl;
    result1.print(B);
    int C = result1.mul(a, b);
    cout << "Multiplicación con enteros" << endl;
    result1.print(C);
    int D = result1.div(a, b);
    cout << "División con enteros" << endl;
    result1.print(D);
    float c = 2.545;
    float d = 3.534;
    Calculadora<float> result2;
    float E = result2.add(c,d);
    cout<<"Suma con reales"<<endl;
    result2.print(E);
    float F = result2.sub(c, d);
    cout << "Resta con reales" << endl;
    result2.print(F);
    float G = result2.mul(c, d);
    cout << "Multiplicación con reales" << endl;
    result2.print(G);
    float H = result2.div(c, d);
    cout << "División con reales" << endl;
    result2.print(H);
    Calculadora<Polinomio> P;
    Polinomio p1(a,b,2);
    Polinomio p2(0,3,1);
    Polinomio pol1 = P.add(p1,p2);
    cout<<"Suma de polinomios"<<endl;
    P.print(pol1);
    Polinomio pol2 = P.sub(p1, p2);
    cout << "Resta de polinomios" << endl;
    P.print(pol2);
    Polinomio pol3 = P.mul(p1, p2);
    cout << "Multiplicación de polinomios" << endl;
    P.print(pol3);
    Polinomio pol4 = P.div(p1, p2);
    cout << "División de polinomios" << endl;
    P.print(pol4);
    int num1 = 2;
    int num2 = 3;
    int den1 = 5;
    int den2 = 4;
    Calculadora<Fraccion> Fr;
    Fraccion f1(num1,den1);
    Fraccion f2(num2,den2);
    Fraccion fr1 = Fr.add(f1,f2);
    cout<<"Suma de fracciones"<<endl;
    Fr.print(fr1);
    Fraccion fr2 = Fr.sub(f1, f2);
    cout << "Resta de fracciones" << endl;
    Fr.print(fr2);
    Fraccion fr3 = Fr.mul(f1, f2);
    cout << "Multiplicación de fracciones" << endl;
    Fr.print(fr3);
    Fraccion fr4 = Fr.div(f1, f2);
    cout << "División de fracciones" << endl;
    Fr.print(fr4);
    return 0;
}

