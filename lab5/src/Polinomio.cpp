#include "Polinomio.h"

Polinomio::Polinomio()
{
    this->a = 0;
    this->b = 0;
    this->c = 0;
    this->d = 0;
    this->e = 0;
}

Polinomio::Polinomio(float c, float d, float e)
{
    this->a = 0;
    this->b = 0;
    this->c = c;
    this->d = d;
    this->e = e;
}

Polinomio Polinomio::operator+(Polinomio const &obj)
{
    Polinomio result;
    result.c = this->c + obj.c;
    result.d = this->d + obj.d;
    result.e = this->d + obj.e;

    return result;
}

Polinomio Polinomio::operator-(Polinomio const &obj)
{
    Polinomio result;
    result.c = this->c - obj.c;
    result.d = this->d - obj.d;
    result.e = this->d - obj.e;

    return result;
}

Polinomio Polinomio::operator*(Polinomio const &obj)
{

    Polinomio result;

    result.a = (this->c) * (obj.c);
    result.b = (this->c) * (obj.d) + (this->d) * (obj.c);
    result.c = (this->c) * (obj.e) + (this->d) * (obj.d) + (this->e) * (obj.c);
    result.d = (this->d) * (obj.e) + (this->e) * (obj.d);
    result.e = (this->e) * (obj.e);

    return result;
}

Polinomio Polinomio::operator/(Polinomio const &obj)
{
    Polinomio result;
    vector<float> Div;
    vector<float> aux;
    Div.push_back(this->a);
    Div.push_back(this->b);
    Div.push_back(this->c);
    Div.push_back(this->d);
    Div.push_back(this->e);
    float div = (obj.e) * -1;
//    float residue;
    for (size_t i = 0; i < Div.size(); i++)
    {
        if (i == 0)
        {
            aux.push_back(Div[i]);
        }
        else
        {
            float res = (aux[i - 1] * div) + Div[i];
            aux.push_back(res);
        }
    }
    int count = 0;
    for (size_t j = 0; j < aux.size(); j++)
    {
        if (aux[j] == 0)
        {
            count += 1;
        }
    }
 //   residue = aux[int(aux.size() - 1)];
    aux.pop_back();

    int idx = 0;
    while (idx <= count)
    {
        aux.insert(aux.begin(), 0);
        idx += 1;
    }
    int num = int(aux.size() - 1);

    result.e = aux[num];
    result.d = aux[num - 1];
    result.c = aux[num - 2];
    result.b = aux[num - 3];
    result.a = aux[num - 4];
    return result;
}

istream &operator>>(istream &is, Polinomio &obj)
{
    int x0;
    int x1;
    int x2;
    cout << "\t\tIngrese la constate para x⁰:" << endl
         << "\t\t";
    is >> x0;
    cout << "\t\tIngrese la constate para x¹:" << endl
         << "\t\t";
    is >> x1;
    cout << "\t\tIngrese la constate para x²:" << endl
         << "\t\t";
    is >> x2;

    obj.c = x2;
    obj.d = x1;
    obj.e = x0;

    return is;
}

ostream &operator<<(ostream &os, Polinomio &obj)
{

    if (obj.a == 0 && obj.b == 0)
    {
        os << obj.c << "x²"
           << " + " << obj.d << "x"
           << " + " << obj.e;
    }
    else if (obj.a == 0 && obj.b != 0)
    {
        os << obj.b << "x³ + "
           << obj.c << "x²"
           << " + " << obj.d << "x"
           << " + " << obj.e;
    }
    else
    {
        os << obj.a << "x⁴"
           << " + " << obj.b << "x³"
           << " + " << obj.c << "x²"
           << " + " << obj.d << "x"
           << " + " << obj.e;
    }
    return os;
}