#include "Fraccion.h"

Fraccion::Fraccion()
{
    this->num = 0;
    this->dem = 0;
}

Fraccion::Fraccion(float num, float dem)
{
    this->num = num;
    this->dem = dem;
}

Fraccion Fraccion::operator+(Fraccion const &obj)
{
    Fraccion result;
    float maxCD = (this->dem) * (obj.dem); //  Cálculo del máximo común divisor
    float ePLP = (maxCD / this->dem) * this->num + (maxCD / dem) * num;

    result.num = ePLP;
    result.dem = maxCD;

    return result;
}

Fraccion Fraccion::operator-(Fraccion const &obj)
{
    Fraccion result;
    float maxCD = (this->dem) * (obj.dem); //  Cálculo del máximo común divisor
    float ePLP = (maxCD / this->dem) * this->num - (maxCD / obj.dem) * obj.num;

    result.num = ePLP;
    result.dem = maxCD;

    return result;
}

Fraccion Fraccion::operator*(Fraccion const &obj)
{

    Fraccion result;
    result.num = (this->num) * (obj.num);
    result.dem = (this->dem) * (obj.dem);

    return result;
}


Fraccion Fraccion::operator / (Fraccion const &obj){
    Fraccion result;
    float newNum = (this->num) * obj.num;
    float newDem = (this->dem) * obj.dem;
    result.num = newNum;
    result.dem = newDem;
    return result;
}

istream &operator>>(istream &is, Fraccion &obj)
{
    string input;

    is >> input;

    float a = (input[0]) - '0';
    float b = (input[2]) - '0';

    obj.num = a;
    obj.dem = b;

    return is;
}

ostream &operator<<(ostream &os, Fraccion &obj)
{
    os << obj.num << "/" << obj.dem;

    return os;
}