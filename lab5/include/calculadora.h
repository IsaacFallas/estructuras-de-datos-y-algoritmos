#pragma once

#include "Polinomio.h"
#include "Fraccion.h"

using namespace std;

template <typename Data>

class Calculadora{
    private:

    public:
      /**
       * @brief Constructor por defecto de la clase.
       */
      Calculadora();
      /**
       * @brief Destructor por defecto de la clase.
       */
      ~Calculadora();
      /**
       * @brief Método emplatillado que suma dos datos genércicos.
       * @param &d1 - Referencia a un objeto emplatillado.
       * @param &d2 - Referencia constante a un objeto emplatillado.
       * @return Dato genérico que resulta de la suma.
       */
      Data add(Data &d1, const Data &d2);
      /**
       * @brief Método emplatillado que resta dos datos genércicos.
       * @param &d1 - Referencia a un objeto emplatillado.
       * @param &d2 - Referencia constante a un objeto emplatillado.
       * @return Dato genérico que resulta de la resta.
       */
      Data sub(Data &d1, const Data &d2);
      /**
       * @brief Método emplatillado que multiplica dos datos genércicos.
       * @param &d1 - Referencia a un objeto emplatillado.
       * @param &d2 - Referencia constante a un objeto emplatillado.
       * @return Dato genérico que resulta de la multiplicación.
       */
      Data mul(Data &d1, const Data &d2);
      /**
       * @brief Método emplatillado que divide dos datos genércicos.
       * @param &d1 - Referencia a un objeto emplatillado.
       * @param &d2 - Referencia constante a un objeto emplatillado.
       * @return Dato genérico que resulta de la división.
       */
      Data div(Data &d1, const Data &d2);
      void print(Data &d);
};

template <typename Data> Calculadora<Data>::Calculadora(){
  //Does nothing
}

template <typename Data> Calculadora<Data>::~Calculadora(){
  //Does nothing
}

template <typename Data> Data Calculadora<Data>::add(Data &d1, const Data &d2){
  Data result;
  result = d1 + d2;
  return result;
}

template <typename Data> Data Calculadora<Data>::sub(Data &d1, const Data &d2){
  Data result;
  result = d1 - d1;
  return result;
}

template <typename Data> Data Calculadora<Data>::mul(Data &d1, const Data &d2){
  Data result;
  result = d1 * d2;
  return result;
}

template <typename Data> Data Calculadora<Data>::div(Data &d1, const Data &d2){
  Data result;
  result = d1 / d2;
  return result;
}

template <typename Data> void Calculadora<Data>::print(Data &d){
  cout << "El resultado es: " << d << endl;
}
